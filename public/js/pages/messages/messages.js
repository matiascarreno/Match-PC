function rate()
{

    swal({
        title: "Dinos que te pareció!",
        text: "cuéntanos que piensas de nuestra asesoría",
        type: "input",
        showCancelButton: true,
        confirmButtonText: "Enviar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Cuéntanos"
    }, function (inputValue) {
        input=inputValue;
        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("Debes escribir algo que podamos entender"); return false
        }
        swal("Genial!", "Gracias por tu feedback", "success");
        
    });
}


function message_send()
{
    if($("#message_text").val()!=="" && $("#message_text").val()!==null)
    {
        var message_text = $("#message_text").val();
        var current_chat = $("#current_chat").val();

        var message = 
        {
            id_chat : current_chat,
            text : message_text,            
        };

        $.ajax
        ({
            url: "/messages",
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'PUT',
            dataType: 'json',
            data: message,

            success:function(response)
            {
                var clase;
                if(response['response'] == "0")
                {
                    clase="chat sending";
                }
                else
                { 
                    clase="chat receiving";
                }
                $(".chat").last().after('<div id="message_'+response['id_message']+'" class="'+clase+'">'+message_text+'</div>');
                
                $("#message_text").val("");
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado.");
            }
        });
    }
    return false;
}

function refresh()
{
    var current_chat = $("#current_chat").val();

    var chat = 
    {
        id_chat : current_chat,          
    };
    $.ajax
        ({
            url: "/messages",
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'POST',
            dataType: 'json',
            data: chat,

            success:function(response)
            {
                if($("#message_"+response['id_message']).length < 1)
                {  
                    var clase;
                    if(response['response']=="1")
                    {
                        clase="chat receiving";
                    }
                    else
                    { 
                        clase="chat sending";
                    }
                    id="message_{{ $message->id_message }}"
                    $(".chat").last().after('<div id="message_'+response['id_message']+'" class="'+clase+'">'+response['text']+'</div>');
                }
                
            },

            error:function()
            {
                //showErrorMessage("Ha habido un error inesperado.");
            }
        });    
}

$("#message_text").on('keyup', function (e) {
    if (e.keyCode == 13) {
        message_send()
    }
});

$("#send_button").click('keyup', function (e) 
{
    message_send()
});

$("#rate_button").click('keyup', function (e) 
{
    rate()
});

$(window).load(function(){
    $("body").animate({ scrollTop: $(document).height()}, 10);    
});

setInterval('refresh()',1000);