$(function () {
    if($("#bar_chart").length > 0)
    {
        new Chart(document.getElementById("bar_chart").getContext("2d"), getChartJs('bar'));
    }

    if($("#radar_chart").length > 0)
    {
        new Chart(document.getElementById("radar_chart").getContext("2d"), getChartJs('radar'));
    }
    //new Chart(document.getElementById("bar_chart").getContext("2d"), getChartJs('bar'));
    //new Chart(document.getElementById("radar_chart").getContext("2d"), getChartJs('radar'));
});