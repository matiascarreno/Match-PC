//Al cargar la página se llena la tabla
$(document).ready(function(){
    list();
    $('select').selectpicker('refresh');
});

function create()
{
    
    $('#form_validation').validate();

    if($('#form_validation').valid())
    {
        var id_store = $("#id_store").val();
        var name = $("#name").val();
        var website = $("#website").val();
        var description = $("#description").val();
        var active = $("#active").val();

        var store = 
        {
            id_store : id_store,
            name : name,
            website : website,
            description : description,
            active : active
        };

        $.ajax
        ({
            url: "/stores",
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'POST',
            dataType: 'json',
            data: store,

            success:function(response)
            {
                if(response['success'])
                {

                    showSuccessMessage(response['message']);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    return false;
}


function edit()
{
    if($("#id_store").val()!=null && $("#id_store").val()!="")
    {
        $('#form_validation').validate();
        if($('#form_validation').valid())
        {
            var id_store = $("#id_store").val();
            var name = $("#name").val();
            var website = $("#website").val();
            var description = $("#description").val();
            var active = $("#active").val();
            var store = 
            {
                id_store : id_store,
                name : name,
                website : website,
                description : description,
                active : active
            };
            
            $.ajax
            ({
                url: "/stores/"+id_store,
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'PUT',
                dataType: 'json',
                data: store,

                success:function(response)
                {
                    if(response['success'])
                    {
                        showSuccessMessage(response['message']);
                        list();
                    }
                    else
                    {
                        showErrorMessage(response['message'], response['description']);
                    }
                    
                },

                error:function()
                {
                    showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
                }
            });
        }
        return false;
    }
    else
    {
        showErrorMessage('Dede haber in ID de categoría válido para editar.');
    }
}

function drop()
{
    if($("#id_store").val()!=null && $("#id_store").val()!="")
    {
        var id_store = $("#id_store").val();

        var store = 
        {
            id_store : id_store,
        };

        $.ajax
        ({
            url: "/stores/"+id_store,
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'DELETE',
            dataType: 'json',
            data: store,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message'], response['description']);
                    $("#id_store").val("");

                    $("#edit_button").attr('disabled', true);
                    $("#delete_button").attr('disabled', true);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    else
    {
        showErrorMessage('Dede haber in ID de tienda válido para editar.');
    }
    
    return false;
}

function search(id_store)
{
    var route = "/stores/"+id_store; 
    $.get(route, function(res)
    {
        $(res).each(function(key, value)
        {
            $("#id_store").val(value.id_store);
            $("#name").val(value.name);
            $("#description").val(value.description);
            $("#website").val(value.website);
            $("#active").val(value.active);
            
            $('select').selectpicker('refresh');

            $("button").removeAttr('disabled');
        }
        );
    }
    ); 
    window.scrollTo(0,0);   
}

function list()
{
    var table = $('table').DataTable();
    table.clear();
    $.get("/listStores", function(res)
    {
        $(res).each(function(key, value)
        {
            switch(parseInt(value.active))
            {
                case 0:
                    var active = '<i class="material-icons">close</i>';
                    break;

                case 1:
                    var active = '<i class="material-icons">check</i>';
                    break;
            }
            
            var rowNode = table.row.add( [
                value.id_store,
                value.name,
                value.description,
                value.website,
                active,
                '<i class="material-icons" onClick="search(' + value.id_store + ')">search</i>'
            ] ).draw(false).node();

            $(rowNode).find('td').eq(0).addClass('text-center');
            $(rowNode).find('td').eq(4).addClass('text-center');
            $(rowNode).find('td').eq(5).addClass('text-center').find('i').css('cursor', 'pointer');
        }
        );
    }
    );
}