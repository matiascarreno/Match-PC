//Al cargar la página se llena la tabla
$(document).ready(function(){
    list();
    $('select').selectpicker('refresh');

});

function create()
{
    $('#form_validation').validate();

    if($('#form_validation').valid())
    {
        var id_product = $("#id_product").val();
        var name = $("#name").val();

        tinyMCE.triggerSave();
        var description = $("#tinymce").val();
        var picture = $("#image").val();
        var category = $("#category").val();
        var active = $("#active").val();

        var cpu = $("#cpu").val();
        var gpu = $("#gpu").val();
        var ram = $("#ram").val();
        var ssd = $("#ssd").val();
        var hdd = $("#hdd").val();

        var screen_res = $("#scr").val();
        var screen_size = $("#scz").val();
        
        if($("#other").val() != null)
        {
            var other = $("#other").val().join(",");
        }
        else
        {
            var other = "";
        }

        var stores = [];

        $(".products_stores").each(function()
        {
            stores.push({
                id_store: $(this).attr("id"),
                link: $("#link_"+$(this).attr("id")).val(),
                price: $("#price_"+$(this).attr("id")).val()
            });
        });

        var product = 
        {
            id_product : id_product,
            name : name,
            description : description,
            picture : picture,
            category : category,
            active : active,
            cpu : cpu,
            gpu : gpu,
            ram : ram,
            ssd : ssd,
            hdd : hdd,
            screen_res : screen_res,
            screen_size : screen_size,
            other : other,
            stores : stores
        };

        $.ajax
        ({
            url: "/products",
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'POST',
            dataType: 'json',
            data: product,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message']);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    return false;
}

function edit()
{
    if($("#id_product").val()!=null & $("#id_product").val()!="")
    {
        $('#form_validation').validate();

        if($('#form_validation').valid())
        {
            var id_product = $("#id_product").val();
            var name = $("#name").val();

            tinyMCE.triggerSave();
            var description = $("#tinymce").val();
            var picture = $("#image").val();
            var category = $("#category").val();
            var active = $("#active").val();

            var cpu = $("#cpu").val();
            var gpu = $("#gpu").val();
            var ram = $("#ram").val();
            var ssd = $("#ssd").val();
            var hdd = $("#hdd").val();

            var screen_res = $("#scr").val();
            var screen_size = $("#scz").val();
            
            if($("#other").val() != null)
            {
                var other = $("#other").val().join(",");
            }
            else
            {
                var other = "";
            }

            var stores = [];

            $(".products_stores").each(function()
            {
                stores.push({
                    id_store: $(this).attr("id"),
                    link: $("#link_"+$(this).attr("id")).val(),
                    price: $("#price_"+$(this).attr("id")).val()
                });
            });

            var product = 
            {
                id_product : id_product,
                name : name,
                description : description,
                picture : picture,
                category : category,
                active : active,
                cpu : cpu,
                gpu : gpu,
                ram : ram,
                ssd : ssd,
                hdd : hdd,
                screen_res : screen_res,
                screen_size : screen_size,
                other : other,
                stores : stores
            };

            $.ajax
            ({
                url: "/products/"+id_product,
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'PUT',
                dataType: 'json',
                data: product,

                success:function(response)
                {
                    if(response['success'])
                    {
                        showSuccessMessage(response['message']);
                        list();
                    }
                    else
                    {
                        showErrorMessage(response['message'], response['description']);
                    }
                    
                },

                error:function()
                {
                    showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
                }
            })
        }
        return false;
    }
    else
    {
        showErrorMessage('Dede haber in ID de categoría válido para editar.');
    }
}

function drop()
{
    if($("#id_product").val()!=null && $("#id_product").val()!="")
    {
        var id_product = $("#id_product").val();

        var product = 
        {
            id_product : id_product,
        };

        $.ajax
        ({
            url: "/products/"+id_product,
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'DELETE',
            dataType: 'json',
            data: product,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message'], response['description']);
                    $("#id_product").val("");
                    $("#edit_button").attr('disabled', true);
                    $("#delete_button").attr('disabled', true);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    else
    {
        showErrorMessage('Dede haber in ID de producto válido para editar.');
    }
    
    return false;
}

function search(id_product)
{
    $(".products_stores input.form-control").val("");
    var route = "/products/"+id_product; 
    $.get(route, function(res)
    {
        $(res).each(function(key, value)
        {
            $("#id_product").val(value.id_product);
            $("#name").val(value.name);
            
            $(tinymce.get('tinymce').getBody()).html(value.description);

            $("#category").selectpicker('val', value.category);
            $("#image").val(value.picture);
            $("#cpu").selectpicker('val', value.cpu);
            $("#gpu").selectpicker('val', value.gpu);
            $("#ram").selectpicker('val', value.ram);
            $("#ssd").selectpicker('val', value.ssd);
            $("#hdd").selectpicker('val', value.hdd);
            $("#scr").selectpicker('val', value.screen_res);
            $("#scz").selectpicker('val', value.screen_size);
            $("#other").selectpicker('val', value.other.split(","));

            $("#active").selectpicker('val' ,value.active);

            if(value.link != null || value.price != null)
            {
                $("#link_"+value.id_store).val(value.link);
                $("#price_"+value.id_store).val(value.price);
            }
            
            $("button").removeAttr('disabled');
            
        }
        );
    }
    );    
    window.scrollTo(0,0);
}

function list()
{
    var table = $('table').DataTable();
    table.clear();
    $.get("/listProducts", function(res)
    {
        $(res).each(function(key, value)
        {
            switch(parseInt(value.active))
            {
                case 0:
                    var active = '<i class="material-icons">close</i>';
                    break;

                case 1:
                    var active = '<i class="material-icons">check</i>';
                    break;
            }

            var ssd = "No_SSD";
            var hdd = "No_HDD";

            if(value.ssd_name != null)
            {
                ssd = value.ssd_name;
            }
            if(value.hdd_name != null)
            {
                hdd = value.hdd_name;
            }
            
            var rowNode = table.row.add( [
                value.id_product,
                value.name,
                value.category_name + ' (ID: ' + value.category + ')',
                value.cpu_name + ' // ' + value.gpu_name + ' // ' + value.ram_name + " RAM" + ' // ' + ssd + ' // ' + hdd,
                active,
                '<i class="material-icons" onClick="search(' + value.id_product + ')">search</i>'
            ] ).draw(false).node();

            $(rowNode).find('td').eq(0).addClass('text-center');
            $(rowNode).find('td').eq(4).addClass('text-center');
            $(rowNode).find('td').eq(5).addClass('text-center').find('i').css('cursor', 'pointer');
        }
        );
    }
    );
}