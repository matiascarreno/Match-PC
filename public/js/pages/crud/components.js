//Al cargar la página se llena la tabla
$(document).ready(function(){
    list();
    $('select').selectpicker('refresh');

    $(".benchmark").hide();
});

function create()
{
    $('#form_validation').validate();

    if($('#form_validation').valid())
    {
        var id_component = $("#id_component").val();
        var name = $("#name").val();
        var description = $("#description").val();
        var type = $("#type").val();
        var freq = $("#freq").val();
        var cores = $("#cores").val();
        var memory = $("#memory").val();
        var active = $("#active").val();

        var scores = [];

        $(".benchmark_"+type).each(function()
        {
            scores.push({
                id_benchmark: $(this).attr("id"),
                score: $(this).val()
            });
        });

        var component = 
        {
            id_component : id_component,
            name : name,
            description : description,
            type : type,
            freq: freq,
            cores : cores,
            memory : memory,
            scores : scores,
            active : active
        };

        $.ajax
        ({
            url: "/components",
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'POST',
            dataType: 'json',
            data: component,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message']);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    return false;
}

function edit()
{
    if($("#id_component").val()!=null && $("#id_component").val()!="")
    {
        $('#form_validation').validate();

        if($('#form_validation').valid())
        {
            var id_component = $("#id_component").val();
            var name = $("#name").val();
            var description = $("#description").val();
            var type = $("#type").val();
            var freq = $("#freq").val();
            var cores = $("#cores").val();
            var memory = $("#memory").val();
            var active = $("#active").val();

            var scores = [];

            $(".benchmark_"+type).each(function()
            {
                scores.push({
                    id_benchmark: $(this).attr("id"),
                    score: $(this).val()
                });
            });

            var component = 
            {
                id_component : id_component,
                name : name,
                description : description,
                type : type,
                freq: freq,
                cores : cores,
                memory : memory,
                scores : scores,
                active : active
            };

            $.ajax
            ({
                url: "/components/"+id_component,
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'PUT',
                dataType: 'json',
                data: component,

                success:function(response)
                {
                    if(response['success'])
                    {
                        showSuccessMessage(response['message']);
                        list();
                    }
                    else
                    {
                        showErrorMessage(response['message'], response['description']);
                    }
                    
                },

                error:function()
                {
                    showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
                }
            });
        }
        return false;
    }
    else
    {
        showErrorMessage('Dede haber in ID de componente válido para editar.');
    }
}

function drop()
{
    if($("#id_component").val()!=null && $("#id_component").val()!="")
    {
        var id_component = $("#id_component").val();

        var component = 
        {
            id_component : id_component,
        };

        $.ajax
        ({
            url: "/components/"+id_component,
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'DELETE',
            dataType: 'json',
            data: component,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message'], response['description']);
                    $("#id_component").val("");
                    $("#edit_button").attr('disabled', true);
                    $("#delete_button").attr('disabled', true);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    else
    {
        showErrorMessage('Dede haber in ID de componente válido para editar.');
    }
    
    return false;
}

function search(id_component)
{
    $(".benchmark input.form-control").val("");

    var route = "/components/"+id_component; 
    $.get(route, function(res)
    {
        $(res).each(function(key, value)
        {
            $("#id_component").val(value.id_component);
            $("#name").val(value.name);
            $("#description").val(value.description);
            $("#type").selectpicker('val' ,value.type);
            $("#freq").val(value.freq);
            $("#cores").val(value.cores);
            $("#memory").val(value.memory);
            $("#active").selectpicker('val', value.active);

            $("#"+value.id_benchmark).val(value.score);


            $("button").removeAttr('disabled');
        }
        );

        disable_specs();
    }
    );
    window.scrollTo(0,0);    
}

function list()
{
    var table = $('table').DataTable();
    table.clear();

    $.get( "/listComponents", function(res)
    {
        $(res).each(function(key, value)
        {
            switch(parseInt(value.active))
            {
                case 0:
                    var active = '<i class="material-icons">close</i>';
                    break;

                case 1:
                    var active = '<i class="material-icons">check</i>';
                    break;
            }

            switch(parseInt(value.type))
            {
                case 1:
                    type = "CPU";
                    break;

                case 2:
                    type = "GPU";
                    break;

                case 3:
                    type = "RAM";
                    break;

                case 4:
                    type = "SSD";
                    break;

                case 5:
                    type = "HDD";
                    break;
            }

            var rowNode = table.row.add( [
                value.id_component,
                value.name,
                value.description,
                type,
                value.score,
                active,
                '<i class="material-icons" onClick="search(' + value.id_component + ')">search</i>'
            ] ).draw(false).node();

            $(rowNode).find('td').eq(0).addClass('text-center');
            $(rowNode).find('td').eq(5).addClass('text-center');
            $(rowNode).find('td').eq(4).addClass('text-center');
            $(rowNode).find('td').eq(6).addClass('text-center').find('i').css('cursor', 'pointer');
        }
        );
    }
    );
} 

function disable_specs()
{
    var type = $('#type').val();
    //$(".benchmark input.form-control").val("");
    $("#freq").attr('disabled', false);
    $("#cores").attr('disabled', false);
    $("#memory").attr('disabled', false);
    $(".benchmark").hide();

    switch(parseInt(type))
    {
        case 1:
            $("#memory").attr('disabled', true);
            $("#memory").val("");

            $(".benchmark.type_1").show();
            break;
            
        case 2:
            $(".benchmark.type_2").show();
            
            break;
            
        case 3:
            $("#cores").attr('disabled', true);
            $("#cores").val("");
            $(".benchmark.type_3").show();
            break;
            
        case 4:
            $("#freq").attr('disabled', true);
            $("#freq").val("");
            $("#cores").attr('disabled', true);
            $("#cores").val("");

            $(".benchmark.type_4").show();
            break;
            
        case 5:
            $("#freq").attr('disabled', true);
            $("#freq").val("");
            $("#cores").attr('disabled', true);
            $("#cores").val("");

            $(".benchmark.type_5").show();
            break;
    }
}