//Al cargar la página se llena la tabla
$(document).ready(function(){
    list();
    $('select').selectpicker('refresh');
});

function create()
{
    $('#form_validation').validate();

    if($('#form_validation').valid())
    {
        var id_category = $("#id_category").val();
        var name = $("#name").val();
        var description = $("#description").val();
        var active = $("#active").val();

        var category = 
        {
            id_category : id_category,
            name : name,
            description : description,
            active : active
        };

        $.ajax
        ({
            url: "/categories",
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'POST',
            dataType: 'json',
            data: category,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message']);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    return false;
}

function edit()
{
    if($("#id_category").val()!=null && $("#id_category").val()!="")
    {
        $('#form_validation').validate();

        if($('#form_validation').valid())
        {
            var id_category = $("#id_category").val();
            var name = $("#name").val();
            var description = $("#description").val();
            var active = $("#active").val();

            var category = 
            {
                id_category : id_category,
                name : name,
                description : description,
                active : active
            };

            $.ajax
            ({
                url: "/categories/"+id_category,
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'PUT',
                dataType: 'json',
                data: category,

                success:function(response)
                {
                    if(response['success'])
                    {
                        showSuccessMessage(response['message']);
                        list();
                    }
                    else
                    {
                        showErrorMessage(response['message'], response['description']);
                    }
                    
                },

                error:function()
                {
                    showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
                }
            });
        }
        return false;
    }
    else
    {
        showErrorMessage('Dede haber in ID de categoría válido para editar.');
    }
}

function drop()
{
    if($("#id_category").val()!=null && $("#id_category").val()!="")
    {
        var id_category = $("#id_category").val();

        var category = 
        {
            id_category : id_category,
        };

        $.ajax
        ({
            url: "/categories/"+id_category,
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'DELETE',
            dataType: 'json',
            data: category,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message'], response['description']);
                    $("#id_category").val("");
                    $("#edit_button").attr('disabled', true);
                    $("#delete_button").attr('disabled', true);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    else
    {
        showErrorMessage('Dede haber in ID de categoría válido para editar.');
    }
    
    return false;
}

function search(id_category)
{
    var route = "/categories/"+id_category; 
    $.get(route, function(res)
    {
        $(res).each(function(key, value)
        {
            $("#id_category").val(value.id_category);
            $("#name").val(value.name);
            $("#description").val(value.description);
            $("#active").val(value.active);
            
            $('select').selectpicker('refresh');

            $("button").removeAttr('disabled');
        }
        );
    }
    );  
    window.scrollTo(0,0);  
}

function list()
{
    var table = $('table').DataTable();
    table.clear();

    $.get( "/listCategories", function(res)
    {
        $(res).each(function(key, value)
        {
            switch(parseInt(value.active))
            {
                case 0:
                    var active = '<i class="material-icons">close</i>';
                    break;

                case 1:
                    var active = '<i class="material-icons">check</i>';
                    break;
            }
            
            var rowNode = table.row.add( [
                value.id_category,
                value.name,
                value.description,
                active,
                '<i class="material-icons" onClick="search(' + value.id_category + ')">search</i>'
            ] ).draw(false).node();

            $(rowNode).find('td').eq(0).addClass('text-center');
            $(rowNode).find('td').eq(3).addClass('text-center');
            $(rowNode).find('td').eq(4).addClass('text-center').find('i').css('cursor', 'pointer');
        }
        );
    }
    );
} 