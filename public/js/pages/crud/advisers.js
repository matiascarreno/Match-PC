//Al cargar la página se llena la tabla

$(document).ready(function(){
    list();
    $('select').selectpicker('refresh');
});

function create()
{
    $('#form_validation').validate();

    if($('#form_validation').valid())
    {
        var id_user = $("#id_user").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var name = $("#name").val();
        var lastname = $("#lastname").val();
        var phone_number = $("#phone_number").val();
        var gender;

        if($("#gender").val()==="null")
        {
            gender = null;
        }
        else
        {
            gender = $("#gender").val();
        }

        var birthdate = $("#birthdate").val();
        var active = $("#active").val();

        var adviser = 
        {
            id_user : id_user,
            email : email,
            password : password,
            name : name,
            lastname : lastname,
            phone_number : phone_number,
            gender : gender,
            birthdate : birthdate,
            active : active
        };

        $.ajax
        ({
            url: "/advisers",
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'POST',
            dataType: 'json',
            data: adviser,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message'], response['description']);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    return false;
}

function edit()
{
    if($("#id_user").val()!=null && $("#id_user").val()!="")
    {
        $('#form_validation').validate();

        if($('#form_validation').valid())
        {
        var id_user = $("#id_user").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var name = $("#name").val();
        var lastname = $("#lastname").val();
        var phone_number = $("#phone_number").val();
        var gender;

        if($("#gender").val()==="null")
        {
            gender = null;
        }
        else
        {
            gender = $("#gender").val();
        }

        var birthdate = $("#birthdate").val();
        var last_purchase = $("#last_purchase").val();
        var subscription = $("#subscription").val();
        var active = $("#active").val();

        var adviser = 
        {
            id_user : id_user,
            email : email,
            password : password,
            name : name,
            lastname : lastname,
            phone_number : phone_number,
            gender : gender,
            birthdate : birthdate,
            subscription : subscription,
            last_purchase : last_purchase,
            active : active
        };

            $.ajax
            ({
                url: "/advisers/"+id_user,
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'PUT',
                dataType: 'json',
                data: adviser,

                success:function(response)
                {
                    if(response['success'])
                    {
                        showSuccessMessage(response['message']);
                        list();
                    }
                    else
                    {
                        showErrorMessage(response['message'], response['description']);
                    }
                    
                },

                error:function()
                {
                    showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
                }
            });
        }
        return false;
    }
    else
    {
        showErrorMessage('Dede haber in ID de benchmark válido para editar.');
    }
}

function drop()
{
    if($("#id_user").val()!=null && $("#id_user").val()!="")
    {
        var id_user = $("#id_user").val();

        var user = 
        {
            id_user : id_user,
        };

        $.ajax
        ({
            url: "/advisers/"+id_user,
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'DELETE',
            dataType: 'json',
            data: user,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message'], response['description']);
                    $("#id_user").val("");
                    $("#edit_button").attr('disabled', true);
                    $("#delete_button").attr('disabled', true);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    else
    {
        showErrorMessage('Dede haber in ID de benchmark válido para editar.');
    }
    
    return false;
}

function search(id_user)
{
    var route = "/advisers/"+id_user; 
    $.get(route, function(res)
    {
        $(res).each(function(key, value)
        {
            $("#id_user").val(value.id_user);
            $("#email").val(value.email);
            $("#name").val(value.name);
            $("#lastname").val(value.lastname);
            $("#phone_number").val(value.phone_number);
            $("#birthdate").val(value.birthdate.split(" ")[0]);   

            $("#gender").val(value.gender);
            $("#active").val(value.active);
            
            $('select').selectpicker('refresh');

            $("button").removeAttr('disabled');
        }
        );
    }
    );  
    window.scrollTo(0,0);  
}

function list()
{
    var table = $('table').DataTable();
    table.clear();
    $.get("/listAdvisers", function(res)
    {
        $(res).each(function(key, value)
        {
            //Valor a active
            switch(parseInt(value.active))
            {
                case 0:
                    var active = '<i class="material-icons">close</i>';
                    break;

                case 1:
                    var active = '<i class="material-icons">check</i>';
                    break;
            }

            //valor a Género
            switch(value.gender)
            {
                case null:
                var gender = 'Desconocido';
                    break;

                case "O":
                var gender = 'Otro';
                    break;

                case "F":
                var gender = 'Femenino';
                    break;

                case "M":
                var gender = 'Masculino';
                    break;
            }

            var rowNode = table.row.add( [
                value.id_user,
                value.email,
                value.name,
                value.lastname,
                value.phone_number,
                value.birthdate.split(" ")[0],
                gender,
                active,
                '<i class="material-icons" onClick="search(' + value.id_user + ')">search</i>'
            ] ).draw(false).node();

            $(rowNode).find('td').eq(0).addClass('text-center');
            $(rowNode).find('td').eq(7).addClass('text-center');
            $(rowNode).find('td').eq(8).addClass('text-center').find('i').css('cursor', 'pointer');
        }
        );
    }
    );
}
