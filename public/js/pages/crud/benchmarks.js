//Al cargar la página se llena la tabla
$(document).ready(function(){
    list();
    $('select').selectpicker('refresh');
});


function create()
{
    $('#form_validation').validate();

    if($('#form_validation').valid())
    {
        var id_benchmark = $("#id_benchmark").val();
        var name = $("#name").val();
        var description = $("#description").val();
        var component_type = $("#type").val();
        var max_score = $("#max_score").val();
        var min_score = $("#min_score").val();
        var incremental = $("#incremental").val();
        var active = $("#active").val();

        var benchmark = 
        {
            id_benchmark : id_benchmark,
            name : name,
            description : description,
            component_type : component_type,
            max_score : max_score,
            min_score : min_score,
            incremental : incremental,
            active : active
        };

        $.ajax
        ({
            url: "/benchmarks",
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'POST',
            dataType: 'json',
            data: benchmark,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message']);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    return false;
}


function edit()
{
    if($("#id_benchmark").val()!=null && $("#id_benchmark").val()!="")
    {
        $('#form_validation').validate();

        if($('#form_validation').valid())
        {
            var id_benchmark = $("#id_benchmark").val();
            var name = $("#name").val();
            var description = $("#description").val();
            var component_type = $("#type").val();
            var max_score = $("#max_score").val();
            var min_score = $("#min_score").val();
            var incremental = $("#incremental").val();
            var active = $("#active").val();

            var benchmark = 
            {
                id_benchmark : id_benchmark,
                name : name,
                description : description,
                component_type : component_type,
                max_score : max_score,
                min_score : min_score,
                incremental : incremental,
                active : active
            };

            $.ajax
            ({
                url: "/benchmarks/"+id_benchmark,
                headers: {'X-CSRF-TOKEN': $("#token").val()},
                type: 'PUT',
                dataType: 'json',
                data: benchmark,

                success:function(response)
                {
                    if(response['success'])
                    {
                        showSuccessMessage(response['message']);
                        list();
                    }
                    else
                    {
                        showErrorMessage(response['message'], response['description']);
                    }
                    
                },

                error:function()
                {
                    showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
                }
            });
        }
        return false;
    }
    else
    {
        showErrorMessage('Dede haber in ID de benchmark válido para editar.');
    }
}

function drop()
{
    if($("#id_benchmark").val()!=null && $("#id_benchmark").val()!="")
    {
        var id_benchmark = $("#id_benchmark").val();

        var category = 
        {
            id_benchmark : id_benchmark,
        };

        $.ajax
        ({
            url: "/benchmarks/"+id_benchmark,
            headers: {'X-CSRF-TOKEN': $("#token").val()},
            type: 'DELETE',
            dataType: 'json',
            data: category,

            success:function(response)
            {
                if(response['success'])
                {
                    showSuccessMessage(response['message'], response['description']);
                    $("#id_benchmark").val("");
                    $("#edit_button").attr('disabled', true);
                    $("#delete_button").attr('disabled', true);
                    list();
                }
                else
                {
                    showErrorMessage(response['message'], response['description']);
                }
                
            },

            error:function()
            {
                showErrorMessage("Ha habido un error inesperado en la consulta Ajax.");
            }
        });
    }
    else
    {
        showErrorMessage('Dede haber in ID de benchmark válido para editar.');
    }
    
    return false;
}

function search(id_benchmark)
{
    var route = "/benchmarks/"+id_benchmark; 
    $.get(route, function(res)
    {
        $(res).each(function(key, value)
        {
            $("#id_benchmark").val(value.id_benchmark);
            $("#name").val(value.name);
            $("#description").val(value.description);
            $("#type").selectpicker('val', value.component_type);
            $("#max_score").val(value.max_score);
            $("#min_score").val(value.min_score);
            $("#incremental").val(value.incremental);
            $("#active").selectpicker('val', value.active);
            
            $("button").removeAttr('disabled');
        }
        );
    }
    );
    window.scrollTo(0,0);    
}

function list()
{
    var table = $('table').DataTable();
    table.clear();
    $.get("/listBenchmarks", function(res)
    {
        $(res).each(function(key, value)
        {
            //Valor si está activo
            switch(parseInt(value.active))
            {
                case 0:
                    var active = '<i class="material-icons">close</i>';
                    break;

                case 1:
                    var active = '<i class="material-icons">check</i>';
                    break;
            }
            //Valor si es incremental
            switch(parseInt(value.incremental))
            {
                case 0:
                    var incremental = '<i class="material-icons">trending_down</i>';
                    break;

                case 1:
                    var incremental = '<i class="material-icons">trending_up</i>';
                    break;
            }

            var rowNode = table.row.add( [
                value.id_benchmark,
                value.name,
                value.description,
                value.min_score,
                value.max_score,
                incremental,
                active,
                '<i class="material-icons" onClick="search(' + value.id_benchmark + ')">search</i>'
            ] ).draw(false).node();

            $(rowNode).find('td').eq(0).addClass('text-center');
            $(rowNode).find('td').eq(5).addClass('text-center');
            $(rowNode).find('td').eq(6).addClass('text-center');
            $(rowNode).find('td').eq(7).addClass('text-center').find('i').css('cursor', 'pointer');
        }
        );
    }
    );
}
