$(function () {

    if($(".colorpicker").length > 0)
    {
        $('.colorpicker').colorpicker();
    }

    
    $('#optgroup_typePC').multiSelect({
        afterSelect: function(values){
          //alert("Select value: "+values);
          console.log($('#optgroup_typePC').val());
        },
        afterDeselect: function(values){
          //alert("Deselect value: "+values);
          console.log($('#optgroup_typePC').val());
        }
      });

    //Dropzone
/*     Dropzone.options.frmFileUpload = {
        paramName: "file",
        maxFilesize: 2
    }; */

    //Masked Input ============================================================================================================================
    if($(".demo-masked-input").length > 0)
    {
    
    var $demoMaskedInput = $('.demo-masked-input'); 

    //Date
    $demoMaskedInput.find('.date').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' });

    //Time
    $demoMaskedInput.find('.time12').inputmask('hh:mm t', { placeholder: '__:__ _m', alias: 'time12', hourFormat: '12' });
    $demoMaskedInput.find('.time24').inputmask('hh:mm', { placeholder: '__:__ _m', alias: 'time24', hourFormat: '24' });

    //Date Time
    $demoMaskedInput.find('.datetime').inputmask('d/m/y h:s', { placeholder: '__/__/____ __:__', alias: "datetime", hourFormat: '24' });

    //Mobile Phone Number
    $demoMaskedInput.find('.mobile-phone-number').inputmask('+99 (999) 999-99-99', { placeholder: '+__ (___) ___-__-__' });
    //Phone Number
    $demoMaskedInput.find('.phone-number').inputmask('+99 (999) 999-99-99', { placeholder: '+__ (___) ___-__-__' });

    //Dollar Money
    $demoMaskedInput.find('.money-dollar').inputmask('99,99 $', { placeholder: '__,__ $' });
    //Euro Money
    $demoMaskedInput.find('.money-euro').inputmask('99,99 €', { placeholder: '__,__ €' });

    //IP Address
    $demoMaskedInput.find('.ip').inputmask('999.999.999.999', { placeholder: '___.___.___.___' });

    //Credit Card
    $demoMaskedInput.find('.credit-card').inputmask('9999 9999 9999 9999', { placeholder: '____ ____ ____ ____' });

    //Email
    $demoMaskedInput.find('.email').inputmask({ alias: "email" });

    //Serial Key
    $demoMaskedInput.find('.key').inputmask('****-****-****-****', { placeholder: '____-____-____-____' });

    }
    //===========================================================================================================================================

    //Multi-select
    $('#optgroup_type_pc').multiSelect({ selectableOptgroup: true });
    $('#optgroup_games').multiSelect({ selectableOptgroup: true });
    $('#optgroup_programs').multiSelect({ selectableOptgroup: true });

    //noUISlider
    var sliderBasic = document.getElementById('nouislider_basic_example');
    noUiSlider.create(sliderBasic, {
        start: [500000],
        connect: 'lower',
        step: 1,
        range: {
            'min': [50000],
            'max': [1500000]
        }
    });
    getNoUISliderValue(sliderBasic, true);
});

$('#budget').val();
//Get noUISlider Value and write on
function getNoUISliderValue(slider, percentage) {
    slider.noUiSlider.on('update', function () {
        var val = slider.noUiSlider.get();
        if (percentage) {
            val = parseInt(val);
        }
        if(val!=1500000)
        {
            $(slider).parent().find('span.js-nouislider-value').text("$"+formatNumber(val));  
            $('#budget').val(val); 
        }
        else
        {
            $(slider).parent().find('span.js-nouislider-value').text("$"+formatNumber(val)+" o más");
            $('#budget').val(val); 
        }
        
    });
}

function formatNumber(num) {
    if (!num || num == 'NaN') return '-';
    if (num == 'Infinity') return '&#x221e;';
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num);
}

$('#form').submit(function() 
{
    $('#type_pc').val($('#optgroup_type_pc').val());
    $('#programs').val($('#optgroup_programs').val());
    $('#games').val($('#optgroup_games').val());
    $('#other').val($('#select_other').val());
    return true; // return false to cancel form action
});