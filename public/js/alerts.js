//These codes takes from http://t4t5.github.io/sweetalert/

function showSuccessMessage(message, description) {
    swal(message, description, "success");
}

function showErrorMessage(message, description) {
    swal(message, description, "error");
}

function showWarningMessage(message, description) {
    swal(message, description, "warning");
}