<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benchmark extends Model
{
    protected $primaryKey = 'id_benchmark';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'component_type',
        'min_score',
        'max_score',
        'incremental',
        'active',
    ];

    public function isActive()
    {
        if($this->active == 1 || $this->active == "1")
        {
            return true;
        }
        return false;
    }
}
