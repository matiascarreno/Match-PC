<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products_store extends Model
{
    protected $primaryKey = 'id_product_store';

    public $timestamps = false;

    protected $fillable = [
        'id_product',
        'id_store',
        'price',
        'link',
        'date',
        'active',
    ];

    public function isActive()
    {
        if($this->active == 1 || $this->active == "1")
        {
            return true;
        }
        return false;
    }
}
