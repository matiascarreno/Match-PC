<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $primaryKey = 'id_transaction';

    public $timestamps = false;

    protected $fillable = [
        'tokenws',
        'buy_order',
        'id_user',
        'id_subscription',
        'date',
        'approved'
    ];

    public function isApproved()
    {
        if($this->approved === 1 || $this->approved === "1")
        {
            return true;
        }
        return false;
    } 
}
