<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $primaryKey = 'id_chat';

    public $timestamps = false;

    protected $fillable = [
        'id_user',
        'id_adviser',
        'id_quotation',
        'last_read_message',
        'start_date',
        'active',
    ];

    public function isActive()
    {
        if($this->active == 1 || $this->active == "1")
        {
            return true;
        }
        return false;
    }
}
