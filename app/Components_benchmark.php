<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Components_benchmark extends Model
{
    protected $primaryKey = 'id_component_benchmark';

    public $timestamps = false;

    protected $fillable = [
        'id_component',
        'id_benchmark',
        'score',
        'active',
    ];

    public function isActive()
    {
        if($this->active == 1 || $this->active == "1")
        {
            return true;
        }
        return false;
    }
}
