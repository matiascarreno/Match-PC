<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id_user';

    public $timestamps = false;

    protected $fillable = [
        'email',
        'password',
        'remember_token',
        'name',
        'lastname',
        'picture',
        'cover_image',
        'theme',
        'phone_number',
        'birthdate',
        'gender',
        'subscription',
        'last_purchase',
        'privileges',
        'active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isFree()
    {
        if($this->subscription === 1 || $this->subscription === "1")
        {
            return true;
        }
        return false;
    } 

    public function isLite()
    {
        if($this->subscription === 2 || $this->subscription === "2")
        {
            return true;
        }
        return false;
    }

    public function isPremium()
    {
        if($this->subscription === 3 || $this->subscription === "3")
        {
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        if($this->privileges === 1 || $this->privileges === "1")
        {
            return true;
        }
        return false;
    } 

    public function isAdviser()
    {
        if($this->privileges === 0 || $this->privileges === "0")
        {
            return true;
        }
        return false;
    }

    public function isUser()
    {
        if($this->privileges === null)
        {
            return true;
        }
        return false;
    }
}
