<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $primaryKey = 'id_message';

    public $timestamps = false;

    protected $fillable = [
        'id_chat',
        'response',
        'text',
        'archive',
        'active'
    ];

    public function isActive()
    {
        if($this->active == 1 || $this->active == "1")
        {
            return true;
        }
        return false;
    }
}
