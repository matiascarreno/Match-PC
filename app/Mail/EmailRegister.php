<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $random;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $random)
    {
        $this->email = $email;
        $this->random = $random;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.activate');
    }
}
