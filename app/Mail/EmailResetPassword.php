<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $remember_token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $remember_token)
    {
        $this->email = $email;
        $this->remember_token = $remember_token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.reset_password');
    }
}
