<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'id_product';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'picture',
        'category',
        'cpu',
        'gpu',
        'ram',
        'ssd',
        'hdd',
        'screen_res',
        'screen_size',
        'other',
        'active',
    ];

    public function isActive()
    {
        if($this->active == 1 || $this->active == "1")
        {
            return true;
        }
        return false;
    }

    public function getScore(String $component)
    {
        switch ($component) 
        {
            case "cpu":
                $benchmark_scores = Components_benchmark::where('id_component', $this->cpu)->get();
                break;
            
            case "gpu":
                $benchmark_scores = Components_benchmark::where('id_component', $this->gpu)->get();

                break;
            
            case "ram":
                $benchmark_scores = Components_benchmark::where('id_component', $this->ram)->get();
                break;
            
            case "ssd":
                    $benchmark_scores = Components_benchmark::where('id_component', $this->ssd)->get();
                break;
            
            case "hdd":
                $benchmark_scores = Components_benchmark::where('id_component', $this->hdd)->get();
                break;
            
            default:
                return null;
                break;
        }
        //dd($benchmark_scores);

        if($benchmark_scores->isEmpty())
        {
            return 0;
        }

        $cont = 0;

        $display_score = 0;

        foreach($benchmark_scores as $benchmark_score)
        {
            $max_score = Components_benchmark::where('id_benchmark', $benchmark_score->id_benchmark)->max('score');
            //$min_score = Components_benchmark::where('id_benchmark', $benchmark_score->id_benchmark)->min('score');
            
            //dd($max_score);
            $score = $benchmark_score->score * (100 / $max_score);

            if(Benchmark::find($benchmark_score->id_benchmark)->incremental == 0)
            {
                $score = 100 - $score;
            }

            $display_score += $score;
            $cont++;
        }

        $display_score = $display_score / $cont;

        return $display_score;
    }

    public function getPrice()
    {
        $price = Products_store::where('id_product', $this->id_product)->min('price');

        return $price;
    }

    public function getStores()
    {
        $stores = Products_store::where('id_product', $this->id_product)->join('stores', 'products_stores.id_store', '=', 'stores.id_store')->orderBy('products_stores.price', 'asc')->get();

        return $stores;
    }
}
