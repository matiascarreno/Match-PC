<?php

namespace App\Http\Controllers;

use App\Benchmark;
use Illuminate\Http\Request;
use Session;

class BenchmarkController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
        $this->middleware('noAdviser');
        $this->middleware('noUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site/pages/crud/benchmarks');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Benchmark::where('name', $request['name'])->first()==null)
        {
            $request['active']=(int)$request['active'];
            $request['incremental']=(int)$request['incremental'];

            try
            {
                if($request['max_score'] < $request['min_score'])
                {
                    return response()->json([
                        'success' => false,
                        'message' => 'La puntuación mínima debe ser menor que la puntuación máxima.'
                    ]);
                }
                $benchmark = new benchmark($request->all());
                
                $benchmark->save();

                return response()->json([
                    'success' => true,
                    'message' => 'Benchmark agregado correctamente'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al agregar el Benchmark.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Ya existe un benchmark con este nombre.'
            ]);
        }
    }

    /**
     * Display all the resources.
     *
     * @param  \App\Benchmark  $category
     * @return \Illuminate\Http\Response
     */
    public function listBenchmarks()
    {
        return response()->json(Benchmark::all()->toArray());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Benchmark  $benchmark
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $benchmark = Benchmark::find($id);
        return response()->json($benchmark->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Benchmark  $benchmark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $benchmark = Benchmark::find($id);
        if($benchmark!=null)
        {
            $request['active']=(int)$request['active'];
            $request['incremental']=(int)$request['incremental'];
            try
            {
                if($request['max_score'] < $request['min_score'])
                {
                    return response()->json([
                        'success' => false,
                        'message' => 'La puntuación mínima debe ser menor que la puntuación máxima.'
                    ]);
                }

                $benchmark->fill($request->all());
                
                $benchmark->update();

                return response()->json([
                    'success' => true,
                    'message' => 'Benchmark editado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al editar el benchmark.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el benchmark que quiere editar.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Benchmark  $benchmark
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $benchmark = Benchmark::find($id);
        if($benchmark!=null)
        {
            try
            {  
                $benchmark->destroy($benchmark['id_benchmark']);

                return response()->json([
                    'success' => true,
                    'message' => 'Benchmark eliminado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al eliminar el benchmark.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el benchmark que quiere eliminar.'
            ]);
        }
    }
}
