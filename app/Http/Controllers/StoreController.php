<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
        $this->middleware('noAdviser');
        $this->middleware('noUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site/pages/crud/stores');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Store::where('name', $request['name'])->orwhere('website', $request['website'])->first() == null)
        {
            $request['active']=(int)$request['active'];
            try
            {
                $store = new store($request->all());
                
                $store->save();

                return response()->json([
                    'success' => true,
                    'message' => 'Tienda agregada correctamente'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al agregar la tienda.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            if(Store::where('name', $request['name'])->first()==null)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Ya existe una tienda con este sitio web.'
                ]);
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Ya existe una tienda con este nombre.'
                ]);
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = Store::find($id);
        return response()->json($store->toArray());
    }

    /**
     * Display all the resources.
     *
     * @param  \App\Store  $category
     * @return \Illuminate\Http\Response
     */
    public function listStores()
    {
        return response()->json(Store::all()->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Store::find($id);
        if($store!=null)
        {
            $request['active']=(int)$request['active'];
            try
            {
                $store->fill($request->all());
                
                $store->update();

                return response()->json([
                    'success' => true,
                    'message' => 'Tienda editada correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al editar la tienda.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe la tienda que quiere editar.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Store::fin($id);
        if($store!=null)
        {
            try
            {  
                $store->destroy($store['id_store']);

                return response()->json([
                    'success' => true,
                    'message' => 'Tienda eliminada correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al eliminar la tienda.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe la tienda que quiere eliminar.'
            ]);
        }
    }
}
