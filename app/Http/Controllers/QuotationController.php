<?php

namespace App\Http\Controllers;

use App\Quotation;
use App\Product;
use Illuminate\Http\Request;
use Redirect;
use Session;

class QuotationController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
        $this->middleware('noAdviser');
        $this->middleware('noAdmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('site/pages/quotations/new_quotation');
    }

    public function new_quotation(Request $request)
    {
        
        //Get priority of each component
        if($request->office + $request->game + $request->content_creation + $request->model != 0)
        {
            $val_per_preference = 1 / ($request->office + $request->game + $request->content_creation + $request->model);

            $priority_office = $val_per_preference * $request->office;
            $priority_game = $val_per_preference * $request->game;
            $priority_creation_cotent = $val_per_preference * $request->content_creation;
            $priority_modeling = $val_per_preference * $request->model;

            /*
                Each use priority:          Ofimatica   Juegos  Creación de contenido   Modelado
                    CPU:                    80%         20%     30%                     60%
                    GPU:                    0%          70%     30%                     20%
                    RAM:                    10%         10%     20%                     20%
                    SSD:                    10%         0%      20%                     0%
                    HDD:                    0%          0%      0%                      0%
            */
            $cpu_priority = $priority_office * 0.8 + $priority_game * 0.2 + $priority_creation_cotent * 0.3 + $priority_modeling * 0.8;
            $gpu_priority = $priority_office * 0.0 + $priority_game * 0.7 + $priority_creation_cotent * 0.3 + $priority_modeling * 0.0;
            $ram_priority = $priority_office * 0.1 + $priority_game * 0.1 + $priority_creation_cotent * 0.2 + $priority_modeling * 0.2;
            $ssd_priority = $priority_office * 0.1 + $priority_game * 0.0 + $priority_creation_cotent * 0.0 + $priority_modeling * 0.0;
            $hdd_priority = $priority_office * 0.0 + $priority_game * 0.0 + $priority_creation_cotent * 0.0 + $priority_modeling * 0.0;
        }
        else
        {
            $cpu_priority = 0.30;
            $gpu_priority = 0.20;
            $ram_priority = 0.30;
            $ssd_priority = 0.10;
            $hdd_priority = 0.10;
        }
        
        //Get all products
        $products = Product::where('active', 1)->get();

        //filter by PC Type
        if($request->type_pc != null)
        {
            $products = $products->filter(function ($product, $key) use ($request) 
            {
                return str_contains($request->type_pc, (string)$product->category);
            });
        }


        //Filter by price
        $products = $products->filter(function ($product, $key) use ($request) 
        {
            return $product->getPrice() <= $request->budget;
        });     
        
        //Ordenar por puntuación
        $products = $products->sortByDesc(function ($product, $key) use ($cpu_priority, $gpu_priority, $ram_priority, $ssd_priority, $hdd_priority){
            return ($product->getScore("cpu")*$cpu_priority) + ($product->getScore("gpu")*$gpu_priority) + ($product->getScore("ram")*$ram_priority) + ($product->getScore("hdd")*$hdd_priority);
        });

        $products = $products->take(4);
        return view('site/pages/quotations/show_quotation', compact('products'));
        
    }

    public function show_new()
    {
        return view('site/pages/quotations/show_quotation');
    }
}
