<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\User;
use Illuminate\Support\Facades\Hash;
use Session;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('noLogged');
    }

    public function index()
    {
        return view('log_/pages/login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials, $request['rememberme']))
        {
            if(Auth::user()->active == 1)
            {
                return Redirect::to('/home');
            }
            else
            {
                Auth::logout();
                session()->regenerate();
                Session::flash('error', 'Debes verificar tu correo electrónico antes de poder acceder.');
                Session::flash('description', 'Por favor inténtalo mas tarde de nuevo.');
                return Redirect::to('/login')->withInput($request->except('password'));
            }
        }
        else
        {
            Session::flash('error', 'Email o contraseña incorrectos.');
            Session::flash('description', 'Por favor inténtalo mas tarde de nuevo.');
            return Redirect::to('/login')->withInput($request->except('password'));
        }        
    }

}
