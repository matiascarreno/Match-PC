<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\EmailResetPassword;
use App\User;
use Mail;
use Session;
use Redirect;

class ResetController extends Controller
{
    public function __construct()
    {
        $this->middleware('noLogged');
    }

    public function index()
    {
        return view('log_/pages/reset');
    }

    public function reset(Request $request)
    {
        $user = User::where('email', $request['email'])->first();

        try
        {
            if($user != null)
            {
                Mail::to($request['email'])->send(new EmailResetPassword($user->email, $user->remember_token));
            }
            
            Session::flash('success', 'Se ha enviado un email a su correo electrónico.');
            Session::flash('description', 'Por favor verifique su correo electrónico para cambiar la contaseña de su cuenta.');
            return Redirect::to('/login');
        }
        catch(Exception $e)
        {
            Session::flash('error', 'Ha ocurrido un error inesperado.');
            Session::flash('description', $e);
            return Redirect::to('/register')->withInput($request->except('password'));
        }

        return view('log_/pages/reset');
    }

    public function new_password(Request $request)
    {
        $user = User::where('email', $request['email'])->where('remember_token', $request['remember_token'])->first();
        if($user != null)
        {
            Session::flash('email', $request['email']);
            Session::flash('remember_token', $request['remember_token']);
            return view('log_/pages/new_password');
        }

        return view('log_/pages/login');        
    }

    public function new_password_restore(Request $request)
    {
        $user = User::where('email', $request['email'])->where('remember_token', $request['remember_token'])->first();
        
        if($user != null)
        {
            $user->password = bcrypt($request['password']);
            $user->update();
            Session::flash('success', 'Se ha reestablecido exitosamente su contraseña.');
            return view('log_/pages/login');
        }

        Session::flash('error', 'Ha ocurrido un error inesperado al restablecer su contraseña.');
        return view('log_/pages/login');        
    }
}
