<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Session;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
        $this->middleware('noAdviser');
        $this->middleware('noUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site/pages/crud/categories');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Category::where('name', $request['name'])->first()==null)
        {
            $request['active']=(int)$request['active'];
            try
            {
                $category = new category($request->all());
                
                $category->save();

                return response()->json([
                    'success' => true,
                    'message' => 'Categoría agregada correctamente'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al agregar la categoría.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Ya existe una categoría con este nombre.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        return response()->json($category->toArray());
    }

    /**
     * Display all the resources.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function listCategories()
    {
        return response()->json(Category::all()->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        if($category!=null)
        {
            $request['active']=(int)$request['active'];
            try
            {
                $category->fill($request->all());
                
                $category->update();

                return response()->json([
                    'success' => true,
                    'message' => 'Categoría editada correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al editar la categoría.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe la categoría que quiere editar.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if($category!=null)
        {
            try
            {  
                $category->destroy($category['id_category']);

                return response()->json([
                    'success' => true,
                    'message' => 'Categoría eliminada correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al eliminar la categoría.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe la categoría que quiere eliminar.'
            ]);
        }
    }
}
