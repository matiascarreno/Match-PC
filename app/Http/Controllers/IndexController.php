<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Chat;
use Session;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
    }

    public function index()
    {
        if(Auth::User()->isUser())
        {
            return view('site/pages/index/user');
        }

        if(Auth::User()->isAdviser())
        {
            $chats = Chat::join('users', 'chats.id_user', '=', 'users.id_user')
            ->select('chats.*', 'users.name as user_name', 'users.subscription as subscription')
            ->orderBy('users.subscription', 'desc')
            ->getQuery()
            ->get();

            session(['chats' => $chats]);

            return view('site/pages/index/adviser');
        }

        if(Auth::User()->IsAdmin())
        {
            return view('site/pages/index/administrator');
        }

        Auth::logout();
        Session::flash('error', 'Hubo un error al ingresar.');
        return Redirect::to('/login');
    }  
}
