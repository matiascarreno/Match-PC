<?php

namespace App\Http\Controllers;

use App\Component;
use App\Components_benchmark;
use App\Benchmark;
use Illuminate\Http\Request;

class ComponentController extends Controller
{

    public function __construct()
    {
        $this->middleware('noUnlogged');
        $this->middleware('noAdviser');
        $this->middleware('noUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $benchmarks = Benchmark::All();
        return view('site/pages/crud/components', compact('benchmarks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Component::where('name', $request['name'])->first()==null)
        {
            $request['active']=(int)$request['active'];
            try
            {
                $component = new Component($request->all());
                
                $component->save();
                
                if($request['scores'] != null)
                {
                    foreach($request['scores'] as $score)
                    {                    
                        if($score["score"] != null)
                        {  
                            $components_benchmark = new Components_benchmark();
                            $components_benchmark->id_component = $component->id_component;
                            $components_benchmark->id_benchmark = $score["id_benchmark"];
                            $components_benchmark->score = $score["score"];
                            $components_benchmark->active = 1;
    
                            $components_benchmark->save();
                        }
                    }
                }

                return response()->json([
                    'success' => true,
                    'message' => 'Componente agregado correctamente'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al agregar el componente.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Ya existe un componente con este nombre.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Components_benchmark::where('id_component', $id)->get()->isEmpty())
        {
            $component = Component::where('id_component', $id)->get();
        }
        else
        {
            
            $component = Component::where('components.id_component', $id)
            ->leftjoin('components_benchmarks', 'components.id_component', '=', 'components_benchmarks.id_component')
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
            ->get();
        }
        return response()->json($component->toArray());
    }

    /**
     * Display all the resources.
     *
     * @param  \App\component  $component
     * @return \Illuminate\Http\Response
     */
    public function listComponents()
    {
        $result = Component::join('components_benchmarks', 'components.id_component', '=', 'components_benchmarks.id_component')
        ->select('components.*', 'components_benchmarks.score as score')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();
        return response()->json($result->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $component = Component::find($id);
        if($component!=null)
        {
            $request['active']=(int)$request['active'];
            try
            {
                $component->fill($request->all());

                $component->update();

                foreach($request['scores'] as $score)
                {                    
                    $components_benchmark =  Components_benchmark::where('id_component', $component->id_component)->where('id_benchmark', $score["id_benchmark"])->first();
                    
                    if($components_benchmark == null)
                    {
                        if($score["score"] != null)
                        {
                            $components_benchmark = new Components_benchmark();
                            $components_benchmark->id_component = $component->id_component;
                            $components_benchmark->id_benchmark = $score["id_benchmark"];
                            $components_benchmark->score = $score["score"];
                            $components_benchmark->active = 1;
                            $components_benchmark->save();
                        }
                    }
                    else
                    {   
                        if($score["score"] != null)
                        {
                            $components_benchmark->score = $score["score"];
                            $components_benchmark->update();
                        } 
                        else
                        {
                            $components_benchmark->delete();
                        }
                    }
                }

                return response()->json([
                    'success' => true,
                    'message' => 'Componente editado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al editar el componente.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el componente que quiere editar.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Component  $component
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $component = Component::find($id);
        if($component!=null)
        {
            try
            {  
                $component->destroy($component['id_component']);

                return response()->json([
                    'success' => true,
                    'message' => 'Componente eliminado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al eliminar el componente.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el componente que quiere eliminar.'
            ]);
        }
    }
}
