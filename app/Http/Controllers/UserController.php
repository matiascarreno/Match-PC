<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Subscription;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
        $this->middleware('noAdviser');
        $this->middleware('noUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions = Subscription::All();
        return view('site/pages/crud/users', compact('subscriptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(User::where('email', $request['email'])->first()==null && User::where('phone_number', $request['phone_number'])->first()==null)
        {
            try
            {
                $request['active'] = (int)$request['active'];
                $request['subscription'] = (int)$request['subscription'];
                $request['privileges'] = null;
                if($request['password'] == null || $request['password'] == "")
                {
                    $request['password'] = bcrypt("contraseña");
                }
                else
                {
                    $request['password']= bcrypt($request['password']);
                }

                $user = new User($request->all());
                
                $user->save();

                return response()->json([
                    'success' => true,
                    'message' => 'Usuario agregado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al agregar el usuario.'
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Ya existe un usuario con este email o número de teléfono.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $count = 0;
        $subscriptions = Subscription::all();
        foreach($subscriptions as $subscription)
        {
            if($subscription->id_subscription == $user->subscription)
            {
                break;
            }
            $count++;
        }
        return response()->json(array_prepend($user->toArray(), $count, 'subscription_number'));
    }

    /**
     * Display all the resources.
     *
     * @param  \App\User  $category
     * @return \Illuminate\Http\Response
     */
    public function listUsers()
    {
        $result = User::join('subscriptions', 'users.subscription', '=', 'subscriptions.id_subscription')
        ->select('users.*', 'subscriptions.name as subscription_name')
        ->where('users.privileges' ,null)
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();
        return response()->json($result->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if($user!=null)
        {
            try
            {
                $request['subscription']=(int)$request['subscription'];
                $request['active']=(int)$request['active'];
                
                if($request['password']==null || $request['active']=="")
                {
                    $user->fill($request->except('password'));
                }
                else
                {
                    $request['password']=bcrypt($request['password']);
                    $user->fill($request->all());
                }
                
                $user->update();

                return response()->json([
                    'success' => true,
                    'message' => 'Usuario editado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al editar el usuario.'
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el usuario que quiere editar.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if($user!=null)
        {
            try
            {  
                $user->destroy($user['id_user']);

                return response()->json([
                    'success' => true,
                    'message' => 'Usuario eliminado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al eliminar el usuario.'
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el usuario que quiere eliminar.'
            ]);
        }
    }
}