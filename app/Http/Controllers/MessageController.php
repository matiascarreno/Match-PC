<?php

namespace App\Http\Controllers;

use App\Message;
use App\Chat;
use Illuminate\Http\Request;
use Auth;
class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');  
        $this->middleware('noAdmin');
        $this->middleware('noUser.free');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::User()->isUser())
        {
            $chat = Chat::where('id_user', Auth::User()->id_user)->first();
        }
        else
        {
            $chat = Chat::where('id_user', $request->id)->first();
        }
        
        if($chat==null)
        {
            $chat = new chat([
                'id_user' => Auth::User()->id_user,
            ]);
            $chat->save();

            $message = new Message();
            $message->id_chat = $chat->id_chat;
            $message->response = 1;
            $message->text= "Bienvenido, has todas las preguntas que quieras.";
            $message->archive=null;
            $message->active=1;
            $message->save();
        }

        $messages = Message::where('id_chat', $chat->id_chat)->get();

        if(Auth::User()->isAdviser())
        {
            return view('site/pages/messages/adviser', compact('chat', 'messages'));
        }
        
        return view('site/pages/messages/user', compact('chat', 'messages'));
    }

    public function send(Request $request)
    {
        try
        {
            if(Auth::User()->isAdviser())
            {
                $request['response']=1;
            }
            else
            {
                $request['response']=0;
            }

            $message = new message($request->all());
            $message->save();
            return response()->json([
                'success' => true,
                'message' => 'Mensaje enviado.',
                'id_message' => $message->id_message,
                'response' => $message->response
            ]);
        }
        catch(Exception $e)
        {
            return response()->json([
                'success' => false,
                'message' => 'Hubo un error al enviar el mensaje.'
            ]);
        }
    }

    public function refresh(Request $request)
    {
        return response()->json(Message::where('id_chat', $request->id_chat)->orderBy('id_message', 'desc')->first());
    }
}
