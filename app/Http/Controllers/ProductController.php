<?php

namespace App\Http\Controllers;

use App\Product;
use App\Products_store;
use App\Store;
use App\Category;
use App\Component;
use Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
        $this->middleware('noAdviser');
        $this->middleware('noUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::All();
        $components = Component::All();
        $stores = Store::All();
        return view('site/pages/crud/products', compact('categories', 'components', 'stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Product::where('name', $request['name'])->first()==null)
        {
            $request['active']=(int)$request['active'];
            $request['category']=(int)$request['category'];
            try
            {
                $product = new product($request->all());
                
                $product->save();

                if($request['stores'] != null)
                {
                    foreach($request['stores'] as $store)
                    {                    
                        if($store["price"] != null && $store["link"] != null)
                        {  
                            $products_store = new Products_store();
                            $products_store->id_product = $product->id_product;
                            $products_store->id_store = $store["id_store"];
                            $products_store->price = $store["price"];
                            $products_store->link = $store["link"];
                            $products_store->date = Carbon\Carbon::now()->toDateTimeString();
                            $products_store->active = 1;
    
                            $products_store->save();
                        }
                    }
                }

                return response()->json([
                    'success' => true,
                    'message' => 'Producto agregado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al agregar el producto.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Ya existe un producto con este nombre.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Products_store::where('id_product', $id)->get()->isEmpty())
        {
            $product = Product::where('id_product', $id)->get();
        }
        else
        {
            $product = Product::where('products.id_product', $id)
            ->leftjoin('products_stores', 'products.id_product', '=', 'products_stores.id_product')
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
            ->get();
        }
        return response()->json($product->toArray());
    }

    /**
     * Display all the resources.
     *
     * @param  \App\Product  $category
     * @return \Illuminate\Http\Response
     */
    public function listProducts()
    {
        $result = Product::join('categories', 'products.category', '=', 'categories.id_category')
        ->leftjoin('components as cpu_component', 'products.cpu', '=', 'cpu_component.id_component')
        ->leftjoin('components as gpu_component', 'products.gpu', '=', 'gpu_component.id_component')
        ->leftjoin('components as ram_component', 'products.ram', '=', 'ram_component.id_component')
        ->leftjoin('components as ssd_component', 'products.ssd', '=', 'ssd_component.id_component')
        ->leftjoin('components as hdd_component', 'products.hdd', '=', 'hdd_component.id_component')
        ->select('products.*', 'categories.description as category_name', 'cpu_component.name as cpu_name', 'gpu_component.name as gpu_name', 'ram_component.name as ram_name', 'ssd_component.name as ssd_name', 'hdd_component.name as hdd_name')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();
        return response()->json($result->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if($product!=null)
        {
            $request['active']=(int)$request['active'];
            try
            {
                $product->fill($request->all());
                
                $product->update();

                foreach($request['stores'] as $store)
                {                    
                    $products_store =  Products_store::where('id_product', $product->id_product)->where('id_store', $store["id_store"])->first();
                    
                    if($products_store == null)
                    {
                        if($store["price"] != null && $store["link"] != null)
                        {
                            $products_store = new Products_store();
                            $products_store->id_product = $product->id_product;
                            $products_store->id_store = $store["id_store"];
                            $products_store->price = $store["price"];
                            $products_store->link = $store["link"];
                            $products_store->date = Carbon\Carbon::now()->toDateTimeString();
                            $products_store->active = 1;
    
                            $products_store->save();
                        }
                    }
                    else
                    {   
                        if($store["price"] != null && $store["link"] != null)
                        {
                            $products_store->price = $store["price"];
                            $products_store->link = $store["link"];
                            $products_store->date = Carbon\Carbon::now()->toDateTimeString();
                            $products_store->update();
                        } 
                        else
                        {
                            $products_store->delete();
                        }
                    }
                }

                return response()->json([
                    'success' => true,
                    'message' => 'Producto editado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al editar el producto.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el producto que quiere editar.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product!=null)
        {
            try
            {  
                $product->destroy($product['id_product']);

                return response()->json([
                    'success' => true,
                    'message' => 'Producto eliminado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al eliminar el producto.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el producto que quiere eliminar.'
            ]);
        }
    }
}