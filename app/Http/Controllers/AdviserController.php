<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class AdviserController extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
        $this->middleware('noAdviser');
        $this->middleware('noUser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site/pages/crud/advisers');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(User::where('email', $request['email'])->orwhere('phone_number', $request['phone_number'])->first() == null)
        {
            try
            {
                $request['active']=(int)$request['active'];
                $request['last_purchase']=null;
                $request['privileges']=0;
                
                if($request['password']==null || $request['subscription']=="")
                {
                    $request['password']= bcrypt("contraseña");
                }
                else
                {
                    $request['password']= bcrypt($request['password']);
                }

                $user = new User($request->all());
                $user->save();

                return response()->json([
                    'success' => true,
                    'message' => 'Asesor agregado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error inesperado al agregar el asesor.',
                    'description' => $e,
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Ya existe un asesor con este email o número de teléfono.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return response()->json($user->toArray());
    }

    /**
     * Display all the resources.
     *
     * @param  \App\User  $category
     * @return \Illuminate\Http\Response
     */
    public function listAdvisers()
    {
        
        return response()->json(User::where('privileges', "0")->get()->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($user!=null)
        {
            try
            {
                $request['active']=(int)$request['active'];

                if($request['password']==null || $request['active']=="")
                {
                    $user->fill($request->except('password'));
                }
                else
                {
                    $request['password']=bcrypt($request['password']);
                    $user->fill($request->all());
                }

                $user->update();

                return response()->json([
                    'success' => true,
                    'message' => 'Asesor editado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al editar el asesor.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el asesor que quiere editar.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user!=null)
        {
            try
            {  
                $user->destroy($user['id_user']);

                return response()->json([
                    'success' => true,
                    'message' => 'Asesor eliminado correctamente.'
                ]);
            }
            catch(Exception $e)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Hubo un error al eliminar el asesor.',
                    'description' => $e
                ]);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'No existe el asesor que quiere eliminar.'
            ]);
        }
    }
}
