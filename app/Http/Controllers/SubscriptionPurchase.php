<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscription;
use Carbon;
use App\User;
use App\Transaction;
use Auth;
use Session;
use Redirect;
use Transbank\Webpay\Configuration;
use Transbank\Webpay\Webpay;

class SubscriptionPurchase extends Controller
{
    public function __construct()
    {
        $this->middleware('noUnlogged');
        //$this->middleware('noUser.premium');
        $this->middleware('noAdviser');
        $this->middleware('noAdmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {      
        $age = Carbon\Carbon::now()->addYears(-16)->format('Y/m/d');
        $subscriptions = Subscription::where('id_subscription', '!=', 1)->get();

        $data = 
        [
            'age' => $age,
            'subscriptions' => $subscriptions,
        ];
        

        return view('site/pages/purchases/subscription_purchase', compact('data'));
    }

    public function purchase(Request $request)
    {
        $configuration = new Configuration();
        $configuration->setEnvironment("INTEGRACION");

        $webpay = (new Webpay(Configuration::forTestingWebpayPlusNormal()))
        ->getNormalTransaction();
        
        $subscription = Subscription::find($request['subscription']);

        $amount = $subscription->price;

        // Identificador que será retornado en el callback de resultado:
        $sessionId = Session::getId();

        // Identificador único de orden de compra:
        $buyOrder = strval(rand(100000, 999999999));
        
        $returnUrl = "https://match-pc.com/transbank";
        $finalUrl = "https://match-pc.com/transbank_end";
        
        //$returnUrl = "http://127.0.0.1:8000/transbank";
        //$finalUrl = "http://127.0.0.1:8000/transbank_end";

        $initResult = $webpay->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);

        $formAction = $initResult->url;
        $tokenws = $initResult->token;

        $transaction = new Transaction();
        $transaction->tokenws = $tokenws;
        $transaction->buy_order = $buyOrder;
        $transaction->id_user = Auth::user()->id_user;
        $transaction->id_subscription = $subscription->id_subscription;
        $transaction->approved = null;
        $transaction->date = Carbon\Carbon::now()->toDateTimeString();

        $transaction->save();

        $data = 
        [
            'tokenws' => $tokenws,
            'formAction' => $formAction,
        ];
        

        return view('site/pages/purchases/transbank_redirection', compact('data'));
        
        /* switch($request['result'])
        {
            case "succes":
                try
                {
                    $subscription = Subscription::find($request['subscription']);
                    if($subscription==null)
                    {
                        return Redirect::to('/home')->with('error', 'Hubo un error inesperado al comprar la membresía');
                    }
                    $user = Auth::user();
                    $user->subscription = $request['subscription'];
                    $user->last_purchase = Carbon\Carbon::now()->toDateTimeString();
                    $user->update();

                    Session::flash('success', 'Feliciades, ahora eres '.$subscription->name.'.');
                    Session::flash('description', 'Distruta de tus membresía.');
                    return Redirect::to('/home');
                }
                catch (Exception $e)
                {
                    Session::flash('error', 'Hubo un error inesperado al comprar la membresía');
                    Session::flash('description', $e);
                    return Redirect::to('/home');
                }
                
                break;
            
            case "error":
                Session::flash('error', 'Hubo un error durante el pago.');
                return Redirect::to('/home');
                break;

            default:
                Session::flash('error', 'Hubo un error inesperado al comprar la membresía');
                return Redirect::to('/home');
                break;
        } 
        */
    }

    public function transbank(Request $request)
    {
        $transaction = (new Webpay(Configuration::forTestingWebpayPlusNormal()))
        ->getNormalTransaction();

        $result = $transaction->getTransactionResult($request->input("token_ws"));

        $output = $result->detailOutput;

        $transaction = Transaction::where('tokenws', $request->input("token_ws"))->first();

        if ($output->responseCode == 0) 
        {
            $transaction->approved = 1;
            $user = Auth::user();
            $user->subscription = $transaction->id_subscription;
            $user->last_purchase = Carbon\Carbon::now()->toDateTimeString();
            $user->update();
        }
        else
        {
            $transaction->approved = 0;
        }
        $transaction->save();

        $data = 
        [
            'tokenws' => $request->input("token_ws"),
            'formAction' => $result->urlRedirection,
        ];
        
        return view('site/pages/purchases/transbank_redirection', compact('data'));
    }

    public function transbank_end(Request $request)
    {
        $token = $request->input("token_ws");
        $transaction = Transaction::where('tokenws', $token)->first();
        
        if($token == null)
        {
            Session::flash('error', 'Hubo un error inesperado al comprar la membresía');
            Session::flash('description', 'No te preocupes, ho se hará ningún cargo a tu tarjeta.');
            return Redirect::to('/home');
        }

        $subscription = Subscription::find($transaction->id_subscription);

        if($transaction->isApproved())
        {
            Session::flash('success', 'Feliciades, ahora eres '.$subscription->name.'.');
            Session::flash('description', 'Te hemos enviado un correo electrónico con todos los detalles.');
            return Redirect::to('/home');
        }
        else
        {
            Session::flash('error', 'Hubo un error durante el pago.');
            Session::flash('description', 'No te preocupes, ho se hará ningún cargo a tu tarjeta.');
            return Redirect::to('/home');
        }
    }
}
