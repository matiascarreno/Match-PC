<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\Mail\EmailRegister;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Hash;
use Session;
use Mail;


class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('noLogged');
    }

    public function index()
    {
        return view('log_/pages/register');
    }

    public function register(Request $request)
    {
        $user = User::where('email', $request['email'])->first();

        if(Carbon::parse($request['birthdate'])->age <16)
        {
            Session::flash('error', 'Debes ser mayor de 16 añor para registrarte.');
            return Redirect::to('/register')->withInput($request->except('password'));
        }

        if($user == null)
        {
            try
            {
                $user = new user($request->all());

                $random = str_random(100);

                $user->active=0;
                $user->privileges=null;
                $user->password=bcrypt($user->password);
                $user->remember_token = bcrypt($random);
                
                Mail::to($request['email'])->send(new EmailRegister($user->email, $random));

                $user->save();

                Session::flash('success', 'Usuario creado correctamente.');
                Session::flash('description', 'Por favor verifique su correo electrónico para activar la cuenta.');
                return Redirect::to('/login');
            }
            catch(Exception $e)
            {
                Session::flash('error', 'Ha ocurrido un error inesperado.');
                Session::flash('description', $e);
                return Redirect::to('/register')->withInput($request->except('password'));
            }
            
        }
        else
        {
            Session::flash('error', 'Ya existe un usuario con este email.');
            return Redirect::to('/register')->withInput($request->except('password'));
        }
        
    }

    public function activate(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        
        if($user != null && Hash::check($request['random'], $user->remember_token))
        {
            $user->remember_token = "";
            $user->active = 1;
            $user->update();

            Session::flash('success', 'Su cuanta ha sio activada con éxito.');
            Session::flash('description', 'Ingrese su usuario y contraseña para ingresar.');
            return Redirect::to('/login');
        }

        Session::flash('error', 'Ha ocurrido un error inesperado al activar su cuenta.');
        Session::flash('description', 'Por favor intente nuevamente más tarde.');
        return Redirect::to('/login');
    }
}
