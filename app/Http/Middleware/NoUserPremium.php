<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;

class NoUserPremium
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->isPremium())
        {
            Session::flash('error', 'No puedes acceder a esta sección con una cuenta Premium.');
            Session::flash('description', 'Aunque no estoy seguro de porqué');
            return Redirect::to('/home');
        }
        return $next($request);
    }
}
