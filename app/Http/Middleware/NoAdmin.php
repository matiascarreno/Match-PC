<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;

class NoAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->isAdmin())
        {
            Session::flash('error', 'No puedes acceder a esta sección con una cuenta de administrador.');
            return Redirect::to('/home');
        }
        return $next($request);
    }
}
