<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;

class NoUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->isUser())
        {
            Session::flash('error', 'No tienes los permisos para acceder este sitio.');
            return Redirect::to('/home');
        }
        return $next($request);
    }
}
