<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;

class NoUserLite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->isLite())
        {
            Session::flash('error', 'No puedes acceder a esta sección con una cuenta Lite.');
            Session::flash('description', 'Puedes obtener acceso si copras una menbresía Premium, junto con nuchos otros beneficios');
            return Redirect::to('/home');
        }
        return $next($request);
    }
}
