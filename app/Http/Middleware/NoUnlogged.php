<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;

class NoUnlogged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check())
        {
            Session::flash('error', 'Por favor inicia sesión para ingresar.');
            return Redirect::to('/login');
        }
        return $next($request);
    }
}
