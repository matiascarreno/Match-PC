<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;

class NoAdviser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->isAdviser())
        {
            Session::flash('error', 'No puedes acceder a esta sección con una cuenta de asesor.');
            return Redirect::to('/home');
        }
        return $next($request);
    }
}
