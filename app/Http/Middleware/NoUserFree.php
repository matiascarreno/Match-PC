<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;

class NoUserFree
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->isFree())
        {
            Session::flash('error', 'No puedes acceder a esta sección con una cuenta gratuita.');
            Session::flash('description', 'Actualiza hoy su plan y obtén una gran cantidad de beneficios.');
            return Redirect::to('/home');
        }
        return $next($request);;
    }
}
