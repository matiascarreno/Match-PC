<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $primaryKey = 'id_store';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'website',
        'active',
    ];

    public function isActive()
    {
        if($this->active == 1 || $this->active == "1")
        {
            return true;
        }
        return false;
    }
}
