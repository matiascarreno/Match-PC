<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    protected $primaryKey = 'id_component';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'type',
        'freq',
        'cores',
        'memory',
        'active'
    ];

    public function isActive()
    {
        if($this->active == 1 || $this->active == "1")
        {
            return true;
        }
        return false;
    }
}
