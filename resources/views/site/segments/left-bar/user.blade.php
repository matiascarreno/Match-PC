        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->

            @include('site/segments/left-bar/segments/header')
            
            <!-- #User Info -->

            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENÚ PRINCIPAL</li>

                    <!-- Inicio -->
                    <li @if(Request::path() == 'home') class="active" @endif>
                        <a href="/home">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>

                    <!-- Premium -->
                    @if(!Auth::User()->isPremium())

                        <li @if(Request::path() == 'premium') class="active" @endif>
                            <a href="/premium">
                                <i class="material-icons">new_releases</i>
                                <span>Conviértete en Premium</span>
                            </a>
                        </li>

                    @endif

                    <!-- Nueva Cotización -->
                    <li @if(Request::path() == 'new_quotations' || Request::path() == 'show_quotations') class="active" @endif>
                        <a href="/new_quotations">
                            <i class="material-icons">playlist_add_check</i>
                            <span>Nueva Cotización</span>
                        </a>
                    </li>

                    <!-- Cotizaciones -->
                    <li @if(Request::path() == '*****') class="active" @endif>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Cotizaciones</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="javascript:void(0);">
                                    <span>Futuro pc gamer Master Race<span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <span>PC para la cocina</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!-- Mensajes -->
                    @if(!Auth::User()->isFree())
                        <li @if(Request::path() == 'messages') class="active" @endif>
                            <a href="/messages">
                                <i class="material-icons">forum</i>
                                <span>Mensajes con tu asesor</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                <a href="javascript:void(0);" id="rate_button">
                                        <i class="material-icons">spellcheck</i> 
                                        <span>Califica tu asesoría<span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif

                    <!-- Tutorial -->
                    <li @if(Request::path() == '*****') class="active" @endif>
                        <a href="javascript:void(0);">
                            <i class="material-icons">explore</i>
                            <span>Tutorial (En desarrollo)</span>
                        </a>
                    </li>

                    <!-- Danos tu opinión -->
                    <li @if(Request::path() == '*****') class="active" @endif>
                        <a href="javascript:void(0);">
                            <i class="material-icons">rate_review</i>
                            <span>Danos tu opinión (En desarrollo)</span>
                        </a>
                    </li>

                    <!-- Mensajes -->
                    <li class="header">INFORMACIÓN</li>

                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-red">donut_large</i>
                            <span>Mensaje mucho muy importante</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-amber">donut_large</i>
                            <span>Mensaje normal</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>Información</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->

            <!-- Footer -->

            @include('site/segments/left-bar/segments/footer')

            <!-- #Footer -->
        </aside>