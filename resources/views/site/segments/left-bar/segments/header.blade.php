            <div class="user-info" style='background: url("../images/pictures/{{ Auth::User()->cover_image }}") no-repeat no-repeat;'>
                <div class="image">
                    <img src="{{ asset('images/avatar/'.Auth::User()->picture) }}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::User()->name }}</div>
                    <div class="email">{{ Auth::User()->email }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Mi Perfil</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Compras</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Feedback</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="/logout"><i class="material-icons">exit_to_app</i>Cerrar sesión</a></li>
                        </ul>
                    </div>
                </div>
            </div>