        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->

            @include('site/segments/left-bar/segments/header')
            
            <!-- #User Info -->

            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENÚ PRINCIPAL</li>

                    <!-- Inicio -->
                    <li @if(Request::path() == 'home') class="active" @endif>
                       <a href="/home">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>

                    <!-- Mensajes -->
                    <li @if(Request::path() == 'messages') class="active" @endif>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">forum</i>
                            <span>Mensajes (Asesor)</span>
                        </a>
                        <ul class="ml-menu">
                            @foreach (Session::get('chats') as $chat)
                                @if($chat->subscription != 1)
                                    <li>
                                        <a href="/messages/?id={{$chat->id_user}}">
                                            @if($chat->subscription == 3)
                                                <i class="material-icons col-yellow" id="premium_message{{ $chat->id_user }}">star</i>
                                            @else
                                                <i class="material-icons" id="lite_message{{ $chat->id_user }}">star_border</i>
                                            @endif
                                            <span>Mensajes de {{$chat->user_name}}, (ID: {{ $chat->id_user }})<span>
                                        </a>
                                    </li>
                                @endif
                            

                            @endforeach
                        </ul>
                    </li>

                    <!-- Cotizaciones -->
                    <li @if(Request::path() == '*****') class="active" @endif>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">widgets</i>
                            <span>Cotizaciones (En desarrollo)</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="javascript:void(0);">
                                    <span>Premium - Perico Los Palotes<span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <span>Lite - Don Joe</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!-- Buscar -->
                    <li @if(Request::path() == '*****') class="active" @endif>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">search</i>
                            <span>Búscar (En desarrollo)</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="javascript:void(0);">Buscar Notebooks</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Buscar Desktops</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Buscar All-in-one</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Buscar piezas</a>
                            </li>
                        </ul>
                    </li>

                    <!-- Tutorial -->
                    <li @if(Request::path() == '*****') class="active" @endif>
                        <a href="javascript:void(0);">
                            <i class="material-icons">explore</i>
                            <span>Tutorial (En desarrollo)</span>
                        </a>
                    </li>

                    <!-- Danos tu opinión -->
                    <li @if(Request::path() == '*****') class="active" @endif>
                        <a href="javascript:void(0);">
                            <i class="material-icons">rate_review</i>
                            <span>Danos tu opinión (En desarrollo)</span>
                        </a>
                    </li>

                    <!-- Mensajes -->
                    <li class="header">INFORMACIÓN</li>

                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-red">donut_large</i>
                            <span>Mensaje mucho muy importante</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-amber">donut_large</i>
                            <span>Mensaje normal</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>Información</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->

            <!-- Footer -->

            @include('site/segments/left-bar/segments/footer')

            <!-- #Footer -->
        </aside>