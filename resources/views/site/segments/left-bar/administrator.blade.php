        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->

            @include('site/segments/left-bar/segments/header')
            
            <!-- #User Info -->

            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MENÚ PRINCIPAL</li>

                    <!-- Inicio -->
                    <li @if(Request::path() == 'home') class="active" @endif> 
                        <a href="/home">
                            <i class="material-icons">home</i>
                            <span>Inicio</span>
                        </a>
                    </li>

                    <!-- CRUD - Productos (Administrador) -->
                    <li @if(Request::path() == 'products') class="active" @endif> 
                    <a href="/products">
                            <i class="material-icons">computer</i>
                            <span>CRUD - Productos</span>
                        </a>
                    </li>

                    <!-- CRUD - Productos (Administrador) -->
                    <li @if(Request::path() == 'components') class="active" @endif> 
                        <a href="/components">
                                <i class="material-icons">settings_input_component</i>
                                <span>CRUD - Componentes</span>
                            </a>
                        </li>

                    <!-- CRUD - Categorias (Administrador) -->
                    <li @if(Request::path() == 'categories') class="active" @endif> 
                        <a href="/categories">
                            <i class="material-icons">dashboard</i>
                            <span>CRUD - Categorias</span>
                        </a>
                    </li>

                    <!-- CRUD - Usuarios (Administrador) -->
                    <li @if(Request::path() == 'users') class="active" @endif> 
                        <a href="/users">
                            <i class="material-icons">people</i>
                            <span>CRUD - Usuarios</span>
                        </a>
                    </li>

                    <!-- CRUD - Asesores (Administrador) -->
                    <li @if(Request::path() == 'advisers') class="active" @endif> 
                        <a href="/advisers">
                            <i class="material-icons">people_outline</i>
                            <span>CRUD - Asesores (Admin - En desarrollo)</span>
                        </a>
                    </li>

                    <!-- CRUD - Tiendas (Administrador) -->
                    <li @if(Request::path() == 'stores') class="active" @endif> 
                        <a href="/stores">
                            <i class="material-icons">store</i>
                            <span>CRUD - Tiendas</span>
                        </a>
                    </li>

                    <!-- CRUD - BenchMark (Administrador) -->
                    <li @if(Request::path() == 'benchmarks') class="active" @endif> 
                        <a href="/benchmarks">
                            <i class="material-icons">show_chart</i>
                            <span>CRUD - BenchMark</span>
                        </a>
                    </li>

                    <!-- CRUD - Cotizaciones (Administrador) -->
                    <li @if(Request::path() == '*****') class="active" @endif> 
                        <a href="javascript:void(0);">
                            <i class="material-icons">widgets</i>
                            <span>CRUD - Cotizaciones (Admin - En desarrollo)</span>
                        </a>
                    </li>

                    <!-- Tutorial -->
                    <li @if(Request::path() == '*****') class="active" @endif>
                        <a href="javascript:void(0);">
                            <i class="material-icons">explore</i>
                            <span>Tutorial (En desarrollo)</span>
                        </a>
                    </li>

                    <!-- Mensajes -->
                    <li class="header">INFORMACIÓN</li>

                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-red">donut_large</i>
                            <span>Mensaje mucho muy importante</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-amber">donut_large</i>
                            <span>Mensaje normal</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>Información</span>
                        </a>
                    </li>                    
                </ul>
            </div>
            <!-- #Menu -->

            <!-- Footer -->

            @include('site/segments/left-bar/segments/footer')

            <!-- #Footer -->
        </aside>