        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">APARIENCIA</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">CONFIGURACIÓN</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" @if(Auth::User()->theme == "red") class="active" @endif>
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink" @if(Auth::User()->theme == "pink") class="active" @endif>
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple" @if(Auth::User()->theme == "purple") class="active" @endif>
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple" @if(Auth::User()->theme == "deep-purple") class="active" @endif>
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo" @if(Auth::User()->theme == "indigo") class="active" @endif>
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue" @if(Auth::User()->theme == "blue") class="active" @endif>
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue" @if(Auth::User()->theme == "light-blue") class="active" @endif>
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan" @if(Auth::User()->theme == "cyan") class="active" @endif>
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal" @if(Auth::User()->theme == "teal") class="active" @endif>
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green" @if(Auth::User()->theme == "green") class="active" @endif>
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green" @if(Auth::User()->theme == "light-green") class="active" @endif>
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime" @if(Auth::User()->theme == "lime") class="active" @endif>
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow" @if(Auth::User()->theme == "yellow") class="active" @endif>
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber" @if(Auth::User()->theme == "amber") class="active" @endif>
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange" @if(Auth::User()->theme == "orange") class="active" @endif>
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange" @if(Auth::User()->theme == "deep-orange") class="active" @endif>
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown" @if(Auth::User()->theme == "brown") class="active" @endif>
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey" @if(Auth::User()->theme == "grey") class="active" @endif>
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey" @if(Auth::User()->theme == "blue-grey") class="active" @endif>
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black" @if(Auth::User()->theme == "black") class="active" @endif>
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL</p>
                        <ul class="setting-list">
                            <li>
                                <span style="width: 80%;display: block;">Ayudar con envío anónimo de datos</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Cerrar cuenta</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>NOTIFICACIONES</p>
                        <ul class="setting-list">
                            <li>
                                <span>Recibir notificaciones</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>recibir ofertas</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>CUENTA</p>
                        <ul class="setting-list">
                            <li>
                                <span>Nombre</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Imagen de perfil</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Imagen de portada</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Acceso a ubicación</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>