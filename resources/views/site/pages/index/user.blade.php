@extends('site/layout')

@section('title', 'Inicio')

@section('css')

    <!-- Morris Chart Css-->
    <link href="{{ asset('plugins/morrisjs/morris.css') }}" rel="stylesheet" />

@endsection

<!-- Top Bar -->
@section('top-bar')
    @include('site/segments/top-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Left Bar -->
@section('left-bar')
    @include('site/segments/left-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Right Bar -->
@section('right-bar')
    @include('site/segments/right-bar/right-bar')
@endsection
<!-- #Right Bar -->

<!-- Content -->
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Resumen de cuenta Usuario</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                @if(Auth::user()->isFree())
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="/premium">
                    <div class="info-box-4 hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons col-yellow">new_releases</i>
                        </div>
                        <div class="content">
                            <div class="text">Conviértete en Premium!!</div>
                            <div class="number">Free</div>
                        </div>
                    </div>
                </a>
                </div>
                @else 
                @if(Auth::user()->isLite())
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="/premium">
                    <div class="info-box-4 hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons col-yellow">new_releases</i>
                        </div>
                        <div class="content">
                            <div class="text">Membresía</div>
                            <div class="number">Lite</div>
                        </div>
                    </div>
                </a>
                </div>
                @else
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons col-yellow">new_releases</i>
                        </div>
                        <div class="content">
                            <div class="text">Membresía</div>
                            <div class="number">Premium</div>
                        </div>
                    </div>
                </div>
                @endif
                @endif

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons col-blue">account_balance_wallet</i>
                        </div>
                        <div class="content">
                            <div class="text">Presupuesto actual</div>
                            <div class="number count-to" data-from="0" data-to="700000" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-4 hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons col-light-green">attach_money</i>
                        </div>
                        <div class="content">
                            <div class="text">Precio de la cotización</div>
                            <div class="number count-to" data-from="0" data-to="684990" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a @if(!Auth::User()->isFree()) href="/messages" @else href="javascript:void(0);" @endif>
                        <div class="info-box-4 hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons col-orange">forum</i>
                            </div>
                            <div class="content">
                                @if(!Auth::User()->isFree())
                                <div class="text">Has todas las consultas que tengas</div>
                                <div class="number">Asesoría</div>
                                @else
                                    <div class="text">Obtén tu</div>
                                    <div class="number">Asesoría</div>
                                @endif
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- #END# Widgets -->
            <div class="row clearfix">
                <!-- Visitors -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body col-pink">
                            <div class="sparkline" data-type="line" data-spot-Radius="4" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#e91e63"
                                data-min-Spot-Color="rgb(233, 30, 99,0.7)" data-max-Spot-Color="rgb(233, 30, 99,0.7)" data-spot-Color="rgb(255,255,255)"
                                data-offset="90" data-width="100%" data-height="92px" data-line-Width="2" data-line-Color="rgba(233, 30, 99,0.7)"
                                data-fill-Color="rgba(0, 188, 212, 0)">
                                530000, 500000, 540000, 500000, 680000, 700000, 660000, 600000
                            </div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Precio de cotización hoy
                                    <span class="pull-right"><b>$600.000</b> <small></small></span>
                                </li>
                                <li>
                                    Precio de cotización ayer
                                    <span class="pull-right"><b>$660.000</b> <small></small></span>
                                </li>
                                <li>
                                    Precio de cotización hace 7 dias
                                    <span class="pull-right"><b>$530.000</b> <small></small></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Visitors -->
                <!-- Latest Social Trends -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body col-cyan">
                            <div class="m-b--35 font-bold">Últimos articulos agregados</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    HP Envy 13-AD006LA
                                    <span class="pull-right">
                                        <i class="material-icons">trending_up</i>
                                    </span>
                                </li>
                                <li>
                                    Lenovo ThinkPad X1 Carbon
                                    <span class="pull-right">
                                        <i class="material-icons">trending_up</i>
                                    </span>
                                </li>
                                <li>
                                    Dell Inspiron 15 5559
                                    <span class="pull-right">
                                        <i class="material-icons">trending_down</i>
                                    </span>
                                </li>
                                <li>Lenovo IdeaPad Flex</li>
                                <li>Dell Inspiron 15 7000</li>
                                <li>
                                    Lenovo IdeaPad 720s
                                    <span class="pull-right">
                                        <i class="material-icons">trending_up</i>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Latest Social Trends -->
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body col-brown">
                            <div class="font-bold m-b--35">historial de acciones</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Creada catización "PC para la cocina"
                                    <span class="pull-right"><b>Hoy</b></span>
                                </li>
                                <li>
                                    Creada cotización "Futuro pc gamer Master Race"
                                    <span class="pull-right"><b>Ayer</b></span>
                                </li>
                                <li>
                                    Contraseña Cambiada
                                    <span class="pull-right"><b>Hace 3 días</b></span>
                                </li>
                                <li>
                                    Avatar cambiado
                                    <span class="pull-right"><b>Hace 5 días</b></span>
                                </li>
                                <li>
                                    Imagen de portada cambiada
                                    <span class="pull-right"><b>Semana pasada</b></span>
                                </li>
                                <li>
                                    Cuenta creada
                                    <span class="pull-right"><b>Semana pasada</b></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
            </div>

            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Los Más Populares</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Opción</a></li>
                                        <li><a href="javascript:void(0);">Otra opción</a></li>
                                        <li><a href="javascript:void(0);">Otra más</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nombre</th>
                                            <th>Oferta</th>
                                            <th>Tienda</th>
                                            <th>Benchmark</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>HP Envy 13-AD006LA</td>
                                            <td><span class="label bg-green">-50%</span></td>
                                            <td>PCFactory</td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Lenovo ThinkPad X1 Carbon</td>
                                            <td><span class="label bg-blue">-40%</span></td>
                                            <td>SPDigital</td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Dell Inspiron 15 5559</td>
                                            <td><span class="label bg-light-blue">-35%</span></td>
                                            <td>Winpi</td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar bg-light-blue" role="progressbar" aria-valuenow="72" aria-valuemin="0" aria-valuemax="100" style="width: 72%"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Lenovo IdeaPad Flex</td>
                                            <td><span class="label bg-orange">-10%</span></td>
                                            <td>PCFactory</td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%"></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Dell Inspiron 15 7000</td>
                                            <td>
                                                <span class="label bg-red">-0%</span>
                                            </td>
                                            <td>SPDigital</td>
                                            <td>
                                                <div class="progress">
                                                    <div class="progress-bar bg-red" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width: 87%"></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <h2>Uso del presupuesto</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">GPU: Graficos</a></li>
                                        <li><a href="javascript:void(0);">CPU: Procesador</a></li>
                                        <li><a href="javascript:void(0);">SSD: Almacenamiento (rápidp)</a></li>
                                        <li><a href="javascript:void(0);">HDD: Almacenamiento (masivo)</a></li>
                                        <li><a href="javascript:void(0);">RAM: Memoria volatil</a></li>
                                        <li><a href="javascript:void(0);">Placa Madre: Placa madre</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="donut_chart" class="dashboard-donut-chart"></div>
                        </div>
                    </div>
                </div>
                #END# Browser Usage -->
            </div>
        </div>
    </section>
    
@endsection
<!-- #Content -->

@section('js')

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('plugins/jquery-countto/jquery.countTo.js') }}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/morrisjs/morris.js') }}"></script>


    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>
    
    <!-- Custom Js -->
    <script src="{{ asset('js/pages/index.js') }}"></script>

@endsection