@extends('site/layout')

@section('title', 'Asesoría')

@section('css')

@endsection

<!-- Top Bar -->
@section('top-bar')
    @include('site/segments/top-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Left Bar -->
@section('left-bar')
    @include('site/segments/left-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Right Bar -->
@section('right-bar')
    @include('site/segments/right-bar/right-bar')
@endsection
<!-- #Right Bar -->

<!-- Content -->
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>HAZ TODAS LAS CONSULTAS QUE TENGAS</h2>
            </div>

            <!-- Material Design Alerts -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                @if(Auth::User()->isLite())
                                    Recuerda que al no ser premium, la respuesta puede no ser inmediata
                                @else  
                                    Te responderémos a la brevedad
                                @endif
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Primera opción</a></li>
                                        <li><a href="javascript:void(0);">Segunda opción</a></li>
                                        <li><a href="javascript:void(0);">Tercera opción</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body" id="body_chat">
                            <div class="chat" style="heigth: 0px; margin: 0px; padding: 0px;"></div>
                            @foreach($messages as $message)
                                <div id="message_{{ $message->id_message }}"
                                    @if($message->response == 0)
                                        class="chat sending"
                                    @else
                                        class="chat receiving"
                                    @endif
                                >
                                    {{ $message->text }}
                                </div>
                            @endforeach

                            <form action="javascript:void(0);" id="message_form">
                            {{ csrf_field() }}
                            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}">
                            <input type="hidden" id="current_chat" name="current_chat" value="{{ $chat->id_chat }}">
                            
                            <div class="input-group input-group-lg input-chat">
                                <div class="form-line">
                                    <input type="text" name="message_text"  id="message_text" class="form-control date" placeholder="Escribe un mensaje...">
                                </div>
                                <span class="input-group-addon" id="send_button">
                                    <i class="material-icons">send</i>
                                </span>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Material Design Alerts -->

        </div>
    </section>
    
@endsection
<!-- #Content -->

@section('js')

    <!-- Custom Js -->
    <script src="{{ asset('js/pages/index.js') }}"></script>
    <script src="{{ asset('js/pages/messages/messages.js') }}"></script>

@endsection