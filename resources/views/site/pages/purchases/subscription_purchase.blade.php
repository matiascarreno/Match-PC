@extends('site/layout')

@section('title', 'Conviértete en Premium')

@section('css')
>

@endsection

<!-- Top Bar -->
@section('top-bar')
    @include('site/segments/top-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Left Bar -->
@section('left-bar')
    @include('site/segments/left-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Right Bar -->
@section('right-bar')
    @include('site/segments/right-bar/right-bar')
@endsection
<!-- #Right Bar -->

<!-- Content -->
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                CONVIÉRTETE EN PREMIUM Y CONSIGUE MÁS RÁPIDO LO QUE NECESITAS
            </h2>
        </div>

        <!-- Advanced Form Example With Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>FORMULARIO DE COMPRA DE SUSCRIPCIÓN PREMIUM</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">                        
                        <form id="wizard_with_validation" method="POST"action="/premium">
                        {{ csrf_field() }}
                        <input type="hidden" name="result" id="result" value="error">
                            <h3>Información personal necesaria</h3>
                            <fieldset>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="name" class="form-control" minlength="3" maxlength="100" required>
                                        <label class="form-label">Nombres*</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="lastname" class="form-control" minlength="3" maxlength="100" required>
                                        <label class="form-label">Apellidos*</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="number" class="form-control" name="phone_number" minlength="9" maxlength="100" required>
                                        <label class="form-label">Número de teléfono*</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="email" name="email" class="form-control" required>
                                        <label class="form-label">Email*</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="birthdate" class="form-control" onfocus="(this.type='date')" onblur="(this.type='text')" max="{{ $data['age'] }}" placeholder="Fecha de nacimiento*" required>                                    </div>
                                    <div class="help-info">Debes ser mayor de 16 años para contratar el servicio</div>
                                </div>
                            </fieldset>

                            <h3>Selecciona el tipo de cuenta que deseas</h3>
                            <fieldset>
                                @foreach ($data['subscriptions'] as $subscription)
                                @if($subscription->id_subscription != Auth::User()->subscription)
                                <div class="form-group form-float">
                                    <input name="subscription" value="{{ $subscription->id_subscription }}" class="radio-col-yellow" type="radio" id="{{ $subscription->name }}" required/>
                                    <label for="{{ $subscription->name }}">
                                        <p>
                                            <strong><u>{{ $subscription->name }}:</u> ${{ $subscription->price }} mensuales</strong>
                                            {{ $subscription->description }}
                                        </p>
                                    </label>
                                </div>
                                @endif
                                @endforeach
                            </fieldset>

                            <h3>Terminos y Condiciones</h3>
                            <fieldset>
                                <p>
                                    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de 
                                    relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)
                                    desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo 
                                    sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente
                                    igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes
                                    de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye 
                                    versiones de Lorem Ipsum.
                                    <br><br>
                                    ¿Por qué lo usamos?
                                    Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras
                                    que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al 
                                    contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí". Estos textos hacen parecerlo un español que
                                    se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, 
                                    y al hacer una búsqueda de "Lorem Ipsum" va a dar por resultado muchos sitios web que usan este texto si se encuentran
                                    en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras
                                    veces a propósito (por ejemplo insertándole humor y cosas por el estilo).
                                    <br><br>
                                    ¿De dónde viene?
                                    Al contrario del pensamiento popular, el texto de Lorem Ipsum no es simplemente texto aleatorio. Tiene sus raices en una
                                    pieza cl´sica de la literatura del Latin, que data del año 45 antes de Cristo, haciendo que este adquiera mas de 2000 
                                    años de antiguedad. Richard McClintock, un profesor de Latin de la Universidad de Hampden-Sydney en Virginia, encontró 
                                    una de las palabras más oscuras de la lengua del latín, "consecteur", en un pasaje de Lorem Ipsum, y al seguir leyendo
                                    distintos textos del latín, descubrió la fuente indudable. Lorem Ipsum viene de las secciones 1.10.32 y 1.10.33 de "de 
                                    Finnibus Bonorum et Malorum" (Los Extremos del Bien y El Mal) por Cicero, escrito en el año 45 antes de Cristo. Este 
                                    libro es un tratado de teoría de éticas, muy popular durante el Renacimiento. La primera linea del Lorem Ipsum, "Lorem
                                    ipsum dolor sit amet..", viene de una linea en la sección 1.10.32
                                    <br><br>
                                    El trozo de texto estándar de Lorem Ipsum usado desde el año 1500 es reproducido debajo para aquellos interesados. Las 
                                    secciones 1.10.32 y 1.10.33 de "de Finibus Bonorum et Malorum" por Cicero son también reproducidas en su forma original
                                    exacta, acompañadas por versiones en Inglés de la traducción realizada en 1914 por H. Rackham.
                                    <br><br>
                                    ¿Dónde puedo conseguirlo?
                                    Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió alteraciones en alguna manera,
                                    ya sea porque se le agregó humor, o palabras aleatorias que no parecen ni un poco creíbles. Si vas a utilizar un pasaje 
                                    de Lorem Ipsum, necesitás estar seguro de que no hay nada avergonzante escondido en el medio del texto. Todos los 
                                    generadores de Lorem Ipsum que se encuentran en Internet tienden a repetir trozos predefinidos cuando sea necesario, 
                                    haciendo a este el único generador verdadero (válido) en la Internet. Usa un diccionario de mas de 200 palabras
                                    provenientes del latín, combinadas con estructuras muy útiles de sentencias, para generar texto de Lorem Ipsum que 
                                    parezca razonable. Este Lorem Ipsum generado siempre estará libre de repeticiones, humor agregado o palabras no 
                                    características del lenguaje, etc.
                                </p>
                                <div class="form-group form-float">
                                    <input id="acceptTerms-2" name="acceptTerms" type="checkbox" required>
                                    <label for="acceptTerms-2">Estoy deacuerdo con los términos y condiciones</label>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Form Example With Validation -->
    </div>
</section>
    
@endsection
<!-- #Content -->

@section('js')

    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Custom Js Wizzard --> 
    <script src="{{ asset('js/pages/forms/form-wizard.js') }}"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('plugins/jquery-steps/jquery.steps.js') }}"></script>

@endsection