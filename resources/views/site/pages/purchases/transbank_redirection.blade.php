<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Redireccionando a Transbank</title>
</head>
<body>
    <form action="{{ $data['formAction'] }}" method="post" id="form">
        <input type="hidden" name="token_ws" value="{{ $data['tokenws'] }}">
    </form>
</body>
<script>
    document.getElementById("form").submit();
</script>
</html>