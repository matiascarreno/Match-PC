@extends('site/layout')

@section('title', 'Nueva Cotización')

@section('css')

    <!-- Colorpicker Css -->
    <link href="{{ asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="{{ asset('plugins/dropzone/dropzone.css') }}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset('plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">

    <!-- Bootstrap Spinner Css -->
    <link href="{{ asset('plugins/jquery-spinner/css/bootstrap-spinner.css') }}" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="{{ asset('plugins/nouislider/nouislider.min.css') }}" rel="stylesheet" />

@endsection

<!-- Top Bar -->
@section('top-bar')
    @include('site/segments/top-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Left Bar -->
@section('left-bar')
    @include('site/segments/left-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Right Bar -->
@section('right-bar')
    @include('site/segments/right-bar/right-bar')
@endsection
<!-- #Right Bar -->

<!-- Content -->
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>NUEVA COTIZACIÓN</h2>
            </div>

            <form method="POST" id="form" action="/new_quotations">
            {{ csrf_field() }}
            
            <!-- Input Group Precio y uso -->
            <div class="row clearfix">                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Importancia de cada caso de uso
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Grado de importancia para los usos más comunes" data-content="Es importante reconocer los motivos principales para los cuales será utilizado el hardware computacional, por lo que recomendamos elegir las opciones mas acordes a sus necesidades que busca satisfacer con el hardware.">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <p>
                                        <b>Ofimática</b>
                                    </p>
                                    <select class="form-control show-tick" name="office" data-live-search="true" required>
                                        <option value="0">Irrelevante</option>
                                        <option value="1">La usaré de vez en cuando</option>
                                        <option value="2">Ocacionalmente necesito editar uno que otro documento</option>
                                        <option value="3">Frecuentemente debo realizar trabajos o informes</option>
                                        <option value="4">Es muy importante que pueda estar cómodo al trabajar en ofimática</option>
                                        <option value="5">Estoy gran parte de el día en esto</option>
                                        <option value="6">Es a lo que me dedico, necesito lo mejor</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <b>Videojuegos</b>
                                    </p>
                                    <select class="form-control show-tick" name="game" data-live-search="true" required>
                                        <option value="0">No juego</option>
                                        <option value="1">Muy rara vez abro un juego liviano</option>
                                        <option value="2">De vez en cando juego algún juego casual</option>
                                        <option value="3">Considero jugar medianamente importante</option>
                                        <option value="4">Me parece importante ser capaz de juegar juegos actuales, aunque no soy exigente</option>
                                        <option value="5">Quiero jugar, pero la experiencia debe ser excelente</option>
                                        <option value="6">Es a lo que me dedico, necesito lo mejor</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <b>Creación de contenido</b>
                                    </p>
                                    <select class="form-control show-tick" name="content_creation" data-live-search="true">
                                        <option value="0">Irrelevante</option>
                                        <option value="1">Muy rara vez necesito correjir una pequeña imagen</option>
                                        <option value="2">Ocasionalmente edito imagenes por hobby</option>
                                        <option value="3">Ocasionalmente edito videos por hobby</option>
                                        <option value="4">Frecuentemente debo hacer uso de programas de edición de contenido</option>
                                        <option value="5">Necesito poder editar perfectamente grandes proyectos</option>
                                        <option value="6">Es a lo que me dedico, necesito lo mejor</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <b>Modelado 3D</b>
                                    </p>
                                    <select class="form-control show-tick" name="model" data-live-search="true">
                                        <option value="0">Ninguna</option>
                                        <option value="1">Muy rara vez necesito modelar una pequeña pieza</option>
                                        <option value="2">Ocasionalmente modelo por hobby</option>
                                        <option value="3">Ocasionalmente modelo piezas complejas por hobby</option>
                                        <option value="4">Creo modelos 3D frecuentemente</option>
                                        <option value="5">Necesito hacer modelos 3D muy frecuentemenmte</option>
                                        <option value="6">Es a lo que me dedico, necesito lo mejor</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input Group Precio y uso -->

            <!-- Precio -->
            <div class="row clearfix">
                <!-- Spinners -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Presupuesto
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Ingresa el presupuesto máximo" data-content="Nosotros te recomendaremos el mejor PC para ti, pero a veces no es posible encontrar algo adecuado cuando el presupuesto es muy bajo, por lo que lo mejor es decirnos cuánto es lo máximo que podrías invertir en el computador y en caso de que haya una opción que cumpla todas tus necesidades a un precio menor, te lo haremos saber.">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <p><b>Precio máximo de la cotización</b></p>
                                    <div id="nouislider_basic_example"></div>
                                    <div class="m-t-20 font-15"><b>Valor: </b><span class="js-nouislider-value"></span></div>
                                    <input type="hidden" id="budget" name="budget">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Spinners -->
            </div>

            <!-- Multi Select Tipo de PC -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ¿Qué tipo de equipo estas buscando?
                            </h2>
                            <small>Opcional</small>
                            <ul class="header-dropdown m-r--5">
                                <li>
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Selecciona lo que estás dispuesto a comprar" data-content="Si existe alguna preferencia en algún tipo de hardware que se busca, tanto por tendencia o características propias que lo hacen especial.">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <select id="optgroup_type_pc" class="ms" multiple="multiple">
                                <optgroup label="Notebooks">
                                    <option value="2">Ultralivianos</option>
                                    <option value="3">Empresariales</option>
                                    <option value="4">Gamer</option>
                                    <option value="5">Convertibles 2 en 1</option>
                                    <option value="6">MacBook</option>
                                </optgroup>
                                <optgroup label="Desktops">
                                    <option value="7">Armados</option>
                                    <option value="8">All in one</option>
                                    <option value="9">iMac</option>
                                </optgroup>
                            </select>
                            <input type="hidden" id="type_pc" name="type_pc">
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Multi Select Tipo de PC -->

            <!-- Multi Select Programas-->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ¿Qué programas esperas ejecutar en el equipo? 
                            </h2>
                            <small>Opcional</small>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Seleciona los programas que usas frecuentemente" data-content="Para reconocer y brindar una mejor cotización, ofrecemos una gran gamma de software que se utilizan mayormente hoy en día entre las personas para un sinfín de utilidades, si existe algunas que desees utilizar en tu futuro hardware, selecciónalos.">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <select id="optgroup_programs" class="ms" multiple="multiple">
                                <optgroup label="Ofimática">
                                    <option value="OFF">Office Suite</option>
                                    <option value="OFF">iWork</option>
                                    <option value="ADR">Adobe Reader</option>
                                    <option value="SKY">Skype</option>
                                </optgroup>
                                <optgroup label="Edición de Video">
                                    <option value="SVP">Sony Vegas Pro</option>
                                    <option value="FCP">Final Cut Pro</option>
                                    <option value="APP">Adobe Premiere Pro</option>
                                    <option value="AAE">Adobe After Effects</option>
                                    <option value="GOP">GoPro Studio</option>
                                    <option value="CMT">Camtasia Studio</option>
                                </optgroup>
                                <optgroup label="Edición de Fotografía">
                                    <option value="ADP">Adobe Photoshop</option>
                                    <option value="INK">Inkscape</option>
                                    <option value="ADL">Adobe Lightroom</option>
                                    <option value="ADI">Adobe Ilustrator</option>
                                    <option value="GMP">GIMP</option>
                                </optgroup>
                                <optgroup label="CAD">
                                    <option value="ADP">AutoCad</option>
                                    <option value="INK">Fusion 360</option>
                                    <option value="ADL">SolidWorks</option>
                                    <option value="ADI">ArchiCAD</option>
                                    <option value="GMP">Blender</option>
                                </optgroup>
                                <optgroup label="Otros">
                                    <option value="MTL">MatLab</option>
                                    <option value="WKA">WEKA</option>
                                    <option value="WKA">Visual Studio</option>
                                    <option value="WKA">Android Studio</option>
                                    <option value="3DS">3DS Max</option>
                                    <option value="WKA">Android Studio</option>
                                </optgroup>
                            </select>
                            <input type="hidden" name="programs" id="programs">
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Multi Select Programas -->

            <!-- Multi Select Juegos-->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ¿Qué juegos esperas correr en el equipo? 
                            </h2>
                            <small>Opcional</small>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Selecciona los juegos que esperas ejecutar" data-content="Si eres un fan de los video juegos, y deseas jugar un o varios juegos en especial en tu hardware, selecciónalos en el formulario, para así tener una mejor recomendación para tus necesidades.">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <select id="optgroup_games" class="ms" name="use_programs" multiple="multiple">
                                <optgroup label="Ligeros">
                                    <option value="LOL">League of Legends</option>
                                    <option value="WOW">World Of Warcraft</option>
                                    <option value="TF2">Team Fortress 2</option>
                                    <option value="AOE">Age Of Empires 2</option>
                                </optgroup>
                                <optgroup label="FPS Competitivos">
                                    <option value="CSG">Counter Strike Global Offensive</option>
                                    <option value="OVW">Overwatch</option>
                                    <option value="FTN">Fortnite</option>
                                    <option value="PUB">Playerunknown’s Battlegrounds</option>
                                </optgroup>
                                <optgroup label="Juegos Demandantes">
                                    <option value="GTA">GTA V</option>
                                    <option value="RE7">Resident Evil 7</option>
                                    <option value="TW3">The Witcher 3</option>
                                    <option value="F18">Fifa 18</option>
                                    <option value="F18">Fallout 76</option>
                                </optgroup>
                                <optgroup label="Otros">
                                    <option value="NFS">Need For Speed Rivals</option>
                                    <option value="HRS">Hearthstone</option>
                                    <option value="ARK">Ark: Survival Evolved</option>
                                    <option value="CEL">Celeste</option>
                                    <option value="DBF">Dragon ball Fighter Z</option>
                                </optgroup>
                                <input type="hidden" id="games" name="games">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Multi Select Juegos -->

            <!-- Advanced Select Preferencias -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Preferencias personales acerca del Equipo
                                <small>Opcionales, solo para necesidades perticulares.</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Preferencias más específicas acerca de el PC que buscas." data-content="Si tienes alguna preferencia en especial para tu hardware, como por ej: tamaño de pantalla, cantidad de almacenamiento, entre otros, selecciona tus preferencias para así obtener un producto mas acorde a tus necesidades, en caso de no tener dejarlo con las opciones predeterminadas.">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-3 col-sm-6">
                                    <p data-trigger="hover" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="Almacenamiento mínimo que necesitas en tu futuro computador.">
                                        <b>Cantidad de almacenamiento</b>
                                    </p>
                                    <select class="form-control show-tick" data-live-search="true" name="storage_space" required>
                                        <option value="-">No lo sé</option>
                                        <option value="-500">Menos de 500GB</option>
                                        <option value="500-1000">Entre 500GB y 1TB</option>
                                        <option value="1000-2000">Entre 1TB y 2TB</option>
                                        <option value="+">Mientras más, mejor</option>
                                    </select>

                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <p data-trigger="hover" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="Tipo de almacenamiento que debe tener el PC para cumplir con tus necesidades.">
                                        <b>Tipo de Almacenamiento</b>
                                    </p>
                                    <select class="form-control show-tick" data-live-search="true"  name="storage_type">
                                        <option value="-">No lo sé</option>
                                        <option value="HDD">Solo HDD</option>
                                        <option value="SSD">Solo SSD</option>
                                        <option value="NVME">Solo SSD NVME</option>
                                        <option value="SSD-HDD">SSD y HDD</option>
                                        <option value="NVME-HDD">SSD NVME y HDD</option>
                                        <option value="NVME-SSD">SSD NVME y SSD SATA</option>
                                    </select>

                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <p data-trigger="hover" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="Resolución mínima que consideras usabele en tu tipo de uso al PC.">
                                        <b>Resolución Pantalla</b>
                                    </p>
                                    <select class="form-control show-tick" data-live-search="true" name="screen_res">
                                        <option value="">No lo sé</option>
                                        <option value="HD">HD (1280x720 píxeles)</option>
                                        <option value="FHD">FHD (1920x1080 píxeles)</option>
                                        <option value="QHD">QHD (2560x1440 píxeles)</option>
                                        <option value="4K">4K (3840x2160 píxeles)</option>
                                    </select>

                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <p data-trigger="hover" multiple data-container="body" data-toggle="popover" data-placement="top" title="" data-content="Tamaño de pantalla que necesitas tenga el PC.">
                                        <b>Tamaño de pantalla</b>
                                    </p>
                                    <select class="form-control show-tick" data-live-search="true" name="screen_size">
                                        <option value="">No lo sé</option>
                                        <option value="10">10"</option>
                                        <option value="11">11"</option>
                                        <option value="12">12"</option>
                                        <option value="13">13"</option>
                                        <option value="14">14"</option>
                                        <option value="15">15"</option>
                                        <option value="17">17"</option>
                                        <option value="19">19"</option>
                                        <option value="20">20"</option>
                                        <option value="21">21"</option>
                                        <option value="23">23"</option>
                                        <option value="27">27"</option>
                                        <option value="28">28"</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <p data-trigger="hover" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="Cantidad mínima de RAM que debe tener el PC por preferencias personales.">
                                        <b>Cantidad de RAM</b>
                                    </p>
                                    <select class="form-control show-tick" data-live-search="true"  name="ram">
                                        <option value="">No lo sé</option>
                                        <option value="2+">2GB o más</option>
                                        <option value="4+">4GB o más</option>
                                        <option value="6+">6GB o más</option>
                                        <option value="8+">8GB o más</option>
                                        <option value="10+">10GB o más</option>
                                        <option value="12+">12GB o más</option>
                                        <option value="16+">16GB o más</option>
                                        <option value="32+">32GB o más</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <p data-trigger="hover" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="Sistema operativo con el que deseas venga el PC de Fábrica.">
                                        <b>Sistema Operativo</b>
                                    </p>
                                    <select class="form-control show-tick" data-live-search="true"  name="SO">
                                        <option value="">No lo sé</option>
                                        <option value="WIN7">Windows 7</option>
                                        <option value="WIN8">Windows 8</option>
                                        <option value="WIN10">Windows 10</option>
                                        <option value="MAC">MacOS</option>
                                        <option value="LNX">Linux</option>
                                        <option value="FRE">FreeDOS</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <p data-trigger="hover" data-container="body" data-toggle="popover" data-placement="top" title="" data-content="Otras características extra que son importantes para tí en el PC">
                                        <b>Otros (Múltiple)</b>
                                    </p>
                                    <select class="form-control show-tick" multiple data-live-search="true" id="select_other">
                                        <option value="">ninguno</option>
                                        <option value="numpad">Teclado numérico</option>
                                        <option value="retroiluminado">Teclado retroiluminado</option>
                                        <option value="USBC">USB-C</option>
                                        <option value="TUNDERBOT">Thunderbolt 3</option>
                                        <option value="TOUCH">Pantalla Táctil</option>
                                        <option value="DVD">Unidad de DVD</option>
                                    </select>
                                    <input type="hidden" id="other" name="other">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Select Preferencias -->

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="demo-button font-20">
                                <button type="submit" class="btn btn-block btn-lg btn-success waves-effect">SORPRÉNDEME!</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{ Form::close() }}

        </div>
    </section>
    
@endsection
<!-- #Content -->

@section('js')

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="{{ asset('plugins/multi-select/js/jquery.multi-select.js') }}"></script>

    <!-- noUISlider Plugin Js -->
    <script src="{{ asset('plugins/nouislider/nouislider.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/pages/forms/advanced-form-elements.js') }}"></script>
    <script src="{{ asset('js/pages/ui/tooltips-popovers.js') }}"></script>

@endsection