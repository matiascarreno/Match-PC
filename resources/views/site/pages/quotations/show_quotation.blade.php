@extends('site/layout')

@section('title', 'Nueva Cotización')

@section('css')

@endsection

<!-- Top Bar -->
@section('top-bar')
    @include('site/segments/top-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Left Bar -->
@section('left-bar')
    @include('site/segments/left-bar/user')
@endsection
<!-- #Top Bar -->

<!-- Right Bar -->
@section('right-bar')
    @include('site/segments/right-bar/right-bar')
@endsection
<!-- #Right Bar -->

<!-- Content -->
@section('content')
    <div class="btn-group" role="group" aria-label="First group" style="position: fixed; z-index: 1000; bottom: 15px; right: 15px;">
        
        @php
            $cont = 1;
        @endphp

        @foreach ($products as $product)
            @php
                switch ($cont) 
                {
                    case 1:
                        $color = "cyan";
                        break;
                    case 2:
                        $color = "teal";
                        break;
                    case 3:
                        $color = "orange";
                        break;
                    case 4:
                        $color = "pink";
                        break;
                }
            @endphp
            
            <button onclick="window.location.href='#option_{{ $product->id_product }}'" class="btn bg-{{ $color }} waves-effect">Opción N°{{ $cont }}</button>
        @php
            $cont++;
        @endphp
        @endforeach
    </div>
    <section class="content">
        <div class="container-fluid">
            
            <div class="block-header">
                    @if (count($products) > 0)
                        <h1 class="align-center col-grey">Entre estas opciones, puede estar tu PC ideal</h1> 
                    @else
                        <h1 class="align-center col-grey">Vaya, parece que no hemos podido encontrar nada para tí con las características que especificaste, puedes:</h1>
                        <a href="javascript:history.back()">
                            <h3 class="align-center">Cambiar alguno de las opciones de el formulario anterior, o bien... </h3>
                        </a>
                        @if(Auth::user()->isFree())
                            <a href="/premium">
                                <h3 class="align-center">Obtener un plan Premium para asesorarte con un especialista</h3>
                            </a>
                        @else
                            <a href="/messages">
                                <h3 class="align-center">Preguntarle a tu asesor</h3>
                            </a>
                        @endif
                        <h3 class="align-center col-grey"></h3>
                    @endif
                
            </div>

            <br>

            @php
                $cont = 0;
                $color = "";
            @endphp
            
            @foreach ($products as $product)

            @php
                switch ($cont) 
                {
                    case 0:
                        $color = "cyan";
                        $rgb_color = "#00BCD4";
                        break;
                    case 1:
                        $color = "teal";
                        $rgb_color = "#009688";
                        break;
                    case 2:
                        $color = "orange";
                        $rgb_color = "#FF9800";
                        break;
                    case 3:
                        $color = "pink";
                        $rgb_color = "#E91E63";
                        break;
                }
            @endphp

            <!-- Opción N°{{ $product->id_product }} -->
            <div class="row clearfix p-t-100" id="option_{{ $product->id_product }}">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <div class="card">
                        <div class="header bg-{{ $color }}">
                            <h1 class="margin-0 font-normal">
                                {{ $product->name }}
                            </h1>
                        </div>
                        <div class="body">
                            <img class="img-responsive thumbnail" src="{{ $product->picture }}">
                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-{{ $color }}">
                            <h2>
                                Cómpralo ahora mismo!
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Popover Title" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @php
                            $tiendas = $product->getStores();
                        @endphp
                        <div class="body">
                            @foreach ($tiendas as $tienda)
                            <a href="{{ $tienda->link }}" target="_blank" class="list-group-item font-20">
                                <span class="badge bg-transparent col-{{ $color }} font-20">${{ $tienda->price }}</span> {{ $tienda->name }}
                            </a>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                    <div class="card">
                        <div class="header bg-{{ $color }}">
                            <h2>
                                Ficha Técnica
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Popover Title" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                                {!! $product->description !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix align-center">
                                <div class="col-md-3 col-sm-6">
                                    <h3>
                                        Potencia CPU
                                    </h3>
                                    <input type="text" class="knob" value="{{ (int)$product->getScore("cpu") }}" data-width="125" data-height="125" data-thickness="0.25" data-fgColor="{{ $rgb_color }}" readonly>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <h3>
                                        Potencia Gráfica
                                    </h3>
                                    <input type="text" class="knob" value="{{ (int)$product->getScore("gpu") }}" data-width="125" data-height="125" data-thickness="0.25" data-fgColor="{{ $rgb_color }}" readonly>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <h3>
                                        Almacenamiento
                                    </h3>
                                    <input type="text" class="knob" value="{{ (int)(($product->getScore("hdd")*.25+$product->getScore("ssd")*.75)/2) }}" data-width="125" data-height="125" data-thickness="0.25" data-fgColor="{{ $rgb_color }}" readonly>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <h3>
                                        Factor Cool (Promedio)
                                    </h3>
                                    <input type="text" class="knob" value="{{ (int)(($product->getScore("cpu")+$product->getScore("gpu")+$product->getScore("ram")+$product->getScore("ssd")*.75+$product->getScore("hdd")*.25)/5) }}" data-width="125" data-height="125" data-thickness="0.25" data-fgColor="{{ $rgb_color }}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Opción N°X -->

            @php
                $cont++;
            @endphp

            @endforeach
            
            @if (count($products) > 0)
            <!-- Comparaciones -->
            <div class="row clearfix">
                <!-- Radar Chart -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Gráfico de comparación 1</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Grafico de Radar" data-content="Cada uno de los vértices representa un aspecto de el computador, y se podría decir, que el que abarque mayor área es el mejor en promedio y el más balanceado (aunque eso no siempre es desicivo).">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <canvas id="radar_chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
                <!-- #END# Radar Chart -->
                <!-- Bar Chart -->
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Gráfico de comparación 2</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left" title="Grafico de Barras" data-content="Este es más para comparación directa de los diferentes aspectos de cada PC, depende de el uso que tendrá la importancia de cada aspecto presentado es este gráfico.">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <canvas id="bar_chart" height="150"></canvas>
                        </div>
                    </div>
                </div>
                <!-- #END# Bar Chart -->
            </div>
            <!-- #END# Comparaciones -->
            @endif
        </div>
    </section>
    
@endsection
<!-- #Content -->

@section('js')

    <!-- Chart Plugins Js -->
    <script src="{{ asset('plugins/chartjs/Chart.bundle.js') }}"></script>

    <!-- Jquery Knob Plugin Js -->
    <script src="{{ asset('plugins/jquery-knob/jquery.knob.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/pages/charts/chartjs.js') }}"></script>
    <script src="{{ asset('js/pages/ui/tooltips-popovers.js') }}"></script>
    <script src="{{ asset('js/pages/charts/jquery-knob.js') }}"></script>

    <script>
        function getChartJs(type) {
        var config = null;

        if (type === 'bar') 
        {
            config = {
                type: 'bar',
                data: {
                    labels: ["CPU", "GPU", "RAM", "Almacenamiento", "Factor Cool!"],
                    datasets: 
                    [
                        @php
                            $cont=0;

                        @endphp

                        @foreach ($products as $product)
                        @php
                            switch ($cont) 
                            {
                                case 0:
                                    $color = "0, 188, 212";
                                    break;
                                case 1:
                                    $color = "0, 150, 136";
                                    break;
                                case 2:
                                    $color = "255, 152, 0";
                                    break;
                                case 3:
                                    $color = "233, 30, 99";
                                    break;
                            }
                        @endphp

                        {
                            label: "{{ $product->name }}",
                            data: 
                                [ 
                                    {{ (int)$product->getScore("cpu") }}, 
                                    {{ (int)$product->getScore("gpu") }}, 
                                    {{ (int)$product->getScore("ram") }}, 
                                    {{ (int)(($product->getScore("hdd")*.25+$product->getScore("ssd")*.75)/2) }}, 
                                    {{ (int)(($product->getScore("cpu")+$product->getScore("gpu")+$product->getScore("ram")+$product->getScore("ssd")*.75+$product->getScore("hdd")*.25)/5) }}
                                ],
                            backgroundColor: 'rgba({{ $color }}, 0.8)',
                        },
                        @php
                            $cont++;
                        @endphp
                        @endforeach
                    ]
                },
                options: {
                    responsive: true,
                    legend: false
                }
            }
        }
        else if (type === 'radar') 
        {
            config = {
                type: 'radar',
                data: {
                    labels: ["CPU", "GPU", "RAM", "Almacenamiento", "Factor Cool!"],
                    datasets: 
                    [
                        @php
                            $cont=0;

                        @endphp

                        @foreach ($products as $product)
                        @php
                            switch ($cont) 
                            {
                                case 0:
                                    $color = "0, 188, 212";
                                    break;
                                case 1:
                                    $color = "0, 150, 136";
                                    break;
                                case 2:
                                    $color = "255, 152, 0";
                                    break;
                                case 3:
                                    $color = "233, 30, 99";
                                    break;
                            }
                        @endphp

                        {
                            label: "{{ $product->name }}",
                            data: 
                                [ 
                                    {{ (int)$product->getScore("cpu") }}, 
                                    {{ (int)$product->getScore("gpu") }}, 
                                    {{ (int)$product->getScore("ram") }}, 
                                    {{ (int)(($product->getScore("hdd")*.25+$product->getScore("ssd")*.75)/2) }}, 
                                    {{ (int)(($product->getScore("cpu")+$product->getScore("gpu")+$product->getScore("ram")+$product->getScore("ssd")*.75+$product->getScore("hdd")*.25)/5) }}
                                ],
                            borderColor: 'rgba({{ $color }}, 0.4)',
                            backgroundColor: 'rgba({{ $color }}, 0.3)',
                            pointBorderColor: 'rgba({{ $color }}, 0)',
                            pointBackgroundColor: 'rgba({{ $color }}, 0.8)',
                            pointBorderWidth: 1
                        },
                        @php
                            $cont++;
                        @endphp
                        @endforeach

                        /* {
                            label: "Primera Opción",
                            data: [88, 80, 50, 80, 86],
                            borderColor: 'rgba(0, 188, 212, 0.4)',
                            backgroundColor: 'rgba(0, 188, 212, 0.3)',
                            pointBorderColor: 'rgba(0, 188, 212, 0)',
                            pointBackgroundColor: 'rgba(0, 188, 212, 0.8)',
                            pointBorderWidth: 1
                        },
                        {
                            label: "Segunda Opción",
                            data: [72, 48, 40, 19, 96],
                            borderColor: 'rgba(0, 150, 136, 0.7)',
                            backgroundColor: 'rgba(0, 150, 136, 0.4)',
                            pointBorderColor: 'rgba(0, 150, 136, 0)',
                            pointBackgroundColor: 'rgba(0, 150, 136, 0.8)',
                            pointBorderWidth: 1
                        },
                        {
                            label: "Tercera Opción",
                            data: [79, 70, 55, 77, 70],
                            borderColor: 'rgba(255, 152, 0, 0.7)',
                            backgroundColor: 'rgba(255, 152, 0, 0.4)',
                            pointBorderColor: 'rgba(255, 152, 0, 0)',
                            pointBackgroundColor: 'rgba(255, 152, 0, 0.8)',
                            pointBorderWidth: 1
                        },
                        {
                            label: "Cuarta Opción",
                            data: [90, 70, 70, 90, 86],
                            borderColor: 'rgba(233, 30, 99, 0.7)',
                            backgroundColor: 'rgba(233, 30, 99, 0.4)',
                            pointBorderColor: 'rgba(233, 30, 99, 0)',
                            pointBackgroundColor: 'rgba(233, 30, 99, 0.8)',
                            pointBorderWidth: 1
                        } */
                    ]
                },
                options: {
                    responsive: true,
                    legend: false
                }
            }
        }
        return config;
        }           
    </script>

@endsection