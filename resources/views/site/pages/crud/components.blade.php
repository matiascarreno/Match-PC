@extends('site/layout')

@section('title', 'CRUD Componentes')

@section('css')

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

@endsection

<!-- Top Bar -->
@section('top-bar')
    @include('site/segments/top-bar/administrator')
@endsection
<!-- #Top Bar -->

<!-- Left Bar -->
@section('left-bar')
    @include('site/segments/left-bar/administrator')
@endsection
<!-- #Top Bar -->

<!-- Right Bar -->
@section('right-bar')
    @include('site/segments/right-bar/right-bar')
@endsection
<!-- #Right Bar -->

<!-- Content -->
@section('content')

    <section class="content">
        <div class="container-fluid">

            <!-- Input Group -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                MANTENEDOR DE COMPONENTES
                            </h2>
                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <!-- Input -->
                            <form action="javascript:void(0);" id="form_validation">
                            {{ csrf_field() }}
                            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_1</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="ID Componente" id="id_component" name="id_component" disabled="true" value="">
                                        </div>
                                        <div class="help-info">ID, solo usable para editar o eliminar (Buscar en la tabla inferior)</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_2</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Nombre Componete*" id="name" name="name" value="" required minlength="3" maxlength="100">
                                        </div>
                                        <div class="help-info">Min. 3, Max. 200 caracteres</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_3</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Descripción Componente*" id="description" name="description" value="" required minlength="3" maxlength="1000">
                                        </div>
                                        <div class="help-info">Min. 3, Max. 1000 caracteres</div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_4</i>
                                        </span>
                                        <select class="form-control show-tick" name="type" id="type" onchange="disable_specs()" required>
                                            <option value="">Tipo de componente</option>
                                            <option value="1">CPU</option>
                                            <option value="2">GPU</option>
                                            <option value="3">RAM</option>
                                            <option value="4">SSD</option>
                                            <option value="5">HDD</option>
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Inactivo</strong></u></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_5</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="Frecuencia" id="freq" name="freq" value="" minlength="3" maxlength="1000">
                                        </div>
                                        <div class="help-info">Frecuencia de el componente en MHz (solo para CPU, GPU, y RAM)</div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="active-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_6</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="Núcleos" id="cores" name="cores" value="" maxlength="10">
                                        </div>
                                        <div class="help-info"><strong><u>Número de núcleos (solo para CPU y GPU)</strong></u></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_7</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="Memoria" id="memory" name="memory" value=""maxlength="100">
                                        </div>
                                        <div class="help-info">Cantidad de memoria en GB (Solo para GPU, RAM, SSD y HDD)</div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="active-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_8</i>
                                        </span>
                                        <select class="form-control show-tick" name="active"  id="active" onchange="" required>
                                            <option value="0">Inactivo</option>
                                            <option value="1">Activo</option>
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Inactivo</strong></u></div>
                                    </div>
                                </div>
                            </div>
                            <!-- #END# Input -->

                            <!-- Benchmarks -->
                            <div class="header m-b-20">
                                <h2>
                                    BenchMakrs
                                </h2>
                            </div>
                            <div class="row clearfix">
                                @foreach($benchmarks as $benchmark)
                                <div class="col-md-12 benchmark type_{{ $benchmark->component_type }}">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            {{ $benchmark->name }}
                                        </span>
                                        <div class="form-line">
                                            <input type="number" class="form-control benchmark_{{ $benchmark->component_type }}" placeholder="Puntuación" id="{{ $benchmark->id_benchmark }}" name="benchmark_{{ $benchmark->id_benchmark }}" value="" maxlength="100" min="{{ $benchmark->min_score }}" max="{{ $benchmark->max_score }}">
                                        </div>
                                        <div class="help-info">La puntuación es entre {{ $benchmark->min_score }} y {{ $benchmark->max_score }}</div>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            <!-- Block Buttons -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="create_button" value="crear" onclick="create()" class="btn btn-block btn-lg bg-blue waves-effect">Crear</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="edit_button" value="editar" onclick="edit()" class="btn btn-block btn-lg bg-green waves-effect" disabled>Editar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="delete_button" value="eliminar" onclick="drop()" class="btn btn-block btn-lg bg-pink waves-effect" disabled>Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- #END# Block Buttons -->
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input Group -->

            <!-- Tabla Jquery -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                LISTADO DE COMPONENTES
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-exportable dataTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th>Tipo</th>
                                            <th>Puntuación</th>
                                            <th>Activo</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center">ID</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Descripción</th>
                                            <th class="text-center">Tipo</th>
                                            <th class="text-center">Puntuación</th>
                                            <th class="text-center">Activo</th>
                                            <th class="text-center">Editar</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tabla Jquery -->

        </div>
    </section>
    
@endsection
<!-- #Content -->

@section('js')

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('js/pages/crud/components.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Validation -->
    <script src="{{ asset('js/pages/forms/form-validation.js') }}"></script>   

@endsection