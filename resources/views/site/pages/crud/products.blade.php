@extends('site/layout')

@section('title', 'CRUD Productos')

@section('css')

<!-- JQuery DataTable Css -->
<link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- Bootstrap Select Css -->
<link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

@endsection

<!-- Top Bar -->
@section('top-bar')
@include('site/segments/top-bar/administrator')
@endsection
<!-- #Top Bar -->

<!-- Left Bar -->
@section('left-bar')
@include('site/segments/left-bar/administrator')
@endsection
<!-- #Top Bar -->

<!-- Right Bar -->
@section('right-bar')
@include('site/segments/right-bar/right-bar')
@endsection
<!-- #Right Bar -->

<!-- Content -->
@section('content')

<section class="content">
    <div class="container-fluid">

        <!-- Input Group -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            MANTENEDOR DE PRODUCTOS
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="javascript:void(0);" id="form_validation">
                            <!-- #END# Datos Principales -->
                            {{ csrf_field() }}
                            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_1</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="ID Producto" name="id_product" id="id_product" disabled="true" value="">
                                        </div>
                                        <div class="help-info">ID, solo usable para editar o eliminar (Buscar en la
                                            tabla inferior)</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_2</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Nombre Producto*" name="name" id="name" value="" required minlength="3" maxlength="200">
                                        </div>
                                        <div class="help-info">Min. 3, Max. 200 caracteres</div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-md-4" id="category-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_3</i>
                                        </span>
                                        <select class="form-control show-tick" name="category" id="category" required>
                                            @foreach($categories as $category)
                                            <option value="{{$category->id_category}}">{{$category->description}}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Por definir</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_6</i>
                                        </span>
                                        <select class="form-control show-tick" name="active" id="active" required>
                                            <option value="0">Inactivo</option>
                                            <option value="1">Activo</option>
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Inactivo</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_7</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="url" class="form-control" placeholder="URL Imagen" name="image" id="image" value="" minlength="3" maxlength="200">
                                        </div>
                                        <div class="help-info"><strong><u>Valor por defecto: Null</u></strong></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="body">
                                    <textarea id="tinymce">
                                        <h2>Descripción de el Producto</h2>
                                    </textarea>
                                </div>
                            </div>
                            <!-- #END# Datos Principales -->

                            <!-- Precios y tiendas -->
                            <div class="header m-b-20">
                                <h2>
                                    Precios
                                </h2>
                            </div>

                            <!-- Tienda -->
                            @foreach($stores as $store)
                            <div class="row clearfix products_stores" id="{{ $store->id_store }}">
                                <div class="col-md-2" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                                <i class="material-icons">store</i>
                                        </span>
                                        <h4>{{ $store->name }}</h4>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">open_in_browser</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="url" class="form-control" placeholder="URL 1" name="link_{{ $store->id_store }}" id="link_{{ $store->id_store }}" value="" minlength="3" maxlength="200">
                                        </div>
                                        <div class="help-info"><strong><u>Valor por defecto: Inactivo</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">attach_money</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="Precio" name="price_{{ $store->id_store }}" id="price_{{ $store->id_store }}" value="" minlength="3" maxlength="200">
                                        </div>
                                        <div class="help-info"><strong><u>Valor por defecto: Null</u></strong></div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- #END# Tienda -->


                            <!-- #END# Precios y tiendas -->

                            <!-- Componentes -->
                            <div class="header m-b-20">
                                <h2>
                                    Características
                                </h2>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            CPU
                                        </span>
                                        <select class="form-control show-tick" name="cpu" id="cpu" data-live-search="true">
                                            <option value="">Por definir</option>
                                            @foreach($components as $component)
                                                @if($component->type == 1)
                                                    <option value="{{$component->id_component}}">{{$component->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Inactivo</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            GPU
                                        </span>
                                        <select class="form-control show-tick" name="gpu" id="gpu" data-live-search="true" required>
                                            <option value="">Por definir</option>
                                            @foreach($components as $component)
                                                @if($component->type == 2)
                                                    <option value="{{$component->id_component}}">{{$component->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Por Definir</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            RAM
                                        </span>
                                        <select class="form-control show-tick" name="ram" id="ram" data-live-search="true" required>
                                            <option value="">Por definir</option>
                                            @foreach($components as $component)
                                                @if($component->type == 3)
                                                    <option value="{{$component->id_component}}">{{$component->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Por Definir</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            SSD
                                        </span>
                                        <select class="form-control show-tick" name="ssd" id="ssd" data-live-search="true">
                                            <option value="">Por definir</option>
                                            @foreach($components as $component)
                                                @if($component->type == 4)
                                                    <option value="{{$component->id_component}}">{{$component->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Por Definir</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            HDD
                                        </span>
                                        <select class="form-control show-tick" name="hdd" id="hdd" data-live-search="true">
                                            <option value="">Por definir</option>
                                            @foreach($components as $component)
                                                @if($component->type == 5)
                                                    <option value="{{$component->id_component}}">{{$component->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Por Definir</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            SCZ
                                        </span>
                                        <select class="form-control show-tick" name="scz" id="scz" data-live-search="true" required>
                                            <option value="">Por definir</option>
                                            <option value="10">10"</option>
                                            <option value="11">11"</option>
                                            <option value="12">12"</option>
                                            <option value="13">13"</option>
                                            <option value="14">14"</option>
                                            <option value="15">15"</option>
                                            <option value="17">17"</option>
                                            <option value="19">19"</option>
                                            <option value="20">20"</option>
                                            <option value="21">21"</option>
                                            <option value="23">23"</option>
                                            <option value="27">27"</option>
                                            <option value="28">28"</option>
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Por Definir</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            SCR
                                        </span>
                                        <select class="form-control show-tick" name="scr" id="scr" data-live-search="true" required>
                                            <option value="">Por definir</option>
                                            <option value="HD">HD (1280x720 píxeles)</option>
                                            <option value="FHD">FHD (1920x1080 píxeles)</option>
                                            <option value="QHD">QHD (2560x1440 píxeles)</option>
                                            <option value="4K">4K (3840x2160 píxeles)</option>
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Por Definir</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            Otros
                                        </span>
                                        <select class="form-control show-tick" name="other" id="other" multiple data-live-search="true">
                                            <option data-hidden="true"></option>
                                            <option value="numpad">Teclado numérico</option>
                                            <option value="retroiluminado">Teclado retroiluminado</option>
                                            <option value="USBC">USB-C</option>
                                            <option value="TUNDERBOT">Thunderbolt 3</option>
                                            <option value="TOUCH">Pantalla Táctil</option>
                                            <option value="DVD">Unidad de DVD</option>
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Ninguno</u></strong></div>
                                    </div>
                                </div>
                            </div>
                            <!-- #END# Componentes -->

                            <!-- Block Buttons -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="create_button" value="crear" onclick="create()" class="btn btn-block btn-lg bg-blue waves-effect">Crear</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="edit_button" value="editar" onclick="edit()" class="btn btn-block btn-lg bg-green waves-effect">Editar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="delete_button" value="eliminar" onclick="drop()" class="btn btn-block btn-lg bg-pink waves-effect">Eliminar</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- #END# Block Buttons -->
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Input Group -->

        <!-- Tabla Jquery -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            LISTADO DE PRODUCTOS
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-exportable dataTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Categoría</th>
                                        <th>Especificaciones</th>
                                        <th>Activo</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Categoría</th>
                                        <th class="text-center">Especificaciones</th>
                                        <th class="text-center">Activo</th>
                                        <th class="text-center">Editar</th>
                                    </tr>
                                </tfoot>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Tabla Jquery -->

    </div>
</section>

@endsection
<!-- #Content -->

@section('js')

<!-- Jquery DataTable Plugin Js -->
<script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

<!-- TinyMCE -->
<script src="{{ asset('plugins/tinymce/tinymce.js') }}"></script>

<!-- Select Plugin Js -->
<script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

<!-- Validation Plugin Js -->
<script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

<!-- Validation -->
<script src="{{ asset('js/pages/forms/form-validation.js') }}"></script>

<!-- Custom Js -->
<script src="{{ asset('js/pages/tables/jquery-datatable.js') }}"></script>
<script src="{{ asset('js/pages/crud/products.js') }}"></script>
<script src="{{ asset('js/pages/forms/editors.js') }}"></script>

@endsection