@extends('site/layout')

@section('title', 'CRUD Usuarios')

@section('css')

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

@endsection

<!-- Top Bar -->
@section('top-bar')
    @include('site/segments/top-bar/administrator')
@endsection
<!-- #Top Bar -->

<!-- Left Bar -->
@section('left-bar')
    @include('site/segments/left-bar/administrator')
@endsection
<!-- #Top Bar -->

<!-- Right Bar -->
@section('right-bar')
    @include('site/segments/right-bar/right-bar')
@endsection
<!-- #Right Bar -->

<!-- Content -->
@section('content')

    <section class="content">
        <div class="container-fluid">

            <!-- Input Group -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                MANTENEDOR DE USUARIOS
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form action="javascript:void(0);" id="form_validation">
                            {{ csrf_field() }}
                            <input type="hidden" id="token" name="token" value="{{ csrf_token() }}">
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_1</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="ID Usuario" name="id_user" id="id_user" disabled="true" value="">
                                        </div>
                                        <div class="help-info">ID, solo usable para editar o eliminar (Buscar en la tabla inferior)</div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_2</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="email" class="form-control" placeholder="Email*" name="email" id="email" value="" required minlength="5" maxlength="100">
                                        </div>
                                        <div class="help-info">Email. Min. 3, Max. 100 caracteres</div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_3</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="password" class="form-control" placeholder="Contraseña" name="password" id="password" value="" minlength="8" maxlength="100">
                                        </div>
                                        <div class="help-info">Solo si se quiere cambiar. Min. 5, Max. 100 caracteres</div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_4</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Nombre*" name="name" id="name" value="" required minlength="3" maxlength="100">
                                        </div>
                                        <div class="help-info">Min. 5, Max. 200 caracteres</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_5</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Apellido*" name="lastname" id="lastname" required minlength="5" maxlength="100">
                                        </div>
                                        <div class="help-info">Min. 5, Max. 200 caracteres</div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_6</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="number" class="form-control" placeholder="Número de teléfono" name="phone_number" id="phone_number" value="" minlength="9" maxlength="9">
                                        </div>
                                        <div class="help-info">Número de teléfono. 9 dígitos</div>
                                    </div>
                                </div>
                                <div class="col-md-4" id="gender-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_7</i>
                                        </span>
                                        <select class="form-control show-tick"  name="gender" id="gender" onchange="" required>
                                            <option value="null">Seleccione un sexo</option>
                                            <option value="M">Masculino</option>
                                            <option value="F">Femenino</option>
                                            <option value="O">Otro</option>
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Nulo</u></strong></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_8</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" class="form-control" placeholder="Fecha de nacimiento*" name="birthdate" id="birthdate" value="">
                                        </div>
                                        <div class="help-info">Fecha de necimiento.</div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_8</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" class="form-control" placeholder="Fecha de la última compra*" name="last_purchase" id="last_purchase" value="">
                                        </div>
                                        <div class="help-info">Fecha de la última compra.</div>
                                    </div>
                                </div>
                                <div class="col-md-3" id="subscription-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_9</i>
                                        </span>
                                        <select class="form-control show-tick"  name="subscription" id="subscription" onchange="" required>
                                            @foreach($subscriptions as $subscription)
                                                <option value="{{$subscription->id_subscription}}">{{$subscription->name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Gratuita</u></strong></div>
                                    </div>
                                </div>
                                <div class="col-md-3" id="active-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_9_plus</i>
                                        </span>
                                        <select class="form-control show-tick" name="active" id="active" onchange="" required>
                                            <option value="0">Inactivo</option>
                                            <option value="1">Activo</option>
                                        </select>
                                        <div class="help-info"><strong><u>Valor por defecto: Inactivo</strong></u></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Block Buttons -->
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="create_button" value="crear" onclick="create()" class="btn btn-block btn-lg bg-blue waves-effect">Crear</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="edit_button" value="editar" onclick="edit()" class="btn btn-block btn-lg bg-green waves-effect" disabled="true">Editar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="body">
                                        <div class="demo-button">
                                            <button type="submit" name="submit" id="delete_button" value="eliminar" onclick="drop()" class="btn btn-block btn-lg bg-pink waves-effect" disabled="true">Eliminar</button>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- #END# Block Buttons -->
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input Group -->

            <!-- Hover Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Listado de Productos
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-exportable dataTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Email</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Número de teléfono</th>
                                            <th>Fecha de nacimiento</th>
                                            <th>Genero</th>
                                            <th>Suscripción</th>
                                            <th>Fecha de última Compra</th>
                                            <th>Activo</th>
                                            <th>Buscar</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Apellido</th>
                                            <th class="text-center">Número de teléfono</th>
                                            <th class="text-center">Fecha de nacimiento</th>
                                            <th class="text-center">Genero</th>
                                            <th class="text-center">Suscripción</th>
                                            <th class="text-center">Fecha de última Compra</th>
                                            <th class="text-center">Activo</th>
                                            <th class="text-center">Buscar</th>
                                        </tr>
                                        </tfoot>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Rows -->
        </div>
    </section>

@endsection
    
<!-- #Content -->

@section('js')

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('js/pages/crud/users.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Validation -->
    <script src="{{ asset('js/pages/forms/form-validation.js') }}"></script>   

@endsection