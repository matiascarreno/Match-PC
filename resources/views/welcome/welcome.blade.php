<!DOCTYPE html>
<html lang="en">

	<head>
	  <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Match-PC</title>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		
    	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
		
		<!-- Bootstrap core CSS -->
		<link href="{{ asset('welcome/css/bootstrap.min.css') }}" rel="stylesheet">

		<!-- Material Design Bootstrap -->
		<link href="{{ asset('welcome/css/mdb.min.css') }}" rel="stylesheet">
		
		<!-- Your custom styles (optional) -->
		<link href="{{ asset('welcome/css/style.min.css') }}" rel="stylesheet">
        
        <style type="text/css">
			html,
			    body,
			    header,
			    .view {
			      height: 100%;
			    }
			
			    @media (max-width: 740px) {
			      html,
			      body,
			      header,
			      .view {
			        height: 1000px;
			      }
			    }
			
			    @media (min-width: 800px) and (max-width: 850px) {
			      html,
			      body,
			      header,
			      .view {
			        height: 650px;
			      }
			    }
			    @media (min-width: 800px) and (max-width: 850px) {
			              .navbar:not(.top-nav-collapse) {
			                  background: #1C2331!important;
			              }
			          }
			
        </style>
        
	</head>

	<body>

		<!-- Navbar -->
		<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
			<div class="container">

				<!-- Brand -->
				<a class="navbar-brand" href="/">
					<strong>Match-PC</strong>
				</a>

				<!-- Collapse -->
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

				<!-- Links -->
				<div class="collapse navbar-collapse" id="navbarSupportedContent">

					<!-- Left -->
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="/">Home
              <span class="sr-only">(current)</span>
            </a>
					</ul>

					<!-- Right -->
					<ul class="navbar-nav nav-flex-icons">
						<li class="nav-item">
							<a href="https://www.facebook.com/" class="nav-link">
								<i class="fa fa-facebook"></i>
							</a>
						</li>
						<li class="nav-item">
							<a href="https://twitter.com/" class="nav-link">
								<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li class="nav-item">
							<a href="/login" class="nav-link border border-light rounded">
								<i class="fa fa-sign-in mr-2"></i>Login
							</a>
						</li>
					</ul>

				</div>

			</div>
		</nav>
		<!-- Navbar -->

		<!-- Full Page Intro -->
		<div class="view full-page-intro" style="background-image: url('{{ asset('images/welcome_bg.jpg') }}'); background-repeat: no-repeat; background-size: cover;">

			<!-- Mask & flexbox options-->
			<div class="mask rgba-black-light d-flex justify-content-center align-items-center">

				<!-- Content -->
				<div class="container">

					<!--Grid row-->
					<div class="row wow fadeIn">

						<!--Grid column-->
						<div class="col-md-12 mb-4 white-text text-center text-md-left">

							<h1 class="display-4 font-weight-bold">Encuéntra el mejor equipo computacional para lo que necesitas</h1>

							<hr class="hr-light">

							<p>
								<strong>Te ayudamos a encontrar el equipo que se adecúe mejor a tus necesidades, y al mejor precio.</strong>
							</p>

							<p class="mb-4 d-none d-md-block">
								<strong>Asesórate con nosotros, y no pierdas más tiempo buscando.</strong>
							</p>

							<a target="_blank" href="/register" class="btn btn-indigo btn-lg">Regístrate
              					<i class="fa fa-sign-in ml-2"></i>
            				</a>

						</div>
						<!--Grid column-->

					</div>
					<!--Grid row-->

				</div>
				<!-- Content -->

			</div>
			<!-- Mask & flexbox options-->

		</div>
		<!-- Full Page Intro -->

		<!--Footer-->
		<footer class="page-footer text-center font-small wow fadeIn">

			<!--Call to action-->
			<div class="pt-4">
				<a class="btn btn-outline-white" href="/login" target="_blank" role="button">Inicia Sesión
      </a>
				<a class="btn btn-outline-white" href="/register" target="_blank" role="button">Regístrate
      </a>
			</div>
			<!--/.Call to action-->

			<hr class="my-4">

			<!-- Social icons -->
			<div class="pb-4">
				<a href="https://www.facebook.com" target="_blank">
					<i class="fa fa-facebook mr-3"></i>
				</a>

				<a href="https://twitter.com" target="_blank">
					<i class="fa fa-twitter mr-3"></i>
				</a>

				<a href="https://www.youtube.com" target="_blank">
					<i class="fa fa-youtube mr-3"></i>
				</a>

				<a href="https://plus.google.com" target="_blank">
					<i class="fa fa-google-plus mr-3"></i>
				</a>

				<a href="https://dribbble.com" target="_blank">
					<i class="fa fa-dribbble mr-3"></i>
				</a>

				<a href="https://pinterest.com" target="_blank">
					<i class="fa fa-pinterest mr-3"></i>
				</a>

				<a href="https://github.com" target="_blank">
					<i class="fa fa-github mr-3"></i>
				</a>

				<a href="http://codepen.io" target="_blank">
					<i class="fa fa-codepen mr-3"></i>
				</a>
			</div>
			<!-- Social icons -->

			<!--Copyright-->
			<div class="footer-copyright py-3">
				© 2018 Copyright:
				<a href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank"> Match-PC </a>
			</div>
			<!--/.Copyright-->

		</footer>
		<!--/.Footer-->

		<!-- SCRIPTS -->
		<!-- JQuery -->
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<!-- Bootstrap tooltips -->
		<script type="text/javascript" src="js/popper.min.js"></script>
		<!-- Bootstrap core JavaScript -->
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<!-- MDB core JavaScript -->
		<script type="text/javascript" src="js/mdb.min.js"></script>
		<!-- Initializations -->
		<script type="text/javascript">
			// Animations initialization
			    new WOW().init();
			
		</script>
	</body>

</html>