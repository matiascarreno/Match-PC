@extends('log_.log_')

@section('title', 'Regístrate')

@section('content')

            <div class="body">
                <form id="sign_up" method="POST" action="/register">
                    {{ csrf_field() }}
                    <div class="msg">Crea tu cuenta...</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" placeholder="Nombre*" value="{{ old('name') }}" required minlength="3" maxlength="200" autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="lastname" placeholder="Apellido*" value="{{ old('lastname') }}" required minlength="3" maxlength="200" autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Correo electrónico*" value="{{ old('email') }}" minlength="5" maxlength="200" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="birthdate" onfocus="(this.type='date')" onblur="(this.type='text')" value="{{ old('birthdate') }}" placeholder="Fecha de nacimiento" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">face</i>
                        </span>
                        <select class="form-control show-tick" name="gender" required>
                            <option>Selecciona tu sexo</option>
                            <option value="M" @if(old('gender')=='M') selected @endif>Masculino</option>
                            <option value="F" @if(old('gender')=='F') selected @endif>Femenino</option>
                            <option value="O" @if(old('gender')=='O') selected @endif>Ni lo uno ni lo otro, mas bien todo lo contrario</option>
                        </select>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Contraseña"  minlength="8" maxlength="200" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="confirm" placeholder="Confirmar Contraseña" minlength="8" maxlength="200" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="terms" id="terms" class="chk-col-pink">
                        <label for="terms">Estoy de acuerdo con los <a href="javascript:void(0);">terminos de uso</a>.</label>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">Crear Cuenta</button>

                    <div class="m-t-25 m-b--5 align-center">
                    <a href="/login">¿Ya tienes una cuenta?</a>
                    </div>
                </form>
            </div>
@endsection


@section('js')

    <!-- Login Js -->
    <script src="{{ asset('js/pages/login/sign-up.js') }}"></script>  

@endsection