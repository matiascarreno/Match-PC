@extends('log_.log_')

@section('title', 'Login')

@section('content')
                
            <div class="body">
                <form id="sign_in" method="POST" action="/login">
                    {{ csrf_field() }}
                    <div class="msg">Ingrese sus credenciales para iniciar sesión</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Contraseña" minlength="8" maxlength="50" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" value="true" class="chk-col-pink">
                            <label for="rememberme">Recordar</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">INGRESAR</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="/register">Regístrate</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="/reset-password">¿Olvidaste tu contraseña?</a>
                        </div>
                    </div>
                </form>
            </div>
@endsection

@section('js')

    <!-- Login Js -->
    <script src="{{ asset('js/pages/login/sign-in.js') }}"></script>  

@endsection