@extends('log_.log_')

@section('title', 'Reestablece tu contraseña')

@section('content')
            <div class="body">
                <form id="sign_up" method="POST" action="/new-password">
                    {{ csrf_field() }}

                    <input type="hidden" name="email" value="{{ Session::get('email') }}">
                    <input type="hidden" name="remember_token" value="{{ Session::get('remember_token') }}">

                    <div class="msg">
                        Ingresa tu nueva contraseña
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Ingresa tu nueva contraseña"  minlength="8" maxlength="200" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="confirm" placeholder="Ingresa tu nueva contraseña Nuevamente"  minlength="8" maxlength="200" required>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">REESTABLECER CONTRASEÑA</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="/login">Iniciar sesión</a>
                    </div>
                </form>
            </div>
@endsection

@section('js')

    <!-- Login Js -->
    <script src="{{ asset('js/pages/login/sign-up.js') }}"></script>  

@endsection