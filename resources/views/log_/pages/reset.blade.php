@extends('log_.log_')

@section('title', 'Olvidaste tu contraseña')

@section('content')
            <div class="body">
                <form id="forgot_password" method="POST" action="/reset-password">
                    {{ csrf_field() }}
                    <div class="msg">
                        Ingresa el correo electrónico con el que te registraste, te enviaremos un email con las intrucciones para reestablecer tu contraseña. 
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Correo electrónico" required autofocus>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">REESTABLECER CONTRASEÑA</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="/login">Iniciar sesión</a>
                    </div>
                </form>
            </div>
@endsection

@section('js')

    <!-- Login Js -->
    <script src="{{ asset('js/pages/exaloginmples/forgot-password.js') }}"></script>  


@endsection