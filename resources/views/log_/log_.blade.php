<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>@yield('title')</title>
        <!-- Favicon-->
        <link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet" />

        <!-- Waves Effect Css -->
        <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

        <!-- Bootstrap Select Css -->
        <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

        <!-- Sweetalert Css -->
        <link href="{{ asset('plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" />

        @yield('css')

    </head>

@if(Session::has('error'))
    <script>
        window.onload=function() {
            showErrorMessage('{{ session('error') }}', '{{ session('description') }}');
        }
    </script>
@endif

@if(Session::has('warning'))
    <script>
        window.onload=function() {
            showWarningMessage('{{ session('warning') }}', '{{ session('description') }}');
        }
    </script>
@endif 

@if(Session::has('success'))
    <script>
        window.onload=function() {
            showSuccessMessage('{{ session('success') }}', '{{ session('description') }}');
        }
    </script>
@endif

    <body class="signup-page">
        <div class="signup-box">
            <div class="logo">
                <a href="javascript:void(0);">Match-<b>PC</b></a>
                <small>La cómpra exacta para tus necesidades</small>
            </div>
            <div class="card">
                <!-- Content -->
                
                @yield('content')

                <!-- End Content -->
            </div>

        </div>
        <!-- Jquery Core Js -->
        <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

        <!-- Bootstrap Core Js -->
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

        <!-- SweetAlert Plugin Js -->
        <script src="{{ asset('plugins/sweetalert/sweetalert-dev.js') }}"></script>

        <!-- Select Plugin Js -->
        <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

        <!-- Custom Js -->
        <script src="{{ asset('js/admin.js') }}"></script>
        <script src="{{ asset('js/alerts.js') }}"></script>

        <!-- Validation Plugin Js -->
        <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>
      
        @yield('js')

    </body>
</html>