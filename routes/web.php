<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    /* Ruta prueba */
    Route::get('/asd', function () 
    {
        return view('email.register');
    });

    Route::get('/prueba', 'PruebaController@index');


/* Rutas Básicas del Sitio */

    /* Ruta Inicio Presentación */
    Route::get('/', function () 
    {
        return view('welcome/welcome');
    });

    /* Ruta Index */
    Route::get('/home', 'IndexController@index');

    /* Ruta de compra de membresía */
    Route::get('/premium', 'SubscriptionPurchase@index');
    Route::post('/premium', 'SubscriptionPurchase@purchase');
    Route::post('/transbank', 'SubscriptionPurchase@transbank');
    Route::post('/transbank_end', 'SubscriptionPurchase@transbank_end');
    Route::get('/transbank_end', 'SubscriptionPurchase@transbank_end');

    /* Ruta de nueva Coitización */
    Route::get('/new_quotations', 'QuotationController@index');
    Route::post('/new_quotations', 'QuotationController@new_quotation');
    Route::get('/show_quotations', 'QuotationController@show_new');


    /* Ruta Asesoría */
    Route::get('/messages', 'MessageController@index');
    Route::put('/messages', 'MessageController@send');
    Route::post('/messages', 'MessageController@refresh');

    /* #Rutas Básicas del Sitio */

/* Rutas de Autenticación */

    /* Rutas de login */
    Route::get('/login', 'LoginController@index');
    Route::post('/login', 'LoginController@login');

    /* Rutas de registro */
    Route::get('/register', 'RegisterController@index');
    Route::post('/register', 'RegisterController@register');
    Route::get('/activate', 'RegisterController@activate');


    /* Rutas de reseteo de contraseña */
        /* Página de solicitud de nueva contraseña */
        Route::get('/reset-password', 'ResetController@index');
        Route::post('/reset-password', 'ResetController@reset');
        /* Página de creación de nueva contraseña */
        Route::get('/new-password', 'ResetController@new_password');
        Route::post('/new-password', 'ResetController@new_password_restore');

    /* Ruta Logout */
    Route::get('/logout', function () 
    {
        Auth::logout();
        session()->regenerate();
        return redirect('/login');
    });

/* #Rutas de Autenticación */

/* Rutas Resource de CRUD y actualizar tabla de listado*/

    /* Ruta CRUD Productos */
    Route::resource('/products', 'ProductController');
    Route::get('/listProducts', 'ProductController@listProducts');

    /* Ruta CRUD Components */
    Route::resource('/components', 'ComponentController');
    Route::get('/listComponents', 'ComponentController@listComponents');

    /* Ruta CRUD Categorias */
    Route::resource('/categories', 'CategoryController');
    Route::get('/listCategories', 'CategoryController@listCategories');

    /* Ruta CRUD Tiendas */
    Route::resource('/stores', 'StoreController');
    Route::get('/listStores', 'StoreController@listStores');

    /* Ruta CRUD Benchmarks */
    Route::resource('/benchmarks', 'BenchmarkController');
    Route::get('/listBenchmarks', 'BenchmarkController@listBenchmarks');

    /* Ruta CRUD Usuarios */
    Route::resource('/users', 'UserController');
    Route::get('/listUsers', 'UserController@listUsers');

    /* Ruta CRUD Asesores */
    Route::resource('/advisers', 'AdviserController');
    Route::get('/listAdvisers', 'AdviserController@listAdvisers');

/* #Rutas Resource de CRUD y actualizar tabla de listado*/