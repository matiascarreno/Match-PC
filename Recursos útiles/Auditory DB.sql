drop database auditory;

/* Base de datos de Auditoría-PC */

create database auditory default character set utf8 default collate utf8_unicode_ci;

SET default_storage_engine=InnoDB;


/* Asignar un users */

use auditory;

create table subscriptions
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(100),
    new_value varchar(100),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_subscriptions (id)
);

create table users
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_users (id)
);

create table transactions
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_transactions (id)
);

create table chats
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_chats (id)
);

create table categories
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_categories (id)
);

create table products
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_products (id)
);

create table components
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_components (id)
);

create table component_types
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_component_types (id)
);
create table benchmarks
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_benchmarks (id)
);

create table components_benchmarks
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_components_benchmarks (id)
);

create table stores
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_stores (id)
);

create table products_stores
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_products_stores (id)
);

create table quotations
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_quotations (id)
);

create table quotations_products
(
    id int unsigned not null auto_increment,
    transaction char not null,
    primary_key int not null,
    field varchar(50) not null,
    old_value varchar(1000),
    new_value varchar(1000),
    date timestamp not null default current_timestamp,
    user varchar(128) not null,
    /* etc */
    constraint primary key pk_quotations_products (id)
);