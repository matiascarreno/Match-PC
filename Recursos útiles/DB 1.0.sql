drop database match_pc;

create database match_pc default character set utf8 default collate utf8_unicode_ci;

SET default_storage_engine=InnoDB;

/* Asignar un usuario */

use match_pc;

create table subscriptions
(
    id_subscription int(1) unsigned auto_increment,
    price int unsigned not null default 0,
    name varchar(100) not null default "",
    description text not null default "",
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_subscriptions (id_subscription)
)engine=InnoDB;

create table users
(
    id_user int unsigned auto_increment,
    email varchar(100) not null,
    password varchar(100) not null,
    remember_token varchar(100),
    name varchar(100),
    lastname varchar(100),
    picture varchar(100) default "avatar1.png",
    cover_image varchar(100) default "picture1.jpg",
    theme varchar(50) not null default "cyan",
    phone_number int unsigned,
    birthdate timestamp not null,
    gender char,
    subscription int(1) unsigned default 1,
    last_purchase timestamp null default null,
    privileges tinyint(1) unsigned default null,  /* null=user 0=adviser, 1=administrator */
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_users (id_user),
    constraint foreign key fk_users_subscriptions (subscription) references subscriptions(id_subscription) on delete cascade on update cascade
)engine=InnoDB;


create table transactions
(
    id_transaction int unsigned auto_increment,
    tokenws varchar(100) not null,
    buy_order int unsigned not null,
    id_user int unsigned not null,
    id_subscription int unsigned not null,
    date timestamp not null,
    approved tinyint(1) unsigned default 0, /* null= incompleto, 0=no aprobado, 1=aprobado */
    /* etc */
    constraint primary key pk_transaction (id_transaction),
    constraint foreign key fk_transaction_user (id_user) references users(id_user) on delete cascade on update cascade,
    constraint foreign key fk_transaction_subscription (id_subscription) references subscriptions(id_subscription) on delete cascade on update cascade
)engine=InnoDB;

create table chats
(
    id_chat int unsigned auto_increment,
    id_user int unsigned not null,
    id_adviser int unsigned,
    id_quotation int unsigned,
    last_read_message int unsigned not null default 0,
    start_date timestamp not null default current_timestamp,
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_chats (id_chat),
    constraint foreign key fk_chats_users (id_user) references users(id_user) on delete cascade on update cascade,
    constraint foreign key fk_chats_advisers (id_adviser) references users(id_user) on delete cascade on update cascade
)engine=InnoDB;

create table messages
(
    id_message int unsigned auto_increment, 
    id_chat int unsigned not null,
    response tinyint(1) unsigned not null default 0, /* 0=mensaje de usuario, 1=mensaje de asesor */
    date timestamp not null default current_timestamp,
    text text not null default "",
    archive varchar(100),
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_message (id_message),
    constraint foreign key fk_message_chats (id_chat) references chats(id_chat) on delete cascade on update cascade
)engine=InnoDB;

create table categories
(
    id_category int unsigned auto_increment,
    name varchar(100) not null default "" unique,
    description text not null default "",
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_category (id_category)
)engine=InnoDB;

create table components
(
    id_component int unsigned auto_increment,
    name varchar(200) not null default "" unique,
    description text not null default "",
    type int unsigned not null, /* 1=CPU, 2=GPU, 3=RAM, 4=SSD, 5=HDD */
    freq int unsigned, /* En MHz */
    cores int unsigned,
    memory decimal(6,1) unsigned,
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_components (id_component)

)engine=InnoDB;

create table products
(
    id_product int unsigned auto_increment,
    name varchar(200) not null default "" unique,
    description text not null default "",
    picture varchar(100) default "picture1.png",
    category int unsigned not null,
    cpu int unsigned not null,
    gpu int unsigned not null,
    ram int unsigned not null,
    ssd int unsigned,
    hdd int unsigned,
    screen_res varchar(20) not null,
    screen_size int unsigned not null,
    other text default "",
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_products (id_product),
    constraint foreign key fk_products_category (category) references categories(id_category) on delete cascade on update cascade,
    constraint foreign key fk_products_components_cpu (gpu) references components(id_component) on delete cascade on update cascade,
    constraint foreign key fk_products_components_gpu (cpu) references components(id_component) on delete cascade on update cascade,
    constraint foreign key fk_products_components_ram (ram) references components(id_component) on delete cascade on update cascade,
    constraint foreign key fk_products_components_ssd (ssd) references components(id_component) on delete cascade on update cascade,
    constraint foreign key fk_products_components_hdd (hdd) references components(id_component) on delete cascade on update cascade
)engine=InnoDB;

create table benchmarks
(
    id_benchmark int unsigned auto_increment,
    name varchar(100) not null default "" unique,
    description text not null default "",
    component_type int unsigned not null, /* 1=CPU, 2=GPU, 3=RAM, 4=SSD, 5=HDD */
    min_score int unsigned not null default 0,
    max_score int unsigned not null default 0,
    incremental tinyint(1) unsigned not null default 1, /* 0=decreciente, 1=creciente */
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_benchmarks (id_benchmark)
)engine=InnoDB;

create table components_benchmarks
(
    id_component_benchmark int unsigned auto_increment,
    id_component int unsigned not null,
    id_benchmark int unsigned not null,
    score int unsigned not null default 0,
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_components_benchmarks (id_component_benchmark),
    constraint unique key un_components_benchmarks (id_component, id_benchmark),
    constraint foreign key fk_components_benchmarks_products (id_component) references components(id_component) on delete cascade on update cascade,
    constraint foreign key fk_components_benchmarks_benchmarks (id_benchmark) references benchmarks(id_benchmark) on delete cascade on update cascade
)engine=InnoDB;

create table stores
(
    id_store int unsigned auto_increment,
    name varchar(100)not null unique,
    description text not null default "",
    website varchar(200) not null default "" unique,
    active tinyint(1) unsigned not null default 0, /* 0=incativo, 1=activo */
    /* etc */
    constraint primary key pk_stores (id_store)
)engine=InnoDB;

create table products_stores
(
    id_product_store int unsigned auto_increment,
    id_product int unsigned not null,
    id_store int unsigned not null,
    price int unsigned not null default 0,
    link varchar(500) not null default "",
    date timestamp not null default current_timestamp,
    active tinyint(1) unsigned not null default 0, /* 0=incativo, 1=activo */
    /* etc */
    constraint primary key pk_products_stores (id_product_store),
    constraint foreign key fk_products_stores_products (id_product) references products(id_product) on delete cascade on update cascade,
    constraint foreign key fk_products_stores_stores (id_store) references stores(id_store) on delete cascade on update cascade
)engine=InnoDB;

create table quotations
(
    id_quotation int unsigned auto_increment,
    id_user int unsigned not null,
    name varchar(200) not null default "",
    date timestamp not null default current_timestamp,
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_quotations (id_quotation),
    constraint foreign key fk_quotations_users (id_user) references users(id_user) on delete cascade on update cascade
)engine=InnoDB;

create table quotations_products
(
    id_quotation_product int unsigned auto_increment,
    id_product int unsigned not null,
    id_quotation int unsigned not null,
    active tinyint(1) unsigned not null default 0, /* 0=inactivo, 1=activo */
    /* etc */
    constraint primary key pk_quotations_products (id_quotation_product),
    constraint foreign key fk_quotations_products_products (id_product) references products(id_product) on delete cascade on update cascade,
    constraint foreign key fk_quotations_products_quotations (id_quotation) references quotations(id_quotation) on delete cascade on update cascade
)engine=InnoDB;

/* Triggers de auditoría */

    /* Category *********************************************************************************** */

        /* Insert Category */
        DELIMITER $$
        CREATE TRIGGER categories_insert AFTER INSERT ON categories FOR EACH ROW BEGIN

            insert into auditory.categories values 
            (null, "I", new.id_category, "name", null, new.name, current_timestamp, current_user),
            (null, "I", new.id_category, "description", null, new.description, current_timestamp, current_user),
            (null, "I", new.id_category, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Category */
        DELIMITER $$
        CREATE TRIGGER categories_update AFTER UPDATE ON categories FOR EACH ROW BEGIN 

            IF old.active != new.active && NEW.active = 0 THEN
                update products set active=0 where category=OLD.id_category;
            END IF;

            IF old.name != new.name THEN
                insert into auditory.categories values 
                (null, "U", old.id_category, "name", old.name, new.name, current_timestamp, current_user);
            END IF;

            IF old.description != new.description THEN
                insert into auditory.categories values 
                (null, "U", old.id_category, "description", old.description, new.description, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.categories values 
                (null, "U", old.id_category, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Category */
        DELIMITER $$
        CREATE TRIGGER categories_delete AFTER DELETE ON categories FOR EACH ROW BEGIN

            insert into auditory.categories values 
            (null, "D", old.id_category, "delete", old.id_category, old.id_category, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* Subscription *********************************************************************************** */

        /* Insert Subscription */
        DELIMITER $$
        CREATE TRIGGER subscriptions_insert AFTER INSERT ON subscriptions FOR EACH ROW BEGIN

            insert into auditory.subscriptions values 
            (null, "I", new.id_subscription, "price", null, new.price, current_timestamp, current_user),
            (null, "I", new.id_subscription, "name", null, new.name, current_timestamp, current_user),
            (null, "I", new.id_subscription, "description", null, new.description, current_timestamp, current_user),
            (null, "I", new.id_subscription, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Subscription */
        DELIMITER $$
        CREATE TRIGGER subscriptions_update AFTER UPDATE ON subscriptions FOR EACH ROW BEGIN 

            IF old.price != new.price THEN
                insert into auditory.subscriptions values 
                (null, "U", old.id_subscription, "price", old.price, new.price, current_timestamp, current_user);
            END IF;

            IF old.name != new.name THEN
                insert into auditory.subscriptions values 
                (null, "U", old.id_subscription, "name", old.name, new.name, current_timestamp, current_user);
            END IF;

            IF old.description != new.description THEN
                insert into auditory.subscriptions values 
                (null, "U", old.id_subscription, "description", old.description, new.description, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.subscriptions values 
                (null, "U", old.id_subscription, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Subscription */
        DELIMITER $$
        CREATE TRIGGER subscriptions_delete AFTER DELETE ON subscriptions FOR EACH ROW BEGIN

            insert into auditory.subscriptions values 
            (null, "D", old.id_subscription, "delete", old.id_subscription, old.id_subscription, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* User *********************************************************************************** */

        /* Insert User */
        DELIMITER $$
        CREATE TRIGGER users_insert AFTER INSERT ON users FOR EACH ROW BEGIN

            insert into auditory.users values 
            (null, "I", new.id_user, "email", null, new.email, current_timestamp, current_user),
            (null, "I", new.id_user, "password", null, new.password, current_timestamp, current_user),
            (null, "I", new.id_user, "name", null, new.name, current_timestamp, current_user),
            (null, "I", new.id_user, "lastname", null, new.lastname, current_timestamp, current_user),
            (null, "I", new.picture, "picture", null, new.picture, current_timestamp, current_user),
            (null, "I", new.cover_image, "cover_image", null, new.cover_image, current_timestamp, current_user),
            (null, "I", new.theme, "theme", null, new.theme, current_timestamp, current_user),
            (null, "I", new.id_user, "phone_number", null, new.phone_number, current_timestamp, current_user),
            (null, "I", new.id_user, "birthdate", null, new.birthdate, current_timestamp, current_user),
            (null, "I", new.id_user, "gender", null, new.gender, current_timestamp, current_user),
            (null, "I", new.id_user, "subscription", null, new.subscription, current_timestamp, current_user),
            (null, "I", new.last_purchase, "last_purchase", null, new.last_purchase, current_timestamp, current_user),
            (null, "I", new.id_user, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update User */
        DELIMITER $$
        CREATE TRIGGER users_update AFTER UPDATE ON users FOR EACH ROW BEGIN 

            IF old.email != new.email THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "email", old.email, new.email, current_timestamp, current_user);
            END IF;

            IF old.password != new.password THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "password", old.password, new.password, current_timestamp, current_user);
            END IF;

            IF old.name != new.name THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "name", old.name, new.name, current_timestamp, current_user);
            END IF;

            IF old.lastname != new.lastname THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "lastname", old.lastname, new.lastname, current_timestamp, current_user);
            END IF;

            IF old.picture != new.picture THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "picture", old.picture, new.picture, current_timestamp, current_user);
            END IF;

            IF old.cover_image != new.cover_image THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "cover_image", old.cover_image, new.cover_image, current_timestamp, current_user);
            END IF;

            IF old.theme != new.theme THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "theme", old.theme, new.theme, current_timestamp, current_user);
            END IF;

            IF old.phone_number != new.phone_number THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "phone_number", old.phone_number, new.phone_number, current_timestamp, current_user);
            END IF;

            IF old.birthdate != new.birthdate THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "birthdate", old.birthdate, new.birthdate, current_timestamp, current_user);
            END IF;

            IF old.gender != new.gender THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "gender", old.gender, new.gender, current_timestamp, current_user);
            END IF;

            IF old.subscription != new.subscription THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "subscription", old.subscription, new.subscription, current_timestamp, current_user);
            END IF;

            IF old.last_purchase != new.last_purchase THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "last_purchase", old.last_purchase, new.last_purchase, current_timestamp, current_user);
            END IF;

            IF old.privileges != new.privileges THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "privileges", old.privileges, new.privileges, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.users values 
                (null, "U", old.id_user, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete User */
        DELIMITER $$
        CREATE TRIGGER users_delete AFTER DELETE ON users FOR EACH ROW BEGIN

            insert into auditory.users values 
            (null, "D", old.id_user, null, null, null, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* Chat *********************************************************************************** */

        /* Insert Chat */
        DELIMITER $$
        CREATE TRIGGER chats_insert AFTER INSERT ON chats FOR EACH ROW BEGIN

            insert into auditory.chats values 
            (null, "I", new.id_chat, "id_user", null, new.id_user, current_timestamp, current_user),
            (null, "I", new.id_adviser, "id_adviser", null, new.id_adviser, current_timestamp, current_user),
            (null, "I", new.id_quotation, "id_quotation", null, new.id_quotation, current_timestamp, current_user),
            (null, "I", new.last_read_message, "last_read_message", null, new.id_adviser, current_timestamp, current_user),
            (null, "I", new.id_chat, "start_date", null, new.start_date, current_timestamp, current_user),
            (null, "I", new.id_chat, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Chat */
        DELIMITER $$
        CREATE TRIGGER chats_update AFTER UPDATE ON chats FOR EACH ROW BEGIN 

            IF old.id_user != new.id_user THEN
                insert into auditory.chats values 
                (null, "U", old.id_chat, "id_user", old.id_user, new.id_user, current_timestamp, current_user);
            END IF;

            IF old.id_adviser != new.id_adviser THEN
                insert into auditory.chats values 
                (null, "U", old.id_chat, "id_adviser", old.id_adviser, new.id_adviser, current_timestamp, current_user);
            END IF;

            IF old.id_quotation != new.id_quotation THEN
                insert into auditory.chats values 
                (null, "U", old.id_chat, "id_quotation", old.id_quotation, new.id_quotation, current_timestamp, current_user);
            END IF;

            IF old.last_read_message != new.last_read_message THEN
                insert into auditory.chats values 
                (null, "U", old.id_chat, "last_read_message", old.last_read_message, new.last_read_message, current_timestamp, current_user);
            END IF;

            IF old.start_date != new.start_date THEN
                insert into auditory.chats values 
                (null, "U", old.id_chat, "start_date", old.start_date, new.start_date, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.chats values 
                (null, "U", old.id_chat, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Chat */
        DELIMITER $$
        CREATE TRIGGER chats_delete AFTER DELETE ON chats FOR EACH ROW BEGIN

            insert into auditory.chats values 
            (null, "D", old.id_chat, "delete", old.id_chat, old.id_chat, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* Product *********************************************************************************** */

        /* Insert Product */
        DELIMITER $$
        CREATE TRIGGER products_insert AFTER INSERT ON products FOR EACH ROW BEGIN

            insert into auditory.products values 
            (null, "I", new.id_product, "name", null, new.name, current_timestamp, current_user),
            (null, "I", new.id_product, "description", null, new.description, current_timestamp, current_user),
            (null, "I", new.id_product, "category", null, new.category, current_timestamp, current_user),
            (null, "I", new.id_product, "cpu", null, new.cpu, current_timestamp, current_user),
            (null, "I", new.id_product, "gpu", null, new.gpu, current_timestamp, current_user),
            (null, "I", new.id_product, "ram", null, new.ram, current_timestamp, current_user),
            (null, "I", new.id_product, "ssd", null, new.ssd, current_timestamp, current_user),
            (null, "I", new.id_product, "hdd", null, new.hdd, current_timestamp, current_user),
            (null, "I", new.id_product, "screen_res", null, new.screen_res, current_timestamp, current_user),
            (null, "I", new.id_product, "screen_size", null, new.screen_size, current_timestamp, current_user),
            (null, "I", new.id_product, "other", null, new.other, current_timestamp, current_user),
            (null, "I", new.id_product, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Product */
        DELIMITER $$
        CREATE TRIGGER products_update AFTER UPDATE ON products FOR EACH ROW BEGIN 

            IF old.name != new.name THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "name", old.name, new.name, current_timestamp, current_user);
            END IF;

            IF old.description != new.description THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "description", old.description, new.description, current_timestamp, current_user);
            END IF;

            IF old.category != new.category THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "category", old.category, new.category, current_timestamp, current_user);
            END IF;

            IF old.cpu != new.cpu THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "cpu", old.cpu, new.cpu, current_timestamp, current_user);
            END IF;

            IF old.gpu != new.gpu THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "gpu", old.gpu, new.gpu, current_timestamp, current_user);
            END IF;

            IF old.ram != new.ram THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "ram", old.ram, new.ram, current_timestamp, current_user);
            END IF;

            IF old.ssd != new.ssd THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "ssd", old.ssd, new.ssd, current_timestamp, current_user);
            END IF;

            IF old.hdd != new.hdd THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "hdd", old.hdd, new.hdd, current_timestamp, current_user);
            END IF;

            IF old.screen_res != new.screen_res THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "screen_res", old.screen_res, new.screen_res, current_timestamp, current_user);
            END IF;

            IF old.screen_size != new.screen_size THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "screen_size", old.screen_size, new.screen_size, current_timestamp, current_user);
            END IF;

            IF old.other != new.other THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "other", old.other, new.other, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.products values 
                (null, "U", old.id_product, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Product */
        DELIMITER $$
        CREATE TRIGGER products_delete AFTER DELETE ON products FOR EACH ROW BEGIN

            insert into auditory.products values 
            (null, "D", old.id_product, "delete", old.id_product, old.id_product, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* Benchmark *********************************************************************************** */

        /* Insert Category */
        DELIMITER $$
        CREATE TRIGGER benchmarks_insert AFTER INSERT ON benchmarks FOR EACH ROW BEGIN

            insert into auditory.benchmarks values 
            (null, "I", new.id_benchmark, "name", null, new.name, current_timestamp, current_user),
            (null, "I", new.id_benchmark, "description", null, new.description, current_timestamp, current_user),
            (null, "I", new.id_benchmark, "component_type", null, new.component_type, current_timestamp, current_user),
            (null, "I", new.id_benchmark, "min_score", null, new.min_score, current_timestamp, current_user),
            (null, "I", new.id_benchmark, "max_score", null, new.max_score, current_timestamp, current_user),
            (null, "I", new.id_benchmark, "incremental", null, new.incremental, current_timestamp, current_user),
            (null, "I", new.id_benchmark, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Category */
        DELIMITER $$
        CREATE TRIGGER benchmarks_update AFTER UPDATE ON benchmarks FOR EACH ROW BEGIN 

            IF old.name != new.name THEN
                insert into auditory.benchmarks values 
                (null, "U", old.id_benchmark, "name", old.name, new.name, current_timestamp, current_user);
            END IF;

            IF old.description != new.description THEN
                insert into auditory.benchmarks values 
                (null, "U", old.id_benchmark, "description", old.description, new.description, current_timestamp, current_user);
            END IF;

            IF old.component_type != new.component_type THEN
                insert into auditory.benchmarks values 
                (null, "U", old.id_benchmark, "component_type", old.component_type, new.component_type, current_timestamp, current_user);
            END IF;

            IF old.min_score != new.min_score THEN
                insert into auditory.benchmarks values 
                (null, "U", old.id_benchmark, "min_score", old.min_score, new.min_score, current_timestamp, current_user);
            END IF;

            IF old.max_score != new.max_score THEN
                insert into auditory.benchmarks values 
                (null, "U", old.id_benchmark, "max_score", old.max_score, new.max_score, current_timestamp, current_user);
            END IF;

            IF old.incremental != new.incremental THEN
                insert into auditory.benchmarks values 
                (null, "U", old.id_benchmark, "incremental", old.incremental, new.incremental, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.benchmarks values 
                (null, "U", old.id_benchmark, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Category */
        DELIMITER $$
        CREATE TRIGGER benchmarks_delete AFTER DELETE ON benchmarks FOR EACH ROW BEGIN

            insert into auditory.benchmarks values 
            (null, "D", old.id_benchmark, "delete", old.id_benchmark, old.id_benchmark, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* Component_Benchmark *********************************************************************************** */

        /* Insert Component_benchmark */
        DELIMITER $$
        CREATE TRIGGER components_benchmarks_insert AFTER INSERT ON components_benchmarks FOR EACH ROW BEGIN

            insert into auditory.components_benchmarks values 
            (null, "I", new.id_component_benchmark, "id_component", null, new.id_component, current_timestamp, current_user),
            (null, "I", new.id_component_benchmark, "id_benchmark", null, new.id_benchmark, current_timestamp, current_user),
            (null, "I", new.id_component_benchmark, "score", null, new.score, current_timestamp, current_user),
            (null, "I", new.id_component_benchmark, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Component_benchmark */
        DELIMITER $$
        CREATE TRIGGER components_benchmarks_update AFTER UPDATE ON components_benchmarks FOR EACH ROW BEGIN 

            IF old.id_component != new.id_component THEN
                insert into auditory.components_benchmarks values 
                (null, "U", old.id_component_benchmark, "id_component", old.id_component, new.id_component, current_timestamp, current_user);
            END IF;

            IF old.id_benchmark != new.id_benchmark THEN
                insert into auditory.components_benchmarks values 
                (null, "U", old.id_component_benchmark, "id_benchmark", old.id_benchmark, new.id_benchmark, current_timestamp, current_user);
            END IF;

            IF old.score != new.score THEN
                insert into auditory.components_benchmarks values 
                (null, "U", old.id_component_benchmark, "score", old.score, new.score, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.components_benchmarks values 
                (null, "U", old.id_component_benchmark, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Component_benchmark */
        DELIMITER $$
        CREATE TRIGGER components_benchmarks_delete AFTER DELETE ON components_benchmarks FOR EACH ROW BEGIN

            insert into auditory.components_benchmarks values 
            (null, "D", old.id_component_benchmark, "delete", old.id_component_benchmark, null, current_timestamp, current_user);

        END$$
        DELIMITER ;

    /* Store *********************************************************************************** */

        /* Insert Store */
        DELIMITER $$
        CREATE TRIGGER stores_insert AFTER INSERT ON stores FOR EACH ROW BEGIN

            insert into auditory.stores values 
            (null, "I", new.id_store, "name", null, new.name, current_timestamp, current_user),
            (null, "I", new.id_store, "description", null, new.description, current_timestamp, current_user),
            (null, "I", new.id_store, "website", null, new.website, current_timestamp, current_user),
            (null, "I", new.id_store, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Store */
        DELIMITER $$
        CREATE TRIGGER stores_update AFTER UPDATE ON stores FOR EACH ROW BEGIN 

            IF old.name != new.name THEN
                insert into auditory.stores values 
                (null, "U", old.id_store, "name", old.name, new.name, current_timestamp, current_user);
            END IF;

            IF old.description != new.description THEN
                insert into auditory.stores values 
                (null, "U", old.id_store, "description", old.description, new.description, current_timestamp, current_user);
            END IF;

            IF old.website != new.website THEN
                insert into auditory.stores values 
                (null, "U", old.id_store, "website", old.website, new.website, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.stores values 
                (null, "U", old.id_store, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Store */
        DELIMITER $$
        CREATE TRIGGER stores_delete AFTER DELETE ON stores FOR EACH ROW BEGIN

            insert into auditory.stores values 
            (null, "D", old.id_store, "delete", old.id_store, old.id_store, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* Product_Store *********************************************************************************** */

        /* Insert Product_Store */
        DELIMITER $$
        CREATE TRIGGER products_stores_insert AFTER INSERT ON products_stores FOR EACH ROW BEGIN

            insert into auditory.products_stores values 
            (null, "I", new.id_product_store, "id_product", null, new.id_product, current_timestamp, current_user),
            (null, "I", new.id_product_store, "id_store", null, new.id_store, current_timestamp, current_user),
            (null, "I", new.id_product_store, "price", null, new.price, current_timestamp, current_user),
            (null, "I", new.id_product_store, "link", null, new.link, current_timestamp, current_user),
            (null, "I", new.id_product_store, "date", null, new.date, current_timestamp, current_user),
            (null, "I", new.id_product_store, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Product_Store */
        DELIMITER $$
        CREATE TRIGGER products_stores_update AFTER UPDATE ON products_stores FOR EACH ROW BEGIN 

            IF old.id_product != new.id_product THEN
                insert into auditory.products_stores values 
                (null, "U", old.id_product_store, "id_product", old.id_product, new.id_product, current_timestamp, current_user);
            END IF;

            IF old.id_store != new.id_store THEN
                insert into auditory.products_stores values 
                (null, "U", old.id_product_store, "id_store", old.id_store, new.id_store, current_timestamp, current_user);
            END IF;

            IF old.price != new.price THEN
                insert into auditory.products_stores values 
                (null, "U", old.id_product_store, "price", old.price, new.price, current_timestamp, current_user);
            END IF;

            IF old.link != new.link THEN
                insert into auditory.products_stores values 
                (null, "U", old.id_product_store, "link", old.link, new.link, current_timestamp, current_user);
            END IF;

            IF old.date != new.date THEN
                insert into auditory.products_stores values 
                (null, "U", old.id_product_store, "date", old.date, new.date, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.products_stores values 
                (null, "U", old.id_product_store, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Product_Store */
        DELIMITER $$
        CREATE TRIGGER products_stores_delete AFTER DELETE ON products_stores FOR EACH ROW BEGIN

            insert into auditory.products_stores values 
            (null, "D", old.id_product_store, "delete", old.id_product_store, old.id_product_store, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* Quotation *********************************************************************************** */

        /* Insert Quotation */
        DELIMITER $$
        CREATE TRIGGER quotations_insert AFTER INSERT ON quotations FOR EACH ROW BEGIN

            insert into auditory.quotations values 
            (null, "I", new.id_quotation, "id_user", null, new.id_user, current_timestamp, current_user),
            (null, "I", new.id_quotation, "name", null, new.name, current_timestamp, current_user),
            (null, "I", new.id_quotation, "date", null, new.date, current_timestamp, current_user),
            (null, "I", new.id_quotation, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Quotation */
        DELIMITER $$
        CREATE TRIGGER quotations_update AFTER UPDATE ON quotations FOR EACH ROW BEGIN 

            IF old.id_user != new.id_user THEN
                insert into auditory.quotations values 
                (null, "U", old.id_quotation, "id_user", old.id_user, new.id_user, current_timestamp, current_user);
            END IF;

            IF old.name != new.name THEN
                insert into auditory.quotations values 
                (null, "U", old.id_quotation, "name", old.name, new.name, current_timestamp, current_user);
            END IF;

            IF old.date != new.date THEN
                insert into auditory.quotations values 
                (null, "U", old.id_quotation, "date", old.date, new.date, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.quotations values 
                (null, "U", old.id_quotation, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Quotation */
        DELIMITER $$
        CREATE TRIGGER quotations_delete AFTER DELETE ON quotations FOR EACH ROW BEGIN

            insert into auditory.quotations values 
            (null, "D", old.id_quotation, "delete", old.id_quotation, old.id_quotation, current_timestamp, current_user);
            
        END$$
        DELIMITER ;

    /* Quotations_Product *********************************************************************************** */

        /* Insert Quotations_Product */
        DELIMITER $$
        CREATE TRIGGER quotations_products_insert AFTER INSERT ON quotations_products FOR EACH ROW BEGIN

            insert into auditory.quotations_products values 
            (null, "I", new.id_quotation_product, "id_product", null, new.id_product, current_timestamp, current_user),
            (null, "I", new.id_quotation_product, "id_quotation", null, new.id_quotation, current_timestamp, current_user),
            (null, "I", new.id_quotation_product, "active", null, new.active, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Quotations_Product */
        DELIMITER $$
        CREATE TRIGGER quotations_products_update AFTER UPDATE ON quotations_products FOR EACH ROW BEGIN 

            IF old.id_product != new.id_product THEN
                insert into auditory.quotations_products values 
                (null, "U", old.id_quotation_product, "id_product", old.id_product, new.id_product, current_timestamp, current_user);
            END IF;

            IF old.id_quotation != new.id_quotation THEN
                insert into auditory.quotations_products values 
                (null, "U", old.id_quotation_product, "id_quotation", old.id_quotation, new.id_quotation, current_timestamp, current_user);
            END IF;

            IF old.active != new.active THEN
                insert into auditory.quotations_products values 
                (null, "U", old.id_quotation_product, "active", old.active, new.active, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Quotations_Product */
        DELIMITER $$
        CREATE TRIGGER quotations_products_delete AFTER DELETE ON quotations_products FOR EACH ROW BEGIN

            insert into auditory.quotations_products values 
            (null, "D", old.id_quotation_product, "delete", old.id_quotation_product, old.id_quotation_product, current_timestamp, current_user);
            
        END$$
        DELIMITER ;
    /* Transactions *********************************************************************************** */

        /* Insert Transactions */
        DELIMITER $$
        CREATE TRIGGER transactions_insert AFTER INSERT ON transactions FOR EACH ROW BEGIN

            insert into auditory.transactions values 
            (null, "I", new.id_transaction, "id_transaction", null, new.id_transaction, current_timestamp, current_user),
            (null, "I", new.id_transaction, "tokenws", null, new.tokenws, current_timestamp, current_user),
            (null, "I", new.id_transaction, "buy_order", null, new.buy_order, current_timestamp, current_user),
            (null, "I", new.id_transaction, "id_user", null, new.id_user, current_timestamp, current_user),
            (null, "I", new.id_transaction, "id_subscription", null, new.id_subscription, current_timestamp, current_user),
            (null, "I", new.id_transaction, "date", null, new.date, current_timestamp, current_user),
            (null, "I", new.id_transaction, "approved", null, new.approved, current_timestamp, current_user);

        END$$
        DELIMITER ;
        
        /* Update Transactions */
        DELIMITER $$
        CREATE TRIGGER transactions_update AFTER UPDATE ON transactions FOR EACH ROW BEGIN 

            IF old.tokenws != new.tokenws THEN
                insert into auditory.transactions values 
                (null, "U", old.id_transaction, "tokenws", old.tokenws, new.tokenws, current_timestamp, current_user);
            END IF;

            IF old.buy_order != new.buy_order THEN
                insert into auditory.transactions values 
                (null, "U", old.id_transaction, "buy_order", old.buy_order, new.buy_order, current_timestamp, current_user);
            END IF;

            IF old.id_user != new.id_user THEN
                insert into auditory.transactions values 
                (null, "U", old.id_transaction, "id_user", old.id_user, new.id_user, current_timestamp, current_user);
            END IF;

            IF old.id_subscription != new.id_subscription THEN
                insert into auditory.transactions values 
                (null, "U", old.id_transaction, "id_subscription", old.id_subscription, new.id_subscription, current_timestamp, current_user);
            END IF;

            IF old.date != new.date THEN
                insert into auditory.transactions values 
                (null, "U", old.id_transaction, "date", old.date, new.date, current_timestamp, current_user);
            END IF;

            IF old.approved != new.approved THEN
                insert into auditory.transactions values 
                (null, "U", old.id_transaction, "approved", old.approved, new.approved, current_timestamp, current_user);
            END IF;
            
        END$$
        DELIMITER ;

        /* Delete Transactions */
        DELIMITER $$
        CREATE TRIGGER transactions_delete AFTER DELETE ON transactions FOR EACH ROW BEGIN

            insert into auditory.quotations_products values 
            (null, "D", old.id_transaction, "delete", old.id_transaction, old.id_transaction, current_timestamp, current_user);
            
        END$$
        DELIMITER ;
/* Triggers de auditoría */

/* Insert de prueba */
    insert into subscriptions values
        (null, 0, "Gratuita", "Membresía Gratuita", 1),
        (null, 1100, "Lite", "Membresía Lite", 1),
        (null, 3000, "Premium", "Membresía Premium", 1);


    insert into users values 
        (null, "users1@mail.cl", "$2y$10$nv0nek5vPWC9GuTEOApU8e88OZGYnUws8AySKYHLl74QBQkU81f9u", null, "name1", "lastname1", "avatar44.png", "picture15.jpg", "blue", 1111111, current_timestamp, 'F', 1, null, null, 1),
        (null, "users2@mail.cl", "$2y$10$QIi7jxxmnQJs8nc4FGFT0ufYXh.TdHtbd36C8ekHeukF8K9HSsll6", null, "name2", "lastname2", "avatar45.png", "picture16.jpg", "green", 2222222, current_timestamp, 'M', 2, null, null, 1),
        (null, "users3@mail.cl", "$2y$10$7gzFxNXiUQcOW5u0OtIVOuuaqmeSTEbo3GnmcqbDu6Ei1a/I2noOy", null, "name3", "lastname3", "avatar46.png", "picture17.jpg", "cyan", 3333333, current_timestamp, null, 3, null, null, 1),

        (null, "advisers1@mail.cl", "$2y$10$o2NwrPtyD9Pry9M9zfANOe3kdwpAnb8UhVuiiFPqzkFyTrZzyBUNO", null, "name1", "lastname1", "avatar47.png", "picture18.jpg", "yellow", 4444444, current_timestamp, 'M', null, null, 0, 1),
        (null, "advisers2@mail.cl", "$2y$10$LgAUQN389003ydBLUeMcGO3f4l31SP3F2cZzSWeq3zZV/QcVXPQtm", null, "name2", "lastname2", "avatar48.png", "picture19.jpg", "red", 5555555, current_timestamp, 'F', null, null, 0, 1),
        (null, "advisers3@mail.cl", "$2y$10$bMe857bXC1wsdA/.vDEfceebtmB63nDkl0srDA9lo3iDutBN/xpny", null, "name3", "lastname3", "avatar49.png", "picture20.jpg", "gey", 6666666, current_timestamp, null, null, null, 0, 1), 

        (null, "administrators1@mail.cl", "$2y$10$DsS08P4DQ4UnBPWe9WzvaeLdKn9tyDCC1DFBf43RJDM59q1mTnNP2", null, "name1", "lastname1", "avatar50.png", "picture21.jpg", "teal", 7777777, current_timestamp, 'M', null,null, 1, 1),
        (null, "administrators2@mail.cl", "$2y$10$qV7XXAagpf75saGdFWZGzuaJYjLvmb9Eew/Dx1Joq7IgBTTAD22XK", null, "name2", "lastname2", "avatar51.png", "picture22.jpg", "black", 8888888, current_timestamp, 'F', null, null, 1, 1),
        (null, "administrators3@mail.cl", "$2y$10$z6oq7JpkgHqJnPLK7NsfPuzBYGg.o81cCooBbyFFAAAXPPCa24Fmy", null, "name3", "lastname3", "avatar52.png", "picture23.jpg", "orange", 9999999, current_timestamp, null, null, null, 1, 1);


    insert into categories values 
        (null, 'Por Definir', 'aún sin categoría', 1),
        (null, 'ulv', 'Ultralivianos', 1),
        (null, 'emp', 'Empresariales', 1),
        (null, 'gmr', 'Gamer', 1),
        (null, '2e1', 'Convertibles (2 en 1)', 1),
        (null, 'mac', 'MacBook', 1),
        (null, 'arm', 'Armados', 1),
        (null, 'aio', 'All in One', 1),
        (null, 'imac', 'iMac', 1);

    insert into chats values 
        (null, 1, 4, null, 0, current_timestamp, 1),
        (null, 2, 5, null, 0, current_timestamp, 1),
        (null, 3, 6, null, 0, current_timestamp, 1);

    insert into messages values
        (null, 1, 0, current_timestamp, 'Primer mensaje', null, 1),
        (null, 1, 1, current_timestamp, 'Segundo mensaje', null, 1),
        (null, 1, 0, current_timestamp, 'Tercer mensaje', null, 1),
        (null, 1, 1, current_timestamp, 'Cuarto mensaje', null, 1),
        (null, 1, 0, current_timestamp, 'Quinto mensaje', null, 1),
        (null, 1, 1, current_timestamp, 'Sexto mensaje', null, 1),

        (null, 2, 0, current_timestamp, 'Primer mensaje', null, 1),
        (null, 2, 1, current_timestamp, 'Segundo mensaje', null, 1),
        (null, 2, 0, current_timestamp, 'Tercer mensaje', null, 1),
        (null, 2, 1, current_timestamp, 'Cuarto mensaje', null, 1),
        (null, 2, 0, current_timestamp, 'Quinto mensaje', null, 1),
        (null, 2, 1, current_timestamp, 'Sexto mensaje', null, 1),

        (null, 3, 0, current_timestamp, 'Primer mensaje', null, 1),
        (null, 3, 1, current_timestamp, 'Segundo mensaje', null, 1),
        (null, 3, 0, current_timestamp, 'Tercer mensaje', null, 1),
        (null, 3, 1, current_timestamp, 'Cuarto mensaje', null, 1),
        (null, 3, 0, current_timestamp, 'Quinto mensaje', null, 1),
        (null, 3, 1, current_timestamp, 'Sexto mensaje', null, 1);

    insert into stores values 
        (null, 'PCFactory', 'PCFactory', 'http://www.pcfactory.cl', 1),
        (null, 'SPDigital', 'SPDigital', 'https://www.spdigital.cl	', 1),
        (null, 'Winpy', 'Winpy', 'https://www.winpy.cl', 1);

    insert into components values
        (null, "Intel i3 8100k", "Intel core i3 8100k", 1, 3600, 4, null, 1),
        (null, "Intel i5 8400k", "Intel core i5 8400k", 1, 4000, 6, null, 1),
        (null, "Intel i7 8700k", "Intel core i7 8700k", 1, 4700, 6, null, 1),
        (null, "Ryzen 5 2600x", "AMD Ryzen 5 2600x", 1, 3000, 6, null, 1),
        (null, "Ryzen 7 2700x", "AMD Ryzen 7 2700x", 1, 3000, 8, null, 1),

        (null, "GTX 1050", "NVidia GTX 1050", 2, 1354, 640, 2, 1),
        (null, "GTX 1060", "NVidia GTX 1060", 2, 1506, 1280, 6, 1),
        (null, "GTX 1070", "NVidia GTX 1070", 2, 1506, 1920, 8, 1),
        (null, "GTX 1080", "NVidia GTX 1080", 2, 1607, 2560, 8, 1),
        (null, "GTX 1080TI", "NVidia GTX 1080TI", 2, 1480, 3584, 11, 1),

        (null, "2GB", "description", 3, 2400, null, 2, 1),
        (null, "4GB", "description", 3, 2666, null, 4, 1),
        (null, "8GB", "description", 3, 2933, null, 8, 1),
        (null, "16GB", "description", 3, 3200, null, 16, 1),
        (null, "32GB", "description", 3, 3466, null, 32, 1),

        (null, "SSD 120GB", "SSD 120GB", 4, null, null, 120, 1),
        (null, "SSD 240GB", "SSD 240GB", 4, null, null, 240, 1),
        (null, "SSD 520GB", "SSD 520GB", 4, null, null, 520, 1),
        (null, "SSD 1TB", "SSD 1TB", 4, null, null, 1024, 1),
        (null, "SSD 2TB", "SSD 2TB", 4, null, null, 2048, 1),

        (null, "HDD 500GB", "HDD 500GB", 5, null, null, 500, 1),
        (null, "HDD 640GB", "HDD 640GB", 5, null, null, 640, 1),
        (null, "HDD 1TB", "HDD 1TB", 5, null, null, 1024, 1),
        (null, "HDD 2TB", "HDD 2TB", 5, null, null, 2048, 1),
        (null, "HDD 4TB", "HDD 4TB", 5, null, null, 4096, 1);

    insert into products values 
        (null, 'product1', 'description1', 'picture1', 1, 1, 6, 11, 16, null, "HD", 15, 1, 1),
        (null, 'product2', 'description2', 'picture1', 2, 1, 6, 11, 16, null, "HD", 15, 1, 1),
        (null, 'product3', 'description3', 'picture1', 3, 2, 7, 12, 17, 21, "HD", 13, 0, 1),
        (null, 'product4', 'description4', 'picture1', 4, 2, 7, 12, 17, 21, "FHD", 17, 1, 1),
        (null, 'product5', 'description5', 'picture1', 5, 3, 8, 13, 18, 23, "FHD", 15, 0, 1),
        (null, 'product6', 'description6', 'picture1', 6, 3, 8, 13, 18, 23, "FHD", 15, 1, 1),
        (null, 'product7', 'description7', 'picture1', 7, 4, 9, 14, null, 21, "QHD", 17, 1, 1),
        (null, 'product8', 'description8', 'picture1', 8, 4, 9, 14, null, 21, "QHD", 15, 0, 1),
        (null, 'product9', 'description9', 'picture1', 9, 5, 10, 15, null, 22, "4K", 11, 1, 1),
        (null, 'product10', 'description10', 'picture1', 2, 5, 10, 15, null, 22, "4K", 15, 1, 1);

    insert into benchmarks values
        (null, 'benchmark_cpu', 'descripcion1', 1, 0, 10000, 1, 1),
        (null, 'benchmark_gpu', 'descripcion2', 2, 0, 1000, 1, 1),
        (null, 'benchmark_ram', 'descripcion3', 3, 100, 1000, 1, 1),
        (null, 'benchmark_ssd', 'descripcion3', 4, 100, 1000, 1, 1),
        (null, 'benchmark_hdd', 'descripcion3', 5, 100, 1000, 1, 1);

    

    insert into quotations values
        (null, 1, "Cotización1", current_timestamp, 1),
        (null, 1, "Cotización2", current_timestamp, 1),
        (null, 2, "Cotización3", current_timestamp, 1),
        (null, 2, "Cotización4", current_timestamp, 1),
        (null, 3, "Cotización5", current_timestamp, 1),
        (null, 3, "Cotización6", current_timestamp, 1);


    insert into components_benchmarks values
        (null, 1, 1, 111, 1),
        (null, 2, 1, 222, 1),
        (null, 3, 1, 333, 1),
        (null, 4, 1, 444, 1),
        (null, 5, 1, 555, 1),

        (null, 6, 2, 111, 1),
        (null, 7, 2, 222, 1),
        (null, 8, 2, 333, 1),
        (null, 9, 2, 444, 1),
        (null, 10, 2, 555, 1),

        (null, 11, 3, 111, 1),
        (null, 12, 3, 222, 1),
        (null, 13, 3, 333, 1),
        (null, 14, 3, 444, 1),
        (null, 15, 3, 555, 1),

        (null, 16, 4, 111, 1),
        (null, 17, 4, 222, 1),
        (null, 18, 4, 333, 1),
        (null, 19, 4, 444, 1),
        (null, 20, 4, 555, 1),

        (null, 21, 5, 111, 1),
        (null, 22, 5, 222, 1),
        (null, 23, 5, 333, 1),
        (null, 24, 5, 444, 1),
        (null, 25, 5, 555, 1);

    insert into products_stores values
        (null, 1, 1, 599999, "https://www.queteimporta.com", current_timestamp, 1),
        (null, 2, 1, 499999, "https://www.queteimporta.com", current_timestamp, 1),
        (null, 3, 1, 699999, "https://www.queteimporta.com", current_timestamp, 1),
        (null, 4, 1, 999999, "https://www.queteimporta.com", current_timestamp, 1),
        (null, 5, 1, 156999, "https://www.queteimporta.com", current_timestamp, 1),

        (null, 6, 1, 1000000, "https://www.queteimporta.com", current_timestamp, 1),
        (null, 7, 1, 689990, "https://www.queteimporta.com", current_timestamp, 1),
        (null, 8, 1, 499990, "https://www.queteimporta.com", current_timestamp, 1),
        (null, 9, 1, 450000, "https://www.queteimporta.com", current_timestamp, 1),
        (null, 10, 1, 680000, "https://www.queteimporta.com", current_timestamp, 1);

/* Fin Insert */   
