@extends('errors::layout')

@section('title', 'Page Expired')

@section('message')
    Su sesión ha expirado por inactividad.
    <br/><br/>
    Por favor refresque e intente nuevamente.
@stop
