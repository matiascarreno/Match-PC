<p align="center"><img src="http://match-pc.com/logo.jpg"></p>

## About Match-PC

This software is based on a web system, which would be able to advise the client in the best way possible. Based on the needs of the client and identifying the reason for which he wishes to use the hardware (work, office, games and leisure, for the family, personal use, etc.) the system can provide the client with a list of the alternatives of ideal hardware for your needs with comparative tables of performance and price, to facilitate the client the decision of which product is the most suitable for him.

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## License

Match-PC is a training project with the purpose of creating a tool that co-exists different frameworks and libraries used in web development. And it's an open-sourced software.